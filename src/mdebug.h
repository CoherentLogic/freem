/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mdebug.h
 *    debugger enhancements
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_MDEBUG_H)
#define _MDEBUG_H

#define MAXWATCH 100

typedef struct dbg_watch {
    
	/* name of variable to watch */
    char *varnam;

    /* number of changes to this variable */
    int chgct;

    /* pending fires */
    int firect;


} dbg_watch;

extern void dbg_init (void);
extern dbg_watch *dbg_add_watch (char *varnam);
extern void dbg_remove_watch (char *varnam);
extern dbg_watch *dbg_find_watch (char *varnam);
extern void dbg_dump_watch (char *varnam);
extern char *dbg_get_watch_name (char *varnam);
extern void dbg_fire_watch (char *varnam);
extern void dbg_dump_watchlist (void);


extern short dbg_enable_watch;               /* 0 = watches disabled, 1 = watches enabled */
extern int dbg_pending_watches;

#endif