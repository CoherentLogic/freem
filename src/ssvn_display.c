/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_display.c
 *    ^$DISPLAY ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <unistd.h>

#include "mpsdef.h"

#if defined(MWAPI_GTK)

#include <gtk/gtk.h>
#include <X11/Xlib.h>
#include <glib.h>
#include <pango/pangocairo.h>

#include "mref.h"

int ssvn_display_initialized = 0;
char clipboard_buffer[256];

void ssvn_display_update(void)
{
    int i;
    int j;
    int k;
    
    int count_screens;
    char k_buf[512];
    char d_buf[512];

    char display_name[50];

    Display *display;
    Screen *screen;

    PangoFontFamily **families;
    int n_families;
    PangoFontMap *font_map;

    char *result;
    unsigned long ressize, restail;
    int resbits;
    Atom bufid;
    Atom fmtid;
    Atom propid;
    Atom incrid;
    XEvent event;
    unsigned long color;
    Window window;

    memset (clipboard_buffer, 0, sizeof (clipboard_buffer));
    
//    printf ("mwapi-gtk:  using X display %s\r\n", getenv ("DISPLAY"));
    
    display = XOpenDisplay (NULL);

    if (display == NULL) return;
    
    count_screens = ScreenCount (display);

    color = BlackPixel (display, DefaultScreen(display));
    window = XCreateSimpleWindow (display, DefaultRootWindow(display), 0,0, 1,1, 0, color, color);
    bufid = XInternAtom (display, "CLIPBOARD", False);
    fmtid = XInternAtom (display, "STRING", False);
    propid = XInternAtom (display, "XSEL_DATA", False);
    incrid = XInternAtom (display, "INCR", False);
    font_map = pango_cairo_font_map_get_default ();
    pango_font_map_list_families (font_map, &families, &n_families);

    XConvertSelection (display, bufid, fmtid, propid, window, CurrentTime);

    do {
        XNextEvent (display, &event);
    } while (event.type != SelectionNotify || event.xselection.selection != bufid);

    if (event.xselection.property) {
        XGetWindowProperty (display, window, propid, 0, LONG_MAX/4, False, AnyPropertyType,
                           &fmtid, &resbits, &ressize, &restail, (unsigned char**)&result);
        
        if (fmtid != incrid) {
            snprintf (clipboard_buffer, (ressize > 254 ? 254 : ressize), "%s", result);
            stcnv_c2m (clipboard_buffer);
            clipboard_buffer[255] = '\201';
        }
        else {
            clipboard_buffer[0] = '\201';
        }

        XFree (result);
    } 
    
    for (int i = 0; i < count_screens; ++i) {
        screen = ScreenOfDisplay (display, i);

        snprintf (display_name, 49, "%d", i);
    
        snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202PLATFORM\201", display_name);
        symtab (set_sym, k_buf, "X/GTK,3\201");

        /*
        snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202CLIPBOARD\201", display_name);
        symtab (set_sym, k_buf, clipboard_buffer);
        */
        snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202SIZE\201", display_name);
        snprintf (d_buf, 512 - 1, "%d,%d\201", screen->width, screen->height);
        symtab (set_sym, k_buf, d_buf);

        snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202SPECTRUM\201", display_name);
        snprintf (d_buf, 512 - 1, "%d\201", 16777216L);
        symtab (set_sym, k_buf, d_buf);

        snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202COLORTYPE\201", display_name);
        snprintf (d_buf, 512 - 1, "COLOR\201");
        symtab (set_sym, k_buf, d_buf);

        snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202UNITS\201", display_name);
        snprintf (d_buf, 512 - 1, "PIXEL\201");
        symtab (set_sym, k_buf, d_buf);
        
        if (!ssvn_display_initialized) {

//            printf ("mwapi-gtk:  enumerating system fonts... ");
            for (j = 0; j < n_families; j++) {
                PangoFontFamily *family = families[j];
                const char *family_name;

                family_name = pango_font_family_get_name (family);

                
                snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202TYPEFACE\202%s\202%d\201", display_name, family_name, 0);
                snprintf (d_buf, 512 - 1, "\201");
                symtab (set_sym, k_buf, d_buf);

                /*
                for (k = 8; k <= 72; k += 2) {                    
                    snprintf (k_buf, 512 - 1, "^$DISPLAY\202%s\202TYPEFACE\202%s\202%d\201", display_name, family_name, k);
                    snprintf (d_buf, 512 - 1, "\201");
                    symtab (set_sym, k_buf, d_buf);
                }
                */
                
            }
//            printf ("%d font families discovered\r\n", n_families);

        }

    }

    g_free (families);

    if (!ssvn_display_initialized) {
        ssvn_display_initialized = 1;
    }

    XCloseDisplay (display);
    
    return;
}

void ssvn_display(short action, char *key, char *data)
{
    int i;
    
    freem_ref_t *r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_display");

    char *dbuf = (char *) malloc (255 * sizeof (char));
    NULLPTRCHK(dbuf,"ssvn_display");
    
    mref_init (r, MREF_RT_SSV, "^$DISPLAY");
    internal_to_mref (r, key);

    if (r->subscript_count < 2) {
        ierr = INVREF;
        goto done;
    }

    for (i = 0; i < strlen (r->subscripts[1]); i++) r->subscripts[1][i] = toupper (r->subscripts[1][i]);

    switch (action) {

        case fra_order:

            if (strcmp (mref_get_subscript (r, 1), "CLIPBOARD") == 0) {
                ierr = INVREF;
                goto done;                
            }
            
            symtab (action, key, data);
            break;
            
        case get_sym:

            if (strcmp (mref_get_subscript (r, 1), "CLIPBOARD") == 0) {

            }
            
            ssvn_display_update ();
            symtab (action, key, data);
            
            break;
            

        default:
            ierr = INVREF;
            break;

    }

done:

    free (r);
    free (dbuf);
    
    return;

}
#else
void ssvn_display_update(void)
{
    return;
}

void ssvn_display(short action, char *key, char *data)
{
    ierr = M38;
    *data = EOL;
    return;
}
#endif
