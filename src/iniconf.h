/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   iniconf.h
 *    Function prototypes, structs, and macros for reading
 *    FreeM configuration files
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#ifndef _CONFIG_H
 #define _CONFIG_H

#define CONF_BUFSIZE 255

typedef struct ini_keyvalue {
    char *key;
    char *value;

    struct ini_keyvalue *next;
} ini_keyvalue;

typedef struct ini_section {
    char *name;

    ini_keyvalue *head;
    
    struct ini_section *next;
} ini_section;

extern int get_conf(char *section, char *key, char *value);
extern int file_exists(char *filename);
extern void write_profile_string(char *file, char *section, char *key, char *value);
extern ini_keyvalue *ini_insert(ini_section *s, char *section, char *key, char *value);
extern ini_keyvalue *ini_kv_insert(ini_section *s, char *key, char *value);
extern void ini_section_delete(ini_section *head, char *name);
extern void ini_key_delete(ini_section *head, char *key);
#endif
