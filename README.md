FreeM is a work-in-progress implementation of the MUMPS programming language, and attempts to conform to ANSI Standard MUMPS X11.1-1995.

## Unique Features

FreeM has several unique features:

* FreeM is the only current M implementation to offer support for asynchronous events from MDC extension proposal X11/1998-28
* It allows the programmer to define new Z-commands as M routines
* It allows the programmer to define new $Z... ISVs as M routines
* It supports extensive command-line history, editing, and recall through GNU readline in direct mode
* It supports using shebang-lines to run M routines as sysadmin scripts

## History

FreeM started its life as FreeMUMPS, written for MS-DOS and ported to Linux and SCO UNIX by a mysterious individual going by the name of "Shalom ha-Ashkenaz". It was released to MUG/Deutschland in 1998, and maintenance was taken over by the GUMP (Generic Universal M Project) thereafter, which changed its name first to PSM (Public Standard MUMPS) and then by popular request to FreeM.

When GT.M was open-sourced in late 1999, FreeM and GUMP were essentially abandoned. L.D. Landis, the owner of the original GUMP SourceForge project, and one of FreeM's significant contributors, passed maintenance of FreeM and ownership of its SourceForge project to John Willis in 2015. At this point, FreeM would not compile or run on modern Linux systems, so steps were taken to remedy the most pressing issues in the codebase. Limitations on the terminal size (previously hard-coded to 80x25) were lifted, and new $VIEW functions were added to retrieve the terminal size information. $X and $Y intrinsic special variables were updated to support arbitrary terminal sizes, and FreeM was once again able to build and run.

## License

Copyright (C) 1998 MUG Deutschland
Copyright (C) 2014, 2020 Coherent Logic Development LLC

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 
This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for more details.
 
You should have received a copy of the GNU Affero General Public License along with this program.  If not, see https://www.gnu.org/licenses/.

This README document is Copyright (C) 2020 John P. Willis

Permission is granted to copy, distribute and/or modify this document under the 
terms of the GNU Free Documentation License, Version 1.3 or any later version 
published by the Free Software Foundation; with no Invariant Sections, with no 
Front-Cover texts, and with no Back-Cover Texts.

## Building

If you wish to build this software, and have downloaded it with git, you will need to run `autoreconf --install` prior to the first run of `./configure; make; sudo make install`.

You will need GNU autotools, autoconf, texinfo and libtool, and optionally (but highly recommended), GNU readline (the development version, including the headers).

The software is known to build and run on Linux (amd64, i386, armv6l, armv7l, aarch64, and s390x), SCO OpenServer 5.0.7, Solaris 10 (sparc64), Solaris 11.3 (sparc64), Solaris 11.4 (i86), GNU/Hurd 0.9 (i386), macOS Mojave (with the real gcc compiler installed), FreeBSD 12.1 (amd64), NetBSD 9.0 (vax and amd64), and OpenBSD 6.6 (amd64).

## Running

Before running any instance of the FreeM interpreter, you must launch the FreeM daemon, which coordinates shared memory access for the lock table, job table, and interprocess communication. This is accomplished by passing the `--daemon` flag to the `freem` executable. As the daemon is still under heavy development, we recommend also passing the `--nofork` flag to run the daemon in the foreground, e.g. `freem --daemon --nofork`.

If FreeM hangs or otherwise goes sideways, kill the daemon and run the `cleanup_shm.sh` script in the root of the FreeM distribution. This will remove shared memory resources and allow you to start the daemon again. You may also need to run `tset` and/or `reset` after killing the daemon in order to restore terminal parameters to sane values. The particular incantation I use in development is `freem --daemon --nofork; tset`. I also like to run the daemon inside of either a dedicated terminal emulator tab or a dedicated GNU Screen window. Using `tmux` would likely work as well, though I do not use `tmux` and have not tested it.

Once the daemon reaches a more complete state of development, these workarounds will no longer be required.

After the daemon is running correctly, FreeM instances may be launched as normal (i.e. without having `--daemon` passed to them). To minimize lock-ups of the daemon in this early phase of development, avoid killing FreeM processes with `kill -9`.

The daemon is being developed in order to improve the reliability of SSVNs like `^$JOB`, enable transaction processing, and improve the performance of `LOCK`. Eventually, the daemon will also coordinate database replication and allow for multiple environments running on the same host.

## Documentation

FreeM documentation is available at https://freem.coherent-logic.com/docs/

## Production Readiness

FreeM is not yet production-ready. 

## Contributors

Current contributors denoted with a plus sign following their name and role.

* Shalom ha-Ashkenaz (Original Implementer)
* Jon Diamond (Library, Utilities, Conformance)
* Winfried Gerum (Code, Advice, MTA coordination)
* Greg Kreis (Hardhats coordination, Dependencies)
* [Larry Landis](https://www.rosecroft.net) (Coordination, Code, Documentation)
* Frederick D.S. Marshall (MDC Standards Conformance) +
* Lloyd Milligan (Code, Testing, Documentation)
* Steve Morris (Code, Microsoft)
* John Murray (Code, Conformance)
* Wilhelm Pastoors (Testing, Documentation)
* Kate Schell (Coordination, Conformance, MTA, MDC, Advice)
* Lyle Schofield (Advice, Prioritization, Tracking, Project Management)
* Axel Trocha (Code, Utilities)
* Dick Walters (Project Lead, Chief Coordinator, MTA)
* David Whitten (QA Test Suite, MDC, Advice) +
* [David Wicksell](https://gitlab.com/dlwicksell) (Debugging, Code, Testing) +
* John Willis (Current Maintainer and Project Lead) +
* Steve Zeck (Contributions to Symbol Table Management, Code)
