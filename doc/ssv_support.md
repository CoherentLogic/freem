^$GLOBAL(<global>,"BLOCK",<block-number>,"TYPE") => "POINTER", "ROOT", or "DATA"
^$GLOBAL(<global>,"BLOCK",<block-number>,"BLOCKCOUNT") => number (root block only) 
^$GLOBAL(<global>,"BLOCK",<block-number>,"KEYLEN") => number (data blocks only)
^$GLOBAL(<global>,"BLOCK",<block-number>,"RLPTR") => right-link pointer (data blocks only)
^$GLOBAL(<global>,"BLOCK",<block-number>,"LLPTR") => left-link pointer (data blocks only)
^$GLOBAL(<global>,"BLOCK",<block-number>,"OFFS") => offset to first free byte in block data area
^$GLOBAL(<global>,"BLOCK",<block-number>,"CHARACTER") => "M" or "ASCII"
^$GLOBAL(<global>,"BLOCK",<block-number>,"COLLATE") => "M" or "ASCII"
^$GLOBAL(<global>,"NAMESPACE") => namespace where <global> resides
^$GLOBAL(<global>,"FILE") => full path to <global> data file
^$GLOBAL(<global>,"BLOCKSIZE") => block size for <global>
^$GLOBAL(<global>,"BLOCKS") => block count for <global>
^$GLOBAL(<global>,"BYTES") => size of global in blocks

^$DEVICE(<channel>,"EOF") => 1 if <channel> in EOF state, 0 otherwise
^$DEVICE(<channel>,"LENGTH") => length of <channel> in bytes
^$DEVICE(<channel>,"MNEMONICSPACE") => current mnemonic space of <channel>, only for channel 0, always "X364"
^$DEVICE(<channel>,"DSW") => device status word for <channel>
^$DEVICE(<channel>,"TERMINATOR") => read terminator for <channel>
^$DEVICE(<channel>,"TERMID") => terminal model (only for channel 0), ex. "vt100"
^$DEVICE(<channel>,"ECHOON") => 0 if local echo off, 1 otherwise 
^$DEVICE(<channel>,"DELMODE") => 1 | 0
^$DEVICE(<channel>,"ESCSEQPROC") => 1 | 0
^$DEVICE(<channel>,"CONVUPPER") => 1 | 0
^$DEVICE(<channel>,"DELEMPTY") => 1 | 0
^$DEVICE(<channel>,"NOCTRLS") => 1 | 0
^$DEVICE(<channel>,"CTRLOPROC") => 1 | 0
^$DEVICE(<channel>,"NOTYPEAHEAD") => 1 | 0

^$DEVICE(<chan>)             => currently open I/O channels
^$DEVICE(<chan>,"$X")        => current value of $x on open I/O device channel
^$DEVICE(<chan>,"$Y")        => current value of $y on open I/O device channel
^$DEVICE(<chan>,"CHARACTER") => "M" - same as standard
^$DEVICE(<chan>,"FD")        => operating system file descriptor for file/device
^$DEVICE(<chan>,"MODE")      => device's current mode; one of 
                                WRITE|READ|APPEND|IO|TCPIP|SERVER|NOFORK|FORKED|PIPE|NEWPIPE|DEVICE
^$DEVICE(<chan>,"NAME")      => file/device name in the operating system
^$DEVICE(<chan>,"NAMESPACE") => current mnemonic namespace (if set); user-defined M routine
^$DEVICE(<chan>,"TYPE")      => type of device in channel; one of 1,FILE|2,SOCKET|3,PIPE|4,TERMINAL
                                the numbers match the second comma-delimited number in $device, and
                                therefore can be looked up for the type name from this node
^$DEVICE(<chan>,"OPTIONS","DELETE")     => keys which generate a delete; one of NONE|BACK|DELETE|BOTH
                                           BACK is the Backspace key, DELETE is the Delete key
^$DEVICE(<chan>,"OPTIONS","ECHO")       => whether device echoing is enabled; one of 1|0
^$DEVICE(<chan>,"OPTIONS","ESCAPE")     => whether device escape processing is enabled; one of 1|0
^$DEVICE(<chan>,"OPTIONS","OUTPUT")     => the sequence of 0 to 6 characters that are generated from 
                                           a newline in M code with (write !)
^$DEVICE(<chan>,"OPTIONS","TERMINATOR") => the comma-separated list of key codes that terminate a read 
                                           or other input

^$SYSTEM("NAME_LENGTH") => number (max length of names)
^$SYSTEM("STRING_MAX") => number (max length of strings)
^$SYSTEM("$NEXTOK") => 1 ($NEXT enabled)
^$SYSTEM("EOK") => 1 (exponent notation enabled)
^$SYSTEM("OFFOK") => 1 (DO and GOTO offsets enabled)
^$SYSTEM("BIG_ENDIAN") => 1 if host system is big-endian, 0 otherwise

^$DISPLAY(<display>,"CLIPBOARD") => X11 clipboard contents
^$DISPLAY(<display>,"PLATFORM") => window system platform
^$DISPLAY(<display>,"SIZE") => "<width>,<height>" of <display>
^$DISPLAY(<display>,"SPECTRUM") => color depth of <display>
^$DISPLAY(<display>,"COLORTYPE") => "COLOR"
^$DISPLAY(<display>,"UNITS") => "PIXEL"
^$DISPLAY(<display>,"TYPEFACE",<typeface-name>,0) => empty string for each <typeface-name> on system

^$JOB(<pid>,"CHARACTER") => "M"
^$JOB(<pid>,"CWD") => current working directory of <pid>
^$JOB(<pid>,"GLOBAL") => empty string
^$JOB(<pid>,"LOCK") => empty string
^$JOB(<pid>,"IOPATH") => OPEN, USE, CLOSE path for sequential I/O
^$JOB(<pid>,"PRIORITY") => process priority for <pid>
^$JOB(<pid>,"ROUTINE") => routine name of routine currently being executed by <pid>
^$JOB(<pid>,"$PRINCIPAL") => current value of $PRINCIPAL for <pid>
^$JOB(<pid>,"$TLEVEL") => current value of $TLEVEL for <pid>
^$JOB(<pid>,"$IO") => current value of $IO for <pid>
^$JOB(<pid>,"USER") => UID of <pid>'s process owner
^$JOB(<pid>,"GROUP") => GID of <pid>'s process owner
^$JOB(<pid>,"NAMESPACE") => namespace of <pid>
^$JOB(<pid>,"SYMTAB") => 0 | 1 (currently-active symbol table)
^$JOB(<pid>,"PID") => <pid>
^$JOB(<pid>,"MATH") => "IEEE754" or "FIXED"
^$JOB(<pid>,"REVSTR") => 1 if reverse string function forms enabled, 0 otherwise

^$ZPROCESS(<pid>,"EXISTS") => 1 if <pid> is a PID for a valid process
^$ZPROCESS(<pid>,"ATTRIBUTE",<attribute-name>) => contents of /proc/<pid>/<attribute-name>
^$ZPROCESS(<pid>,"SIGNAL")=<numexpr> => sends signal <numexpr> to <pid>

^$ZRPI("INITIALIZE") => initializes Raspberry Pi GPIO subsystem
^$ZRPI("GPIO",<pin>,"MODE") => "INPUT", "OUTPUT", "PWM_OUTPUT", or "GPIO_CLOCK"
^$ZRPI("GPIO",<pin>,"DIGITAL") => digital read/write of <pin>
^$ZRPI("GPIO",<pin>,"ANALOG") => analog read/write of <pin>