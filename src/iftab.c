/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   iftab.c
 *    implementation of the freem transactions-in-flight symbol table
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include "iftab.h"
#include "mpsdef.h"
#include "transact.h"

iftab *iftab_head;

iftab *iftab_insert(short action, char *key, char *data, int tlevel)
{
    iftab *t;

    for (t = iftab_head; t != NULL; t = t->next) {
        
        if (stcmp (t->key, key) == 0) {

            /* the key already exists. this is now an update. */
            free (t->data);
	    
            t->data = malloc (stlen (data) + 1);
	    NULLPTRCHK(t->data,"iftab_insert");
	    
            stcpy (t->data, data);
            t->killed = FALSE;

            return t;
        }

    }

    /* key did not exist. this is now a true insert. */

    t = (iftab *) malloc (sizeof (iftab));
    NULLPTRCHK(t,"iftab_insert");
    
    t->key = malloc (stlen (key) + 1);
    NULLPTRCHK(t->key,"iftab_insert");
    
    t->data = malloc (stlen (data) + 1);
    NULLPTRCHK(t->data,"iftab_insert");

    stcpy (t->key, key);
    stcpy (t->data, data);

    t->killed = FALSE;
    t->tlevel = tlevel;
    t->action = action;

    t->next = iftab_head;
    iftab_head = t;

    return t;
}

iftab *iftab_retrieve(char *key, char *data)
{
    iftab *t;

    for (t = iftab_head; t != NULL; t = t->next) {
        if (stcmp (t->key, key) == 0) {
	    
            data = (char *) malloc (stlen (t->data) + 1);
	    NULLPTRCHK(data,"iftab_retrieve");
	    
            stcpy (data, t->data);

            return t;
        }
    }

    return (iftab *) NULL;
}

void iftab_delete(char *key)
{
    iftab *t = iftab_head;
    iftab *p = NULL;


    if ((t != (iftab *) NULL) && (stcmp (t->key, key) == 0)) {
        iftab_head = t->next;

        free (t->key);
        free (t->data);
        free (t);
        return;
    }

    while ((t != NULL) && (stcmp (t->key, key) != 0)) {
        p = t;
        t = t->next;
    }

    if (t == NULL) return;

    p->next = t->next;
    free (t->key);
    free (t->data);
    free (t);

    return;
}

iftab *iftab_kill(char *key)
{
    iftab *t;

    for(t = iftab_head; t != NULL; t = t->next) {
        if (stcmp (t->key, key) == 0) {
            t->killed = TRUE;

            return t;
        }
    }

    t = iftab_insert (kill_sym, key, "\201", tp_level);
    t->killed = TRUE;

    return t;
}

void iftab_pop_tlevel(int tlevel)
{
    iftab *t;

    for (t = iftab_head; t != NULL; t = t->next) {
        if (t->tlevel == tlevel) iftab_delete (t->key);
    }
}
