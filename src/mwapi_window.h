/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mwapi_window.h
 *    mwapi window support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2014, 2020, 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include "config.h"

#if !defined(__MWAPI_WINDOW_H) && defined(MWAPI_GTK)
# define __MWAPI_WINDOW_H

#include <gtk/gtk.h>

typedef struct mwapi_window {

    char window_name[STRLEN];
    GtkWidget *window;

    short win_visible;
    
    struct mwapi_window *next;

} mwapi_window;

void mwapi_default_destroy_handler(GtkWidget *widget, gpointer data);
void mwapi_on_merge_complete(char *window_name);
void mwapi_foreach(void (*callback)(mwapi_window *));
int mwapi_win_count(void);
void mwapi_win_show_all(void);
void mwapi_win_show(mwapi_window *win);
mwapi_window *mwapi_win_get(char *window_name);

#endif
