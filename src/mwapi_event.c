/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mwapi_event.c
 *    mwapi event support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2014, 2020, 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/
#include "config.h"

#if defined(MWAPI_GTK)

#include <gtk/gtk.h>
#include "mpsdef0.h"
#include "mwapi_event.h"
#include "mwapi_window.h"

short evt_sync_enabled = FALSE;
mwapi_event_queue_entry *mwapi_event_queue_head;

mwapi_event *mwapi_event_new(char *evclass, char *window_name, GtkWidget *window)
{
    mwapi_event *e = (mwapi_event *) malloc (sizeof (mwapi_event));
    NULLPTRCHK(e,"mwapi_event_new");

    e->evclass = evclass;
    e->window_name = window_name;
    e->window = window;
    
    stcnv_m2c (e->window_name);

    return e;    
}

void mwapi_wire_event(mwapi_event *e, char *entryref)
{

    char m_gtk_ev[100];

    if (strcmp (e->evclass, "CLOSE") == 0) strcpy (m_gtk_ev, "destroy");

    e->entryref = (char *) malloc (strlen (entryref) * sizeof (char));
    NULLPTRCHK(e->entryref,"mwapi_wire_event");
    
    strcpy (e->entryref, entryref);
    
    g_signal_connect (e->window, m_gtk_ev, G_CALLBACK(mwapi_enqueue_event), (gpointer) e);

}

void mwapi_enqueue_event(GtkWidget *widget, gpointer data)
{
    mwapi_event *e = (mwapi_event *) malloc (sizeof (mwapi_event));
    NULLPTRCHK(e,"mwapi_enqueue_event");

    mwapi_event_queue_entry *t = (mwapi_event_queue_entry *) malloc (sizeof (mwapi_event_queue_entry));
    NULLPTRCHK(t,"mwapi_enqueue_event");
    
    e = (mwapi_event *) data;

    //printf ("mwapi_enqueue_event():  evclass = '%s' window_name = '%s' entryref = '%s'\r\n\r\n", e->evclass, e->window_name, e->entryref);

    t->e = e;
    t->next = mwapi_event_queue_head;
    mwapi_event_queue_head = t;
    
    return;
}

int mwapi_dequeue_events(char *result)
{

    mwapi_event_queue_entry *t;
    int i = 0;
    
    for (t = mwapi_event_queue_head; t != NULL; t = t->next) {

        strcat (result, t->e->entryref);
        strcat (result, ",");
        i++;
        
    }

    // TODO: delete events
    mwapi_event_queue_head = NULL;

    /* chop off the final comma */
    result[strlen (result) - 1] = '\0';
    
    return i;
    
}

void mwapi_on_merge_complete(char *window_name)
{
    mwapi_window *t;

    if (stcmp (window_name, "\201") == 0) {
        /* we're merging to the global directly; no specified subscript */
        /* we'll do a loop here */        
    }
    else {        
        /* merging to a specific window */
        t = mwapi_win_get (window_name);
        mwapi_win_show (t);
    }

}

#endif
