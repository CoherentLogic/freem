/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_lock.c
 *    ^$LOCK ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mpsdef.h"
#include "mref.h"

void ssvn_lock_add(char *key, pid_t owner_job, int count)
{
    register int i;

    char *t_buf;
    char *k_buf;
    char *d_buf;
    freem_ref_t *r;
    
    t_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(t_buf,"ssvn_lock_add");
    
    k_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(k_buf,"ssvn_lock_add");

    d_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(d_buf,"ssvn_lock_add");
    
    r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_lock_add");

    mref_init (r, MREF_RT_GLOBAL, "");
    internal_to_mref (r, key);
    mref_to_external (r, t_buf);

    for (i = 0; i < strlen (t_buf); i++) {

        if (t_buf[i] == '\001') {
            t_buf[i] = '\0';
            break;
        }

    }
    
    snprintf (k_buf, STRLEN - 1, "^$LOCK\202%s\201", t_buf);
    snprintf (d_buf, STRLEN - 1, "%ld,%d\201\0", owner_job, count);

    global (set_sym, k_buf, d_buf);

    free (t_buf);
    free (k_buf);
    free (d_buf);
    free (r);
    
}

void ssvn_lock_remove(char *key)
{
    register int i;

    char *t_buf;
    char *k_buf;
    char *d_buf;
    freem_ref_t *r;
    
    t_buf = (char *) malloc (STRLEN * sizeof (freem_ref_t));
    NULLPTRCHK(t_buf,"ssvn_lock_add");
    
    k_buf = (char *) malloc (STRLEN * sizeof (freem_ref_t));
    NULLPTRCHK(k_buf,"ssvn_lock_add");

    d_buf = (char *) malloc (STRLEN * sizeof (freem_ref_t));
    NULLPTRCHK(d_buf,"ssvn_lock_add");
    
    r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_lock_add");

    mref_init (r, MREF_RT_GLOBAL, "");
    internal_to_mref (r, key);
    mref_to_external (r, t_buf);

    for (i = 0; i < strlen (t_buf); i++) {

        if (t_buf[i] == '\001') {
            t_buf[i] = '\0';
            break;
        }

    }
    
    snprintf (k_buf, STRLEN - 1, "^$LOCK\202%s\201", t_buf);
    snprintf (d_buf, STRLEN - 1, " \201");

    global (kill_sym, k_buf, d_buf);
    
}

void ssvn_lock(short action, char *key, char *data)
{

    switch (action) {

        case set_sym:       
        case kill_sym:
            //TODO: remove lock            
            ierr = M29;
            return;
            
        default:
            global (action, key, data);
            break;

    }
    
    return;
}
