/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   events.h
 *    event framework
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_EVENTS_H)
#define _EVENTS_H

#include "freem.h"
#include "mref.h"

#define EVT_QLENGTH 255
#define EVT_MAX 9

#define EVT_CLS_COMM 0                          /* Device events */
#define EVT_CLS_HALT 1                          /* Process halted */
#define EVT_CLS_IPC 2                           /* IPC received */
#define EVT_CLS_INTERRUPT 3                     /* Interrupt received */
#define EVT_CLS_POWER 4                         /* Power fail eminent */
#define EVT_CLS_TIMER 5                         /* Timer interval expired */
#define EVT_CLS_USER 6                          /* User-defined (ETRIGGER) events */
#define EVT_CLS_WAPI 7                          /* MWAPI (synchronous only) */
#define EVT_CLS_TRIGGER 8                       /* Database triggers */

#define EVT_S_NOMODIFY -1
#define EVT_S_DISABLED 0
#define EVT_S_ASYNC 1
#define EVT_S_SYNC 2

typedef struct evt_entry {

    char event_id[256];                         /* Event identifier */
    short evt_class;                            /* EVT_CLS_* */

} evt_entry;

typedef struct evt_status_t {

    short comm;
    short halt;
    short ipc;
    short interrupt;
    short power;
    short timer;
    short user;
    short wapi;
    short trigger;
    
} evt_status_t;

extern short evt_async_enabled;
extern short evt_async_restore;
extern short evt_async_initial;

extern short evt_queue_rear;
extern short evt_queue_front;
extern evt_entry *evt_queue[];
extern short evt_status[]; 
extern int evt_blocks[EVT_MAX];
extern int evt_depth;

extern int pending_signal_type;

/* evt_init(): initialize event framework */
extern void evt_init (void);

extern int evt_ablock (short evt_class);
extern int evt_aunblock (short evt_class);

/* evt_enqueue(): enqueue an event. interrupt should be non-zero if you wish to immediately interrupt the interpreter */
extern int evt_enqueue (char *event_id, short evt_class, short interrupt);
extern evt_entry *evt_dequeue (void);
extern char *evt_class_name (evt_entry *e);
extern char *evt_class_name_c (int c);
extern int evt_get_handlers (char *buf);
extern short evt_registered(char *event_id, short evt_class);

#endif
