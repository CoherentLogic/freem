/*
* @Author: John P. Willis
* @Date:   2020-04-03 15:59:49
* @Last Modified by:   John P. Willis
* @Last Modified time: 2020-04-03 16:23:44
*/


#include <stdio.h>
#include <string.h>
#include <freem.h>

int main(int argc, char **argv, char **envp)
{
    freem_ent_t ent;
    
    /* equivalent to $$ADDTWO^%ZFRMSAMP(2,2) */

    sprintf (ent.name, "ADDTWO^%%ZFRMSAMP");
    sprintf (ent.arguments[0], "2");
    sprintf (ent.arguments[1], "2");

    ent.argument_count = 2;

    freem_init ("USER");
    freem_function (&ent);

    printf ("Return value from $$%s(2,2) is '%s'\n", ent.name, ent.value);

}