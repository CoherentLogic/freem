/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_zfreem.c
 *    ^$ZFREEM ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "mpsdef.h"
#include "mref.h"

void ssvn_zfreem (short action, char *key, char *data)
{

    freem_ref_t *r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_zfreem");
    
    mref_init (r, MREF_RT_SSV, "^$ZFREEM");
    internal_to_mref (r, key);

#ifdef SSVN_DEBUG
    {
        int i;

        set_io (UNIX);
        printf ("%s subscript_count:  %d\n", r->name, r->subscript_count);
        for (i = 0; i < r->subscript_count; i++) {
            printf ("%s subscripts[%d]:  %s\n", r->name, i, r->subscripts[i]);
        }
        set_io (MUMPS);
    }
#endif


    *data = EOL;
    return;

}
