/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   errmsg.h
 *    error message definitions
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

int     SIflag[MAXDEV + 1] = {1, 1, 1, 1, 1};           /* SI/SO flag */

char    errmes[MAXERR][ERRLEN] = {
    "\201",                         /* OK       */
    "interrupt\201",                /* INRPT    */
    "BREAK point\201",              /* BKERR    */
    "syntax illegal in current dialect mode\201",      /* NOSTAND  */
    "variable not found\201",       /* UNDEF    */
    "label not found\201",          /* LBLUNDEF */
    "missing operand\201",          /* MISSOPD  */
    "missing operator\201",         /* MISSOP   */
    "unrecognized operator\201",    /* ILLOP    */
    "unmatched quotes\201",         /* QUOTER   */
    "comma expected\201",           /* COMMAER  */
    "equals '=' expected\201",      /* ASSIGNER */
    "argument not permitted\201",   /* ARGER    */
    "blank ' ' expected\201",       /* SPACER   */
    "unmatched parentheses\201",    /* BRAER    */
    "level error\201",              /* LVLERR   */
    "divide by zero\201",           /* DIVER    */
    "function not found\201",       /* ILLFUN   */
    "wrong number of function arguments\201",   /* FUNARG   */
    "ZTRAP error\201",              /* ZTERR    */
    "$NEXT/$ORDER error\201",       /* NEXTER   */
    "$SELECT error\201",            /* SELER    */
    "illegal command\201",          /* CMMND    */
    "argument list incorrect\201",  /* ARGLIST  */
    "invalid expression\201",       /* INVEXPR  */
    "invalid reference\201",        /* INVREF   */
    "string too long\201",          /* MXSTR    */
    "too many parameters\201",      /* TOOPARA  */
    "unit not open\201",            /* NOPEN    */
    "unit does not exist\201",      /* NODEVICE */
    "file protection violation\201",/* PROTECT  */
    "global not permitted\201",     /* GLOBER   */
    "file not found\201",           /* FILERR   */
    "program overflow\201",         /* PGMOV    */
    "stack overflow\201",           /* STKOV    */
    "symbol table overflow\201",    /* STORE    */
    "file won't read\201",          /* NOREAD   */
    "file won't write\201",         /* NOWRITE  */
    "routine not found\201",        /* NOPGM    */
    "illegal naked reference\201",  /* NAKED    */
    "illegal subscript\201",        /* SBSCR    */
    "insert syntax\201",            /* ISYNTX   */
    "database degradation\201",     /* DBDGD    */
    "job kill signal\201",          /* KILLER   */
    "hangup signal\201",            /* HUPER    */
    "numeric overflow\201",         /* MXNUM    */
    "function returns no value\201",/* NOVAL    */
    "type mismatch\201",            /* TYPEMISMATCH */
    "out of memory\201",            /* MEMOV */
    "error in name resolution\201", /* NAMERES */
    "error creating socket\201",    /* SCKCREAT */
    "invalid address family (must be IPV4 or IPV6)\201",    /* SCKIFAM */
    "invalid connection type (must be TCP or UDP)\201",     /* SCKITYP */
    "invalid port number\201",                              /* SCKIPRT */
    "connection error\201",                                 /* SCKCERR */
    "USE action invalid for connection type (possibly CONNECT on UDP socket?)\201", /* SCKAERR */
    "attempted to CONNECT an already-connected socket\201", /* SCKACON */
    "attempted to READ from a disconnected TCP socket\201", /* SCKNCON */
    "error setting socket options\201",                     /* SCKEOPT */
    "error in READ from socket\201",                        /* SCKERCV */
    "error in WRITE to socket\201",                         /* SCKESND */
    "^$ZRPI only supported on Raspberry Pi hardware\201",   /* NORPI */                         
    "cannot redefine CONST\201",                            /* CREDEF */
    "cannot modify CONST\201",                              /* CMODIFY */
    "cannot open existing file for WRITE\201",              /* FILEXWR */
    "initializing NEW with multiple setarguments not supported\201", /* INEWMULT */
    "invalid $HOROLOG value\201", /* ZINVHORO */
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    "invalid value for $ECODE\201",                         /* ECODEINV */
    "programmer assertion failed\201",                      /* ASSERT */
    "user-defined error\201",                               /* USERERR */
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    "syntax error\201", /* 128 - ZSYNTERR */
    "break\201", /* 129 - ZCTRLB */
    " \201",                         /* 130 */
    "asynchronous interruption\201", /* 131 - ZASYNC (pseudo-error) */
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    "naked indicator undefined\201",                        /* 140 - M1 */                    
    "invalid combination with $FNUMBER code atom\201",      /* M2 */
    "$RANDOM seed less than 1\201",                         /* M3 */
    "no true condition in $SELECT\201", /* M4 */
    "line reference less than zero\201", /* M5 */
    "undefined local variable\201", /* M6 */
    "undefined global variable\201", /* M7 */
    "undefined intrinsic special variable\201", /* M8 */
    "divide by zero\201", /* M9 */
    "invalid pattern match range\201", /* M10 */
    "no parameters passed\201", /* 150 - M11 */     
    "invalid line reference (negative offset)\201", /* M12 */
    "invalid line reference (line not found)\201", /* M13 */
    "line level not 1\201", /* M14 */
    "undefined index variable\201", /* M15 */
    "argumented QUIT not allowed\201", /* M16 */
    "argumented QUIT required\201", /* M17 */
    "fixed length READ not greater than zero\201", /* M18 */
    "cannot copy a tree or subtree into itself\201", /* M19 */
    "line must have a formal parameter list\201", /* M20 */
    "algorithm specification invalid\201", /* 160 - M21 */
    "SET or KILL to ^$GLOBAL when data in global\201", /* M22 */
    "SET or KILL to ^$JOB for non-existent job number\201", /* M23 */
    "change to collation algorithm while subscripted local variables defined\201", /* M24 */
    " \201", /* M25 */
    "non-existent namespace\201", /* M26 */
    "attempt to rollback a transaction that is not restartable\201", /* M27 */
    "mathematical function, parameter out of range\201", /* M28 */
    "SET or KILL on structured system variable not allowed by implementation\201", /* M29 */
    "reference to global variable with different collating sequence within a collating algorithm\201", /* M30 */
    "control mnemonic used for device without a mnemonic space selected\201", /* 170 - M31 */
    "control mnemonic used in user-defined mnemonic space which has no associated line\201", /* M32 */
    "SET or KILL to ^$ROUTINE when routine exists\201", /* M33 */
    " \201", /* M34 */
    "device does not support mnemonic space\201", /* M35 */
    "incompatible mnemonic spaces\201", /* M36 */
    "READ from device identified by the empty string\201", /* M37 */
    "invalid structured system variable subscript\201", /* M38 */ 
    "invalid $NAME argument\201", /* M39 */
    "call-by-reference in JOB actual parameter\201", /* M40 */
    "invalid LOCK argument within a transaction\201", /* 180 - M41 */
    "invalid QUIT within a transaction\201", /* M42 */
    "invalid range value ($X, $Y)\201", /* M43 */
    "invalid command outside of a transaction\201", /* M44 */
    "invalid GOTO reference\201", /* M45 */
    " \201", /* M46 */
    " \201", /* M47 */
    " \201", /* M48 */
    " \201", /* M49 */
    " \201", /* M50 */
    " \201", /* 190 - M51 */
    " \201", /* M52 */
    " \201", /* M53 */
    " \201", /* M54 */
    " \201", /* M55 */
    "identifier exceeds maximum length\201", /* M56 */
    "more than one defining occurrence of label in routine\201", /* M57 */
    "too few formal parameters\201", /* M58 */
    " \201", /* M59 */
    "illegal attempt to use an undefined SSVN\201",             /* M60 */
    " \201", /* 200 - M61 */
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201", /* M70 */
    " \201", /* 210 - M71 */
    " \201",
    " \201",
    " \201",
    "string length exceeds implementation limits\201", /* 214 - M75 */
    " \201",
    " \201",
    " \201",
    " \201",
    " \201", /* M80 */
    " \201", /* 220 - M81 */
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201", /* M90 */
    " \201", /* 230 - M91 */
    " \201", 
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201",
    " \201", /* M100 */
    "invalid value for $ECODE\201", /* 240 - M101 */
    "synchronous and asynchronous event processing cannot be simultaneously enabled for the same event class\201", /* 241 - M102 */
    "invalid event identifier\201", /* 242 - M103 */
    "ETRIGGER event identifier for IPC event class does not match job process identifier\201" /* M104 */
};
