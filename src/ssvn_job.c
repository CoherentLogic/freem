/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_job.c
 *    ^$JOB ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <ctype.h>
#include <time.h>

#include "config.h"
#include "mpsdef.h"
#include "transact.h"
#include "namespace.h"
#include "freem.h"
#include "mref.h"
#include "jobtab.h"

short cur_symtab = 0;

void ssvn_job_add_device(int channel, char *device)
{
    char *k_buf;
    char *d_buf;
    
    k_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(k_buf,"ssvn_job_add_device");

    d_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(k_buf,"ssvn_job_add_device");

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202OPEN\202%d\201", pid, channel);
    snprintf (d_buf, 512 - 1, "%s\201", device);

    global (set_sym, k_buf, d_buf);
    
    free (k_buf);
    free (d_buf);

    return;
}

void ssvn_job_remove_device(int channel)
{
    char *k_buf;
    char *d_buf;
    
    k_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(k_buf,"ssvn_job_add_device");

    d_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(k_buf,"ssvn_job_add_device");

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202OPEN\202%d\201", pid, channel);
    snprintf (d_buf, 512 - 1, "\201");

    global (kill_sym, k_buf, d_buf);
    
    free (k_buf);
    free (d_buf);

    return;

}

void ssvn_job_update(void)
{

    char k_buf[512];
    char d_buf[512];

    char tk_buf[512];
    char td_buf[512];
    
#ifndef AMIGA68K
    uid_t uid;
    gid_t gid;
#else
    int uid;
    int gid;
#endif

    int ierr_sav;
    int tf;
    
#if !defined(AMIGA68K)
    uid = getuid ();
    gid = getgid ();
#else
    uid = 0;
    gid = 0;
#endif

    ierr_sav = ierr;
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202PIPE_GLVN\201", pid);
    snprintf (d_buf, 5, "%%\201");
    global (set_sym, k_buf, d_buf);
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ZCOMMANDS\201", pid);
    stcpy (d_buf, zcommds);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ZFUNCTIONS\201", pid);
    stcpy (d_buf, zfunctions);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ZSVS\201", pid);
    stcpy (d_buf, zsvn);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202BREAK_HANDLER\201", pid);
    stcpy (d_buf, brkaction);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ROUTINE_BUFFER_SIZE\201", pid);
    snprintf (d_buf, 512 - 1, "%ld\201", PSIZE0 - 1);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ROUTINE_BUFFER_COUNT\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", NO_OF_RBUF);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ROUTINE_BUFFER_AUTO_ADJUST\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", autorsize);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202SYMBOL_TABLE_SIZE\201", pid);
    snprintf (d_buf, 512 - 1, "%ld\201", PSIZE);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202SYMBOL_TABLE_AUTO_ADJUST\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", autopsize);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202USER_DEFINED_ISV_TABLE_SIZE\201", pid);
    snprintf (d_buf, 512 - 1, "%ld\201", UDFSVSIZ);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202USER_DEFINED_ISV_TABLE_AUTO_ADJUST\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", autousize);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202GVN_UNIQUE_CHARS\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", (glvnflag.one[0] == 0) ? 255 : glvnflag.one[0]);
    global (set_sym, k_buf, d_buf);

    tf = UNSIGN (glvnflag.one[1]);
    toggle (tf);
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202GVN_CASE_SENSITIVE\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", tf);
    global (set_sym, k_buf, d_buf);
    
    tf = UNSIGN (glvnflag.one[2]);
    if (tf == 0) tf = 255;
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202GVN_NAME_SUB_LENGTH\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", tf);
    global (set_sym, k_buf, d_buf);

    tf = UNSIGN (glvnflag.one[3]);
    if (tf == 0) tf = 255;
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202GVN_SUB_LENGTH\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", tf);    
    global (set_sym, k_buf, d_buf);
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202SINGLE_USER\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", lonelyflag);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202LOWERCASE_EVERYWHERE\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", lowerflag);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ZCOMMAND_ARGUMENT_NAME\201", pid);
    stcpy (d_buf, zargdefname);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202TIMEZONE_OFFSET\201", pid);
    lintstr (d_buf, tzoffset);
    global (set_sym, k_buf, d_buf);
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ENGINES\202GLOBAL\202%s\201", pid, nsname);
    snprintf (d_buf, 512 - 1, "%s\201", gbl_u_engine);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ENGINES\202GLOBAL\202SYSTEM\201", pid);
    snprintf (d_buf, 512 - 1, "%s\201", gbl_s_engine);
    global (set_sym, k_buf, d_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ENGINES\202LOCAL\201", pid);
    snprintf (d_buf, 512 - 1, "%s\201", loc_engine);
    global (set_sym, k_buf, d_buf);    
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202CHARACTER\201", pid);
    global (set_sym, k_buf, "M\201");

    snprintf (d_buf, 512 - 1, "%s\201", curdir);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202CWD\201", pid);
    global (set_sym, k_buf, d_buf);
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202GLOBAL\201", pid);
    global (set_sym, k_buf, " \201");

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202LOCK\201", pid);
    global (set_sym, k_buf, " \201");

    snprintf (d_buf, 512 - 1, "%s\201", oucpath);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202IOPATH\201", pid);
    global (set_sym, k_buf, d_buf);

#if !defined(MSDOS)    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202PRIORITY\201", pid);
    snprintf (d_buf, 512 - 1, "%d\201", getpriority (PRIO_PROCESS, pid));
    global (set_sym, k_buf, d_buf);
#endif
    
    snprintf (d_buf, 512 - 1, "%s\201", rou_name);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ROUTINE\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "0:\"%s\"\201", dev[HOME]);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202$PRINCIPAL\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "%d\201", tp_level);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202$TLEVEL\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "0:\"%s\"\201", dev[HOME]);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202$IO\201", pid);
    global (set_sym, k_buf, d_buf);

    ssvn_job_add_device (HOME, dev[HOME]);
    
    snprintf (d_buf, 512 - 1, "%d\201", uid);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202USER\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "%d\201", gid);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202GROUP\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "%s\201", nsname);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202NAMESPACE\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "%d\201", cur_symtab);
    snprintf (k_buf, 512 - 1, "^$JOB\202\%d\202SYMTAB\201", pid);
    global (set_sym, k_buf, d_buf);
    
    snprintf (d_buf, 512 - 1, "%d\201", pid);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202PID\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "%d\201", frm_throw_all_errors);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202SET_ALL_ECODES\201", pid);
    global (set_sym, k_buf, d_buf);    
    
    snprintf (tk_buf, 512 - 1, "^$SYSTEM\202ZDATE_FORMAT\201");
    ssvn (get_sym, tk_buf, td_buf);
    
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ZDATE_FORMAT\201", pid);
    global (set_sym, k_buf, td_buf);
    
    snprintf (tk_buf, 512 - 1, "^$SYSTEM\202ZTIME_FORMAT\201");
    ssvn (get_sym, tk_buf, td_buf);

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202ZTIME_FORMAT\201", pid);
    global (set_sym, k_buf, td_buf);
    
    switch (fp_mode) {
        
        case 0:

            snprintf (d_buf, 512 - 1, "FIXED\201");
            break;

        default:

            snprintf (d_buf, 512 - 1, "IEEE754\201");
            snprintf (fp_conversion, 9, "%%.%df\201", DBL_DIG);
            zprecise = DBL_DIG;
            
            break;

    }

    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202MATH\201", pid);
    global (set_sym, k_buf, d_buf);

    snprintf (d_buf, 512 - 1, "%d\201", en_revstrf);
    snprintf (k_buf, 512 - 1, "^$JOB\202%d\202REVSTR\201", pid);
    global (set_sym, k_buf, d_buf);
    
    stcpy (d_buf, rou_name);
    stcnv_m2c (d_buf);

#if !defined(__FreeBSD__)

    if (run_daemon == FALSE) {
        sprintf (k_buf, "    freem: namespace %s routine %s", nsname, d_buf);
    }
    else {
        sprintf (k_buf, "    freem: daemon process");
    }
    strcpy (m_argv[0], k_buf);

#endif

    ierr = ierr_sav;
    
}


void ssvn_job(short action, char *key, char *data)
{
    int errsav;
    pid_t t_pid;
    int i;
    char ch;
    job_slot_t *slot;
    freem_ref_t *r;
    char *kbuf;
    char *tbuf;
    
    r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_job");
    
    kbuf = (char *) malloc (STRLEN * sizeof(char));
    NULLPTRCHK(kbuf,"ssvn_job");
    
    tbuf = (char *) malloc (STRLEN * sizeof(char));
    NULLPTRCHK(tbuf,"ssvn_job");
    
    stcpy (kbuf, key);

    mref_init (r, MREF_RT_SSVN, "^$JOB");
    internal_to_mref (r, kbuf);

    t_pid = atol (mref_get_subscript (r, 0));

    slot = job_get (t_pid);
    
    if (r->subscript_count > 1) {
        for (i = 0; i < strlen (r->subscripts[1]); i++) {
            r->subscripts[1][i] = toupper (r->subscripts[1][i]);
        }
    }

    key = mref_to_internal (r);
    stcpy (kbuf, key);

    switch (action) {
    
        case set_sym:

            {
                char cbuf[STRLEN];

                snprintf (cbuf, STRLEN - 1, "^$JOB\202%d\202NAMESPACE\201", pid);

                if (stcmp (kbuf, cbuf) == 0) {
                    
                    stcpy (tbuf, data);
                    stcnv_m2c (tbuf);

                    if (validate_namespace (tbuf) == FALSE) {
                        ierr = M26;
                    }
                    else {                    
                        set_namespace (tbuf, FALSE);
                    }

                    goto done;

                }                

                if (strcmp (mref_get_subscript (r, 1), "ZCOMMANDS") == 0) {

                    m_tolower (data);
                    
                    stcpy (zcommds, data);
                    global (action, key, data);

                    ierr = OK;
                    goto done;

                }
                else if (strcmp (mref_get_subscript (r, 1), "PIPE_GLVN") == 0) {

                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "ZFUNCTIONS") == 0) {

                    m_tolower (data);
                    
                    stcpy (zfunctions, data);
                    global (action, key, data);

                    ierr = OK;
                    goto done;

                }
                else if (strcmp (mref_get_subscript (r, 1), "ZSVS") == 0) {

                    m_tolower (data);
                    
                    stcpy (zsvn, data);
                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "LOWERCASE_EVERYWHERE") == 0) {
                    
                    int tf = tvexpr (data);

                    lowerflag = tf;
                    snprintf (data, 512 - 1, "%d\n", tf);

                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "ZCOMMAND_ARGUMENT_NAME") == 0) {

                    stcpy (zargdefname, data);
                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "BREAK_HANDLER") == 0) {

                    stcpy (brkaction, data);
                    global (action, key, data);

                    ierr = OK;
                    goto done;

                }
                else if (strcmp (mref_get_subscript (r, 1), "ROUTINE_BUFFER_SIZE") == 0) {

                    if (intexpr (data) <= 0) {
                        ierr = ARGER;
                        goto done;
                    }

                    newrsize (intexpr (data) + 1, NO_OF_RBUF);
                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "ROUTINE_BUFFER_COUNT") == 0) {
                    
                    if (intexpr (data) <= 0) {
                        ierr = ARGER;
                        goto done;
                    }

                    newrsize (PSIZE0, intexpr (data));
                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "ROUTINE_BUFFER_AUTO_ADJUST") == 0) {

                    autorsize = tvexpr (data);
                    snprintf (data, 512 - 1, "%d\201", tvexpr (data));

                    global (action, key, data);

                    ierr = OK;
                    goto done;                    
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "SYMBOL_TABLE_SIZE") == 0) {

                    if (intexpr (data) <= 0) {
                        ierr = ARGER;
                        goto done;
                    }

                    newpsize (intexpr (data));

                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "SYMBOL_TABLE_AUTO_ADJUST") == 0) {

                    autopsize = tvexpr (data);
                    snprintf (data, 512 - 1, "%d\201", tvexpr (data));

                    global (action, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "USER_DEFINED_ISV_TABLE_SIZE") == 0) {

                    if (intexpr (data) <= 0) {
                        ierr = ARGER;
                        goto done;
                    }

                    newusize (intexpr (data));

                    global (action, key, data);

                    ierr = OK;
                    goto done;

                }
                else if (strcmp (mref_get_subscript (r, 1), "USER_DEFINED_ISV_TABLE_AUTO_ADJUST") == 0) {
                    autousize = tvexpr (data);
                    snprintf (data, 512 - 1, "%d\201", tvexpr (data));

                    global (action, key, data);

                    ierr = OK;
                    goto done;
                }
                else if (strcmp (mref_get_subscript (r, 1), "GVN_UNIQUE_CHARS") == 0) {

                    int t = intexpr (data) & 0377;

                    if (t == 255) t = 0;

                    if (ierr != MXNUM) {

                        glvnflag.one[0] = (char) t;

                        snprintf (data, 512 - 1, "%d\201", t);
                        global (action, key, data);
                        
                        ierr = OK;

                    }
                    else {
                        ierr = ARGER;
                    }

                    goto done;                    
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "GVN_CASE_SENSITIVE") == 0) {
                    int t = tvexpr (data);
                    toggle (t);

                    if (ierr != MXNUM) {

                        glvnflag.one[1] = t;

                        snprintf (data, 512 - 1, "%d\201", t);
                        global (action, key, data);

                        ierr = OK;

                    }
                    else {
                        ierr = ARGER;
                    }

                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "GVN_NAME_SUB_LENGTH") == 0) {
                    int t = intexpr (data) & 0377;

                    if (t == 255) t = 0;

                    if (ierr != MXNUM) {
                        glvnflag.one[2] = (char) t;

                        snprintf (data, 512 - 1, "%d\201", t);
                        global (action, key, data);

                        ierr = OK;
                    }
                    else {
                        ierr = ARGER;
                    }

                    goto done;
                        
                }
                else if (strcmp (mref_get_subscript (r, 1), "GVN_SUB_LENGTH") == 0) {
                    int t = intexpr (data) & 0377;

                    if (t == 255) t = 0;

                    if (ierr != MXNUM) {
                        glvnflag.one[3] = (char) t;

                        snprintf (data, 512 - 1, "%d\201", t);
                        global (action, key, data);

                        ierr = OK;
                    }
                    else {
                        ierr = ARGER;
                    }

                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "SINGLE_USER") == 0) {
                    int tf = tvexpr (data);
                    close_all_globals ();

                    snprintf (data, 512 - 1, "%d\201", tf);
                    global (action, key, data);

                    ierr = OK;
                    goto done;                    
                }
                else if (stcmp (mref_get_subscript (r, 1), "TIMEZONE_OFFSET") == 0) {
                    long tzo = intexpr (data);
                    long tzj;
                    
                    if (ierr > OK) goto done;

                    tzj = time (0L);

                    if ((tzoffset > 0 && ((tzj + 86400 + tzoffset) < 0 || (tzj + tzoffset) < 0)) ||
                        (tzoffset < 0 && ((tzj + 86400 + tzoffset ) > (tzj + 86400) ||
                                          (tzj + tzoffset) > tzj))) {
                        ierr = ARGER;
                        goto done;
                    }

                    tzoffset = tzo;

                    snprintf (data, 512 - 1, "%d\201", tzoffset);
                    global (action, key, data);

                    goto done;
                }                    
                else if (strcmp (mref_get_subscript (r, 1), "ENGINES") == 0) {

                    if (strcmp (mref_get_subscript (r, 2), "GLOBAL") == 0) {

                        if ((strcmp (mref_get_subscript (r, 3), nsname) == 0) && (strcmp (mref_get_subscript (r, 3), "SYSTEM") != 0)) {
                            stcpy (tbuf, data);
                            stcnv_m2c (tbuf);
                            
                            global_set_engine ('u', tbuf);

                            goto done;
                            
                        }
                        else if (strcmp (mref_get_subscript (r, 3), "SYSTEM") == 0) {

                            stcpy (tbuf, data);
                            stcnv_m2c (tbuf);

                            global_set_engine ('s', tbuf);
                        
                            goto done;
                            
                        }
                        
                    }
                    
                    ierr = M29;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "USER_ERRORS") == 0) {

                    global (set_sym, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "SET_ALL_ECODES") == 0) {

                    frm_throw_all_errors = atol (data);
                    global (set_sym, key, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "ALIASES") == 0) {
                    global (set_sym, key, data);

                    ierr = OK;
                    goto done;
                }
                else if (strcmp (mref_get_subscript (r, 1), "CWD") == 0) {

                    stcpy (tbuf, data);
                    stcnv_m2c (tbuf);

                    if (chdir (tbuf) != 0) {
                        ierr = PROTECT;
                        goto done;
                    }
                    else {
                        strcpy (curdir, tbuf);
                        global (set_sym, kbuf, data);

                        ierr = OK;
                        goto done;
                    }

                }
                else if (strcmp (mref_get_subscript (r, 1), "EVENT") == 0) {
                    global (set_sym, kbuf, data);

                    ierr = OK;
                    goto done;
                } 
                else if (strcmp (mref_get_subscript (r, 1), "PRIORITY") == 0) {

                    int new_pri;                   
                    
                    stcpy (tbuf, data);
                    stcnv_m2c (tbuf);

                    new_pri = atoi (tbuf);

                    
                    i = nice (40);
                    nice ((int) (new_pri - 40));

                    global (set_sym, kbuf, data);

                    ierr = OK;
                    goto done;

                }
                else if (strcmp (mref_get_subscript (r, 1), "IOPATH") == 0) {

                    stcpy (oucpath, data);
                    global (set_sym, kbuf, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "REVSTR") == 0) {

                    short tres = tvexpr (data);

                    if (tres == TRUE) {
                        en_revstrf = 1;
                    }
                    else {
                        en_revstrf = 0;
                    }                                        
                    
                    global (set_sym, kbuf, data);

                    ierr = OK;
                    goto done;
                    
                }
                else if (strcmp (mref_get_subscript (r, 1), "ZDATE_FORMAT") == 0) {
                    global (set_sym, key, data);
                    ierr = OK;
                    goto done;

                }
                else if (strcmp (mref_get_subscript (r, 1), "ZTIME_FORMAT") == 0) {
                    global (set_sym, key, data);
                    ierr = OK;
                    goto done;

                }
		else if (strcmp (mref_get_subscript (r, 1), "SYMTAB") == 0) {

		    int itmp = atoi (data); 

		    if ((data[0] < '0') || (data[0] > '9')) {
			ierr = INVEXPR;
			goto done;
		    }
		    
		    if (itmp != 0 && itmp != 1) {
			ierr = INVEXPR;
			goto done;
		    }
		    
		    if (cur_symtab == itmp) {
			ierr = OK;
			goto done;
		    }
		    
		    if (cur_symtab == 0) {
			cur_symtab = 1;
		    }
		    else {
			cur_symtab = 0;
		    }

		    if (apartition == NULL) {
			
			apartition = calloc ((unsigned) PSIZE + 1, 1);
			asymlen = PSIZE;
			
			for (i = 0; i < 128; i++) aalphptr[i] = 0L;
			
		    } 
		    
		    {
			
			char   *x;
			
			x = apartition;
			apartition = partition;
			partition = x;
			
		    }
		    
		    ch = asymlen;
		    asymlen = symlen;
		    symlen = ch;
		    
		    for (i = 0; i < 128; i++) {
			ch = aalphptr[i];
			aalphptr[i] = alphptr[i];
			alphptr[i] = ch;
		    }
		    
		    s = &partition[symlen] - 256;	/* pointer to symlen_offset        */
		    argptr = partition;		/* pointer to beg of tmp-storage   */

		    global (set_sym, key, data);
		    
		    ierr = OK;
		    goto done;
		    
		    
		}
                else if (strcmp (mref_get_subscript (r, 1), "MATH") == 0) {

#if !defined(_AIX)		    
                    if (stcmp (data, "IEEE754\201") == 0) {

                        fp_mode = 1;
                        global (set_sym, kbuf, data);

                        snprintf (fp_conversion, 9, "%%.%df\201", DBL_DIG);
                        zprecise = DBL_DIG;

                        ierr = OK;
                        goto done;
                        
                    }
                    else if (stcmp (data, "FIXED\201") == 0) {

                        fp_mode = 0;
                        global (set_sym, kbuf, data);
                        
                        zprecise = 100;
                        
                        ierr = OK;
                        goto done;
                        
                    }
                    else {
                        ierr = M29;
                        goto done;
                    }
#endif		    
                    
                }                

                ierr = M29;
                goto done;
            }

    	case get_sym:
    	case dat:
    	case fra_order:
    	case fra_query:
    	case bigquery:
    	case getnext:
    	case m_alias:
    	case zdata:

            if (action == get_sym) {

                if (strcmp (mref_get_subscript (r, 1), "PROCESS_START") == 0) {                    
                    
                    unsigned long ilong;
                    unsigned long ilong1;

                    ilong1 = slot->start_time + tzoffset;
                    ilong = ilong1 / 86400;

                    lintstr (data, ilong + 47117);
                    i = stlen (data);

                    data[i++] = ',';
                    ilong = ilong1 - (ilong * 86400);

                    lintstr (&data[i], ilong);

                    ierr = OK;
                    goto done;
                    
                }
                
            }
            
            global (action, kbuf, data);
            
            /* here, we need to convert the data on the way out too */                                    
            if ((action == fra_query) && (stlen (data) > 0)) {
                if (strncmp (data, "^$JOB", 5) != 0) {
                    snprintf (kbuf, STRLEN - 1, "^$JOB%s\201", data);
                    stcpy (data, kbuf);
                }
            }
            
            ierr = OK;
            goto done;
    	
        case killone:
    	case kill_sym:

            if (strcmp (mref_get_subscript (r, 1), "ALIASES") == 0) {

                global (action, key, data);

                goto done;
                
            }
            else {
                job_request_stop (t_pid);
            }

            goto done;


    	default:
    		ierr = INVREF;
    		goto done;

    }


done:

    free (r);
    free (kbuf);
    free (tbuf);
    free (key);
    return;
}

void frm_process_alias (char *key)
{
    int ierr_sav;
    char *ptr;
    char name[256];
    char *subprt;
    register int i;
    register char ch;

    char *k_buf;
    char *d_buf;
    char *t_buf;
    
    k_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(k_buf,"frm_process_alias");

    d_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(d_buf,"frm_process_alias");

    t_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(t_buf,"frm_process_alias");

    ierr_sav = ierr;
    ptr = key;
    i = 0;
    
    while ((ch = *(ptr++)) != DELIM && ch != EOL && i < 256) {
        name[i++] = ch;
    }

    name[i] = '\0';
    subprt = key + i;

    snprintf (k_buf, STRLEN - 1, "^$JOB\202%ld\202ALIASES\202%s\201", pid, name);
    global (get_sym, k_buf, d_buf);

    stcnv_m2c (d_buf);

    if (ierr == OK) {
        snprintf (t_buf, STRLEN - 1, "%s%s\201", d_buf, subprt);
        stcpy (key, t_buf);
    }

    free (k_buf);
    free (d_buf);
    free (t_buf);

    ierr = ierr_sav;

    return;
    
}
