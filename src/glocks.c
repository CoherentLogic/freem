/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   glocks.c
 *    display and clear M locks
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include <stddef.h>
#include "mpsdef0.h"
#include "errmsg.h"
#include <signal.h>
#include <setjmp.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>

#ifndef FREEBSD
#define EOF -1
#endif

#include <stdio.h>
#ifdef SYSFIVE
#include <fcntl.h>
#endif /* SYSFIVE */

/* needed if byte data are to be interpreted as unsigned integer */
#define UNSIGN(A) ((A)&0377)
void    unlock ();
void    zname ();
short int znamenumeric ();
long int tell ();

#ifndef SYSFIVE
#define FreeM_timezone -3600
#else

#ifdef __CYGWIN__
#define FreeM_timezone _timezone
#else
extern long FreeM_timezone;
#endif /* __CYGWIN__ */

#endif /* SYSFIVE */



int
main (argc, argv)
int     argc;			/* arguments count     */
char  **argv;			/* arguments string    */

{
    static char locktab[80] = "/usr/tmp/locktab";	/* file with LOCKs */
    static long rempid = 0L;		/* remove entry with rem PID */
    short   ltab;			/* file descr. for locktab */
    int     pid;
    short   type;
    char    ch;
    char    line[300], varnam[300];
    int     i, j, n;

    if (argc > 1) {
        j = 0;
        while (--argc > 0) {
            j++;
            if (argv[j][0] == '-') {
                if (rempid) {
                    fprintf (stderr, "usage is: %s [-pid] [lockfile]\n", *argv);
                    exit (0);
                }
                n = 0;
                rempid = 0L;
                while ((ch = argv[j][++n])) {
                    if (ch < '0' || ch > '9') {
                        fprintf (stderr, "usage is: %s [-pid] [lockfile]\n", *argv);
                        exit (0);
                    }
                    rempid = rempid * 10 + ch - '0';
                }
                continue;
            }
            strcpy (locktab, *(argv + j));
        }
    }
    if (rempid)
        unlock (locktab, rempid);
    while ((ltab = open (locktab, 0)) == -1) {
        printf ("cannot open '%s'\n", locktab);
        exit (0);
    }

    lseek (ltab, 0L, 0);
    for (;;)
    {
        read (ltab, line, 3);
        pid = UNSIGN (line[0]) * 256 + UNSIGN (line[1]);
        if (pid == 0)
            break;
        type = line[2];
        i = 0;
        do {
            read (ltab, &ch, 1);
            if (ch == EOF)
                goto done;
            varnam[i++] = ch;
        } while (ch != EOL);
        zname (line, varnam);
        printf ("%d\t%s %s\n", pid, type == 'D' ? "ZA" : "L ", line);
    }

done:
    close (ltab);
    exit (0);
}

void
unlock (locktab, pid)			/* unLOCK all entries of pid */
char   *locktab;		/* locktable */
long    pid;			/* process ID */

{
short   ltab;			/* file descr. for locktab */
    int     cpid;
    char    entry[256];
long int r_pos;			/* position to read  */
long int w_pos;			/* position to write */
    int     i,
    j;

/*      open locktab, quit if nothing to be done */
    if ((ltab = open (locktab, 2)) == -1)
        return;

/*      request exclusive access to locktab (read & write) */
    locking (ltab, 1, 0L);

/*      free all of your own locks; we do it by copying the whole stuff */
/*      within 'locktab' omitting the old entries under our PID         */
j = 0;				/* count your old entries */
    lseek (ltab, 0L, 0);
    w_pos = 0L;
    for (;;)
    {
        read (ltab, entry, 2);
        cpid = UNSIGN (entry[0]) * 256 + UNSIGN (entry[1]);
        if (cpid == 0) {
            lseek (ltab, w_pos, 0);
            write (ltab, entry, 2);
            break;
        }
        i = 1;
        do {
            read (ltab, &entry[++i], 1);
        } while (entry[i] != EOL);
        i++;
        if (cpid != pid) {
            if (j) {
                r_pos = tell (ltab);
                lseek (ltab, w_pos, 0);
                write (ltab, entry, (unsigned) i);
                lseek (ltab, r_pos, 0);
            }
            w_pos += i;
        } else
        j++;
    }
    locking (ltab, 0, 0L);
    close (ltab);
    return;
}					/* end lock() */
