/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mtok.c
 *    Token lookup 
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include <string.h>

#include "mpsdef.h"
#include "mtok.h"

tok_lut toktab[] = {
    {0, "ABLOCK"},
    {'a', "ASSIGN"},
    {1, "ASTART"},
    {2, "ASTOP"},
    {3, "AUNBLOCK"},
    {'b', "BREAK"},
    {'c', "CLOSE"},
    {'d', "DO"},
    {4, "DO"},
    {'e', "ELSE"},
    {5, "ESTART"},
    {6, "ESTOP"},
    {7, "ETRIGGER"},
    {'f', "FOR"},
    {'g', "GOTO"},
    {'h', "HA"},
    {8, "HALT"},
    {9, "HANG"},
    {'i', "IF"},
    {'j', "JOB"},
    {'k', "KILL"},
    {10, "KVALUE"},
    {11, "KSUBSCRIPTS"},
    {'l', "LOCK"},
    {'m', "MERGE"},
    {'n', "NEW"},
    {'o', "OPEN"},
    {'q', "QUIT"},
    {'r', "READ"},
    {12, "RLOAD"},
    {13, "RSAVE"},
    {'s', "SET"},
    {14, "TCOMMIT"},
    {15, "THEN"},
    {16, "TRESTART"},
    {17, "TROLLBACK"},
    {18, "TSTART"},
    {'u', "USE"},
    {'v', "VIEW"},
    {'w', "WRITE"},
    {'x', "XECUTE"},
    {'A', "ZALLOCATE"},
    {'B', "ZBREAK"},
    {'D', "ZDEALLOCATE"},
    {'G', "ZGO"},
    {'H', "ZHALT"},
    {'I', "ZINSERT"},
    {'J', "ZJOB"},
    {'L', "ZLOAD"},
    {'N', "ZNAMESPACE"},
    {'P', "ZPRINT"},
    {'Q', "ZQUIT"},
    {'R', "ZREMOVE"},
    {'S', "ZSAVE"},
    {'T', "ZTRAP"},
    {'W', "ZWRITE"},
    {20, "ZWATCH"},
    {21, "ZASSERT"},
    {SP, "PRIVATE"},
    {255, NULL}
};

int mtok_token_to_command (char *buf, int token)
{
    tok_lut *p;
    
    for (p = toktab; p->cmd_name != NULL; ++p) {

        if (p->tok == token) {
            strcpy (buf, p->cmd_name);            
            stcnv_c2m (buf);
            return 1;
        }

    }

    return -1;
}

int mtok_command_to_token (char *cmd_name)
{
    tok_lut *p;

    for (p = toktab; p->cmd_name != NULL; ++p) {

        if (strcmp (cmd_name, p->cmd_name) == 0) {
            return p->tok;
        }

    }

    return -1;
}
