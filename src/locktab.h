/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   locktab.h
 *    lock table data structures
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(__FREEM_LOCKTAB_H)
# define __FREEM_LOCKTAB_H

#define NREF_MAXLEN 800

typedef struct locktab_ent_t {

    char namespace[255];
    char nref[NREF_MAXLEN];

    int tp_level;
    pid_t owner_job;
    int ct;
    
    struct locktab_ent_t *next;
    
} locktab_ent_t;

/* function prototypes */
extern void locktab_init(void);
extern void lock(char *lockarg, long time_out, char type);
extern void locktab_increment(char *key, long timeout, short old_lock);
extern void locktab_decrement(char *key, long timeout);
extern void locktab_unlock_all(void);
extern locktab_ent_t *locktab_find(char *key);
extern locktab_ent_t *locktab_insert(char *key);
extern int locktab_count(char *key);
extern unsigned long locktab_pages(void);
extern unsigned long locktab_bytes(void);
extern short locktab_get_sem(void);
extern void locktab_release_sem(void);
extern void locktab_dump(void);

#endif
