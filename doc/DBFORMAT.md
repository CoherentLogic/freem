# Copyright and License

This document is Copyright (C) 2020 John P. Willis

Permission is granted to copy, distribute and/or modify this document under the 
terms of the GNU Free Documentation License, Version 1.3 or any later version 
published by the Free Software Foundation; with no Invariant Sections, with no 
Front-Cover texts, and with no Back-Cover Texts.


# Global Handler

The database is organized in a B* tree structure in disk files, each file directly
corresponding to a single M global.

On-disk, the arrangement is `/var/local/freem/<namespace-name>/<global-name>`, 
where `<global-name>` would be something like `^DD`.

Percent globals (`^%...`) conventionally reside in the `SYSTEM` namespace.

Non-percent globals (`^...`) reside in other namespaces, or the `USER` namespace
by default.

## Blocks

Blocks are `BLOCKLEN` bytes long (1024 in the current implementation).

## Relevant Source Files

* global.c (see especially the function `global()`)

## Relevant Constants

### Block Offsets

Each of the following constants describe a byte offset to a particular type of 
data within a single block.

```
| CONSTANT | DESCRIPTION                                    | BLOCK TYPE       |
+----------+------------------------------------------------+------------------+
| LLPTR    | Left-Link Pointer                              | DATA, PTR, BPTR  |
| RLPTR    | Right-Link Pointer                             | DATA, PTR, BPTR  |
| NRBLK    | Number of Blocks                               | ROOT             |
| COLLA    | Collation (1: numeric, 2: alphabetical)        | ROOT             |
| FREE     | Free Block Count                               | ROOT             |
| BTYP     | Block Type (2: PTR, 6: BPTR, 8: DATA)          | ALL              |
| OFFS     | Offset to Unused Portion of Data Area          | ALL              |
```

### Other Constants

```
| CONSTANT | DESCRIPTION                                    |
+----------+------------------------------------------------+
| DATALIM  | Length of block data area (offsets 0-1013)     |
| BLOCKLEN | Length of block (currently 1024)               |
```

## Root Block Format

```
| STARTING OFFSET   | ENDING OFFSET    | DESCRIPTION                         |
+-------------------+------------------+-------------------------------------+
| 0                 | 1013 (DATALIM)   | Block Data Area                     |
| 1014 (NRBLK)      | 1016             | Number of Blocks                    |
| 1017 (COLLA)      | 1017             | Collation (0 numeric, 1 alpha)      |
| 1018 (FREE)       | 1020             | Free Block Count                    |
| 1021 (BTYP)       | 1021             | Block Type (2 PTR 6 BPTR 8 DATA)    |
| 1022 (OFFS)       | 1023             | Ptr to free part of block data area | 
```

## Data Block Format

```
| STARTING OFFSET   | ENDING OFFSET    | DESCRIPTION                          |
+-------------------+------------------+--------------------------------------+
| 0                 | 0                | Length of Key - Offset to Prev. Key  |
| 1                 | 1                | Offset to Prev. Key                  |
| 2                 | < DATALIM        | Compressed Key                       |
| <VARIES>          | < DATALIM        | Length of Global Data (2 bytes)      |
| <VARIES>          | < DATALIM        | Global Data                          |
| 1014 (LLPTR)      | 1016             | Left-Link Pointer                    |
| 1017              | 1017             | <RESERVED>                           |
| 1018 (RLPTR)      | 1020             | Right-Link Pointer                   |
| 1021 (BTYP)       | 1021             | Block Type (2: PTR, 6: BPTR, 8: DATA)| 
| 1022 (OFFS)       | 1023             | Ptr to free part of block data area  |
```

## Pointer Block Format

```
| STARTING OFFSET   | ENDING OFFSET    | DESCRIPTION                          |
+-------------------+------------------+--------------------------------------+
| 0                 | 0                | Length of Key - Offset to Prev. Key  |
| 1                 | 1                | Offset to Prev. Key                  |
| 2                 | < DATALIM        | Compressed Key                       |
| <VARIES>          | (2 Bytes)        | Forward Pointer (to ptr or data blk) |
| 1017              | 1017             | <RESERVED>                           |
| 1021 (BTYP)       | 1021             | Block Type (2 or 6)                  | 
| 1022 (OFFS)       | 1023             | Ptr to free part of block data area  |
```