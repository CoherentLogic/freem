/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   jnldefs.h
 *    Data structures and constants for journaling
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#define FRM_JNL_VERSION 2

#define JNLA_TSTART 0
#define JNLA_TRESTART 1
#define JNLA_TROLLBACK 2
#define JNLA_TCOMMIT 3
#define JNLA_SET 4
#define JNLA_KILL 5
#define JNLA_CHECKPOINT_OK 6
#define JNLA_CHECKPOINT_FAIL 7
#define JNLA_CHECKPOINT_RESTORE 7
#define JNLA_CHECKPOINT_REMOVE 8

/* replication status values */
#define REPL_STAT_AWAIT 0
#define REPL_STAT_SENDING 1
#define REPL_STAT_OK 2
#define REPL_STAT_FAILED 3

typedef struct jnl_hdr_t {
    char magic[5]; 	/* FRMJL */
    short fmt_version; 	/* FRM_JNL_VERSION */
} jnl_hdr_t;

typedef struct jnl_ent_t {

    char host_id[256];
    unsigned long tran_id;

    time_t ts; /* timestamp */
    pid_t pid;
    short repl_stat; /* can be any of the REPL_STAT_ constants defined above */
    short action; /* can be any of the JNLA_ constants defined above */

    char key[1024];
    char data[1024];

} jnl_ent_t;
