/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   merr.h
 *    freem error codes
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#ifndef _MERR_H
# define _MERR_H

/* error codes */
#define OK            0
#define INRPT         1
#define BKERR         2
#define NOSTAND       3
#define UNDEF         4
#define LBLUNDEF      5
#define MISSOPD       6
#define MISSOP        7
#define ILLOP         8
#define QUOTER        9
#define COMMAER      10
#define ASSIGNER     11
#define ARGER        12
#define SPACER       13
#define BRAER        14
#define LVLERR       15
#define DIVER        16
#define ILLFUN       17
#define FUNARG       18
#define ZTERR        19
#define NEXTER       20
#define SELER        21
#define CMMND        22
#define ARGLIST      23
#define INVEXPR      24
#define INVREF       25
#define MXSTR        26
#define TOOPARA      27
#define NOPEN        28
#define NODEVICE     29
#define PROTECT      30
#define GLOBER       31
#define FILERR       32
#define PGMOV        33
#define STKOV        34
#define STORE        35
#define NOREAD       36
#define NOWRITE      37
#define NOPGM        38
#define NAKED        39
#define SBSCR        40
#define ISYNTX       41
#define DBDGD        42
#define KILLER       43
#define HUPER        44
#define MXNUM        45
#define NOVAL        46
#define TYPEMISMATCH 47
#define MEMOV        48	

/* socket errors */
#define NAMERES	     49
#define SCKCREAT     50
#define SCKIFAM      51
#define SCKITYP      52
#define SCKIPRT      53
#define SCKCERR      54
#define SCKAERR      55
#define SCKACON      56
#define SCKNCON      57
#define SCKEOPT      58
#define SCKERCV      59
#define SCKESND      60
#define NORPI        61
#define CREDEF       62
#define CMODIFY      63
#define FILEXWR      64
#define INEWMUL      65
#define ZINVHORO     66
#define ECODEINV     80
#define ASSERT       81
#define USERERR      82

/* catch-all for otherwise-unspecified syntax errors */
#define SYNTERR      128

/* pseudo-errors (never reported) */
#define CTRLB        129
#define ASYNC 	     131


/* ANSI X11.1 errors */
#define M1           140
#define M2           141
#define M3           142
#define M4           143
#define M5           144
#define M6           145
#define M7           146
#define M8           147
#define M9           148
#define M10          149
#define M11          150
#define M12          151
#define M13          152
#define M14          153
#define M15          154
#define M16          155
#define M17          156
#define M18          157
#define M19          158
#define M20          159
#define M21          160
#define M22          161
#define M23          162
#define M24          163
#define M25          164
#define M26          165
#define M27          166
#define M28          167
#define M29          168
#define M30          169
#define M31          170
#define M32          171
#define M33          172
#define M34          173
#define M35          174
#define M36          175
#define M37          176
#define M38          177
#define M39          178
#define M40          179
#define M41          180
#define M42          181
#define M43          182
#define M44          183
#define M45          184
#define M46          185
#define M47          186
#define M48          187
#define M49          188
#define M50          189
#define M51          190
#define M52          191
#define M53          192
#define M54          193
#define M55          194
#define M56          195
#define M57          196
#define M58          197
#define M59          198
#define M60          199
#define M75          214
#define M101         240
#define M102         241
#define M103         242
#define M104         243

/* MAXERR = maximum error number plus one */
#define MAXERR  243 + 1
#define MAXVNDERR 49

#define MDCERRDIFF 0 /*91*/
#define ECODEOFFSET 139
#define MDCFIRSTERR 140

#define NESTLEVLS 80


extern int merr_num_to_code (int num, char *code);
extern int merr_code_to_num (char *code);
extern void merr_set_ecode_ierr (void);
extern int merr_set_ecode (char *t_code);
extern void merr_dump (int num, char *rtn, char *nsn, char *tcod, int tpos);
extern void merr_set_iochan_err(int channel, short frm_err, char *err_text);
extern void merr_clear_iochan_err(int channel);

typedef struct merr_stackent {
    char ECODE[256];
    char MCODE[256];
    char PLACE[256];
} merr_stackent;

extern merr_stackent merr_stack[];
extern char etrap[];
extern char ecode[];
extern char user_ecode[];
extern int estack;
extern int etrap_lvl;
extern int merr_topstk;

#endif
