/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_zprocess.c
 *    ^$ZPROCESS ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <dirent.h>

#include "mpsdef.h"
#include "mref.h"
#include "iniconf.h"

void ssvn_zprocess (short action, char *key, char *data)
{
    freem_ref_t *ref = (freem_ref_t *) malloc (sizeof(freem_ref_t));

    char *kbuf = (char *) malloc (STRLEN * sizeof(char));
    char *verb = (char *) malloc (STRLEN * sizeof(char));
    char *proc_path = (char *) malloc (STRLEN * sizeof(char));

    pid_t t_pid;

    NULLPTRCHK(ref,"ssvn_zprocess");
    NULLPTRCHK(kbuf,"ssvn_zprocess");
    NULLPTRCHK(verb,"ssvn_zprocess");
    NULLPTRCHK(proc_path,"ssvn_zprocess");
    
    mref_init (ref, MREF_RT_SSVN, "");
    internal_to_mref (ref, key);

    if (ref->subscript_count < 2) {

        free (kbuf);
        free (ref);
        free (verb);
        free (proc_path);
        
        ierr = INVREF;
        return;
    }

    t_pid = atol(ref->subscripts[0]);
    snprintf (proc_path, STRLEN - 1, "/proc/%d", t_pid);

    strncpy (verb, ref->subscripts[1], STRLEN - 1);


    stcpy (kbuf, key);

    switch (action) {
    
        case get_sym:

            if (strcmp (verb, "EXISTS") == 0) {
                
                if (kill (t_pid, 0) == 0) {
                    snprintf (data, STRLEN - 1, "%d\201", 1);
                }
                else {
                    snprintf (data, STRLEN - 1, "%d\201", 0);
                }

                free (kbuf);
                free (ref);
                free (verb);
                free (proc_path);
                
                ierr = OK;
                return;

            }
            else if (strcmp (verb, "ATTRIBUTE") == 0) {

                char attrib[256];
                char fpath[4096];
                
                FILE *fp;

                strncpy (attrib, ref->subscripts[2], 255);

                snprintf (fpath, 4095, "/proc/%d/%s", t_pid, attrib);

                if ((fp = fopen (fpath, "r")) == NULL) {

                    free (kbuf);
                    free (ref);
                    free (verb);
                    free (proc_path);
 
                    ierr = INVREF;
                    return;
                }

                fgets (data, 256, fp);
                stcnv_c2m (data);

                fclose (fp);

                free (kbuf);
                free (ref);
                free (verb);
                free (proc_path);

                
                ierr = OK;
                return;

            }            

            free (kbuf);            
            free (ref);
            free (verb);
            free (proc_path);

            ierr = INVREF;
            return;

        case set_sym:   

            if (strcmp (verb, "SIGNAL") == 0) {

                int signum;

                stcnv_m2c (data);

                signum = atoi (data);
                kill (t_pid, signum);

                stcnv_c2m (data);

                free (kbuf);
                free (ref);
                free (verb);
                free (proc_path);

                
                ierr = OK;
                return;

                
            }
            else {

                free (kbuf);
                free (ref);
                free (verb);
                free (proc_path);

                ierr = M29;
                return;
            }

        case killone:
        case kill_sym:     
        
            kill (t_pid, 15);

            free (kbuf);
            free (ref);
            free (verb);
            free (proc_path);

            ierr = OK;
            return;
        
        case dat:
        case fra_order:
        case fra_query:
        case bigquery:
        case getnext:
        case m_alias:
        case zdata:
            
        default:

            free (kbuf);
            free (ref);
            free (verb);
            free (proc_path);
            
            ierr = INVREF;
            return;

    }

    free (kbuf);
    free (ref);
    free (verb);
    free (proc_path);
    
    *data = EOL;
    return;
}
