/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   jobtab.h
 *    job table structures
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_FREEM_JOBTAB_H)
# define _FREEM_JOBTAB_H

#if defined(__APPLE__)
# include <sys/time.h>
#endif

#define JFLG_DAEMON        1
#define JFLG_NEW           2
#define JFLG_ALIVE         4
#define JFLG_DEFUNCT       8
#define JFLG_REPLSENDER    16
#define JFLG_REPLRECEIVER  32
#define JFLG_FMADM         64

#define JSTAT_IDLE 0
#define JSTAT_INTERPRETER 1
#define JSTAT_HOUSEKEEPING 2
#define JSTAT_DIRECTMODE 3
#define JSTAT_ERROR 4
#define JSTAT_SHUTDOWN 5

#define JIPCFLG_PENDING    0
#define JIPCFLG_RECEIVED   1

typedef struct ipc_slot_t {

    /* PID of sender */
    pid_t sender_pid;

    /* IPC flags */
    unsigned short flags;

    void *object;
    
    struct ipc_slot_t *next;

} ipc_slot_t;

typedef struct job_slot_t {

    /* process ID */
    pid_t pid;

    /* job flags */
    unsigned short flags;

    /* job status */
    unsigned short status;
    
    /* first IPC slot */
    ipc_slot_t *ipc_head;

    /* timestamp at start */
    time_t start_time;

    /* stop requested */
    pid_t stop_requested;

    /* last error value */
    char last_ecode[20];    
    
    /* next slot */
    struct job_slot_t *next;    

} job_slot_t;

extern short jobtab_get_sem(void);
extern void jobtab_release_sem(void);
extern void jobtab_init(void);
extern job_slot_t *job_init(short is_fmadm);
extern void job_remove(const pid_t pid);
extern void job_request_stop(const pid_t target_pid);
extern void job_set_ecode(const pid_t target_pid, const char *ecode);
extern pid_t job_stop_requested (const pid_t target_pid);
extern void job_request_all_stop(void);
extern void job_signal_all(const int sig);
extern int job_count(void);
extern job_slot_t *job_set_status(const pid_t target_pid, const unsigned short status);
extern void job_gc_mark(void);
extern void job_gc_sweep(void);
extern ipc_slot_t *job_send_ipc(const pid_t receiver_pid, const void *object);
extern job_slot_t *job_get(const pid_t target_pid);
extern void job_dump(void);

#endif
