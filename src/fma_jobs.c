/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   fma_jobs.c
 *    fmadm - jobs
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>

#include "fmadm.h"
#include "freem.h"
#include "jobtab.h"

void get_job_property (char *tbuf, int length, long jobpid, char *property);

int fma_jobs_list (int optc, char **opts)
{
    printf ("\nFreeM Job Listing\n");
    printf (  "-----------------\n\n");

    job_dump ();
    
    return 0;
}

int fma_jobs_remove (int optc, char **opts)
{
    int i;
    pid_t job_pid;
    
    for (i = fma_base_opt; i < optc; i++) {

        job_pid = atol (opts[i]);

        kill (job_pid, SIGTERM);
        
    }

    return 0;
}

int fma_jobs_examine (int optc, char **opts)
{
    long pids[FMA_MAXPID];

    register int i;
    register int j = 0;
    register int pidct = optc - 1;


    char j_uid[512];
    char j_gid[512];
    char j_namespace[512];
    char j_io[512];
    char j_tlevel[512];
    char j_routine[512];


    if (pidct < 1) {
        fprintf (stderr, "usage:  fmadm examine job <namespace> pid1 pid2 ...pidN\n");
        return 1;
    }

    for (i = fma_base_opt; i < optc; i++) {
        if (i == FMA_MAXPID) break;
        pids[j++] = atol (opts[i]);
    }

    printf ("\nFreeM Job Examine\n");
    printf ("-----------------\n\n");

    printf ("PIDs:    ");

    for (i = 0; i < pidct; i++) {
        printf ("%ld ", pids[i]);
    }

    printf ("\n\n");

    /* pid uid gid namespace io tlevel routine */
    printf ("%-8s %-8s %-8s %-12s %-30s %-10s %s\n", "PID", "UID", "GID", "NAMESPACE", "$IO", "$TLEVEL", "ROUTINE");
    printf ("%-8s %-8s %-8s %-12s %-30s %-10s %s\n", "---", "---", "---", "---------", "---", "-------", "-------");

    for (i = 0; i < pidct; i++) {

        get_job_property (j_uid, 511, pids[i], "USER");
        get_job_property (j_gid, 511, pids[i], "GROUP");
        get_job_property (j_namespace, 511, pids[i], "NAMESPACE");
        get_job_property (j_io, 511, pids[i], "$IO");
        get_job_property (j_tlevel, 511, pids[i], "$TLEVEL");
        get_job_property (j_routine, 511, pids[i], "ROUTINE");
        
        printf ("%-8ld %-8s %-8s %-12s %-30s %-10s %s\n", pids[i], j_uid, j_gid, j_namespace, j_io, j_tlevel, j_routine); 


    }

    printf ("\n\n    - %d jobs examined\n\n", pidct);
    
    return 0;

}

void get_job_property (char *tbuf, int length, long jobpid, char *property)
{
    char tkey[512];
    int i;

    snprintf (tkey, 512 - 1, "^$JOB\202%ld\202%s\201\201\201", jobpid, property);
    //stcnv_c2m (key);

    ssvn (get_sym, tkey, tbuf);
    
    for (i = 0; i < length; i++) {
        if (tbuf[i] == '\201') tbuf[i] = '\0';
    }

}
