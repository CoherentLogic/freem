%MAPPING ;
   D S^%DB("M","MAIN","NAME","Main Menu")
   D S^%DB("M","MAIN","DESC","FreeM Mappings Manager")
   D S^%DB("M","MAIN","DEFAULT","GL")
   D SA^%DB("M","MAIN","OPTS","GD","Define Global Mapping")
   D SA^%DB("M","MAIN","OPTS","GR","Remove Global Mapping")
   D SA^%DB("M","MAIN","OPTS","GL","List Global Mappings")
   D SA^%DB("M","MAIN","OPTS","RD","Define Routine Mapping")
   D SA^%DB("M","MAIN","OPTS","RR","Remove Routine Mapping")
   D SA^%DB("M","MAIN","OPTS","RL","List Routine Mappings")
   D SA^%DB("M","MAIN","ACT","GD","GD^%MAPPING")
   D SA^%DB("M","MAIN","ACT","GR","GR^%MAPPING")
   D SA^%DB("M","MAIN","ACT","GL","GL^%MAPPING")
   D SA^%DB("M","MAIN","ACT","RD","RD^%MAPPING")
   D SA^%DB("M","MAIN","ACT","RR","RR^%MAPPING")
   D SA^%DB("M","MAIN","ACT","RL","RL^%MAPPING")
MN D GO^%MENU("M","MAIN")
   Q
GD Q
GR Q
GQ Q
RD Q
RR Q
RL Q