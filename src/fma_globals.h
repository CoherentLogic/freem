/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   fma_globals.h
 *    fmadm - globals
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_FMA_GLOBALS_H)
#define _FMA_GLOBALS_H

#define FALSE   0
#define TRUE    1

#define POINT 28
#define MINUS 26
#define OMEGA 29
#define g_EOL 30
#define ALPHA 31

#define ROOT 1L
    /* length of blocks. status bytes defined as offset to blocklength */
#define BLOCKLEN 1024
#define DATALIM (BLOCKLEN-11)
#define LLPTR   (BLOCKLEN-10)
#define NRBLK    LLPTR
#define RLPTR   (BLOCKLEN- 6)
#define FREE     RLPTR
#define BTYP    (BLOCKLEN- 3)
#define OFFS    (BLOCKLEN- 2)

/* length of blockpointers in bytes */
#define PLEN     3

#define EMPTY    0
#define FBLK     1
#define POINTER  2
#define BOTTOM   6
#define DATA     8

#define PROTECT 30

#define LF      10
#define CR      13

#define NUL      0
#define DEL    127

/* maximum level (i.e. tree depth) */
/* trace limit in globals module, i.e. trees wo'nt grow to the sky */
#define MAXLEV 10
/* error limit */
#define ERRLIM 10000


int fma_globals_list (int optc, char **opts);
int fma_globals_remove (int optc, char **opts);
int fma_globals_examine (int optc, char **opts);
int fma_globals_verify (int optc, char **opts);

#endif
