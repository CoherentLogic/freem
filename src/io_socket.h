/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   io_socket.h
 *    socket i/o support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_IO_SOCKET_H)
# define _IO_SOCKET_H

#if !defined(MSDOS)
# include <sys/socket.h>
# include <arpa/inet.h>
#endif

#if defined(__FreeBSD__) || defined(__OpenBSD__) || defined(_SCO_DS)
# include <netinet/in.h>
#endif

typedef struct io_socket {

    int sck;					/* socket descriptor */
    int typ;					/* type of socket (SOCK_STREAM or SOCK_DGRAM) */

#if !defined(MSDOS)
    struct sockaddr_in srv;		/* remote connection address */    
#endif
    
    short connected;			/* TRUE if connected (only applies if typ == SOCK_STREAM) */

    /* FreeM I/O channel */
    int io_channel;

} io_socket;

extern io_socket *io_sockets[MAXSCK];

extern short msck_open (int channel, char *addr_string);
extern short msck_connect (int channel);
extern short msck_write (int channel, char *buf, short length);
extern short msck_read (int channel, char *buf, long timeout, short timeoutms, short length);
extern short msck_get_terminator (int channel, char *buf);
extern short msck_close (int channel);
#endif
