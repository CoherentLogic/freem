/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mwapi_event.h
 *    mwapi event support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2014, 2020, 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include "config.h"

#if !defined(__MWAPI_EVENT_H) && defined(MWAPI_GTK)
# define __MWAPI_EVENT_H

#include <gtk/gtk.h>

typedef struct mwapi_event {

    char *evclass;
    char *window_name;

    char *entryref;
    
    GtkWidget *window;
    
    
} mwapi_event;

typedef struct mwapi_event_queue_entry {

    mwapi_event *e;

    struct mwapi_event_queue_entry *next;

} mwapi_event_queue_entry;


extern mwapi_event_queue_entry *mwapi_event_queue_head;
extern short evt_sync_enabled;

mwapi_event *mwapi_event_new(char *evclass, char *window_name, GtkWidget *window);
void mwapi_wire_event(mwapi_event *e, char *entryref);
void mwapi_enqueue_event(GtkWidget *widget, gpointer data);
int mwapi_dequeue_events(char *result);
void mwapi_on_merge_complete(char *window_name);

#endif
