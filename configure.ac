#
#                            *
#                           * *
#                          *   *
#                     ***************
#                      * *       * *
#                       *  MUMPS  *
#                      * *       * *
#                     ***************
#                          *   *
#                           * *
#                            *
#
#   configure.ac
#    autoconf rules for FreeM
#
#  
#   Author: John P. Willis <jpw@coherent-logic.com>
#    Copyright (C) 1998 MUG Deutschland
#    Copyright (C) 2020 Coherent Logic Development LLC
#
#
#   This file is part of FreeM.
#
#   FreeM is free software: you can redistribute it and/or modify
#   it under the terms of the GNU Affero Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   FreeM is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU Affero Public License for more details.
#
#   You should have received a copy of the GNU Affero Public License
#   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
#
#

AC_INIT([freem],[0.61.2],[jpw@coherent-logic.com])
AC_CONFIG_HEADERS([src/config.h])
AC_CONFIG_MACRO_DIR([m4])

AX_LIB_READLINE

# check for libm
AC_CHECK_LIB([m], [powf])
AC_CHECK_LIB([m], [pow])

AC_CANONICAL_HOST

# find out if we need to link with -lsocket and/or -lnsl to get
# BSD sockets support

AU_ALIAS([LIB_SOCKET_NSL], [AX_LIB_SOCKET_NSL])

AC_DEFUN([AX_LIB_SOCKET_NSL],
[
        AC_SEARCH_LIBS([gethostbyname], [nsl])
        AC_SEARCH_LIBS([socket], [socket], [], [
                AC_CHECK_LIB([socket], [socket], [LIBS="-lsocket -lnsl $LIBS"],
                [], [-lnsl])])
])

AX_LIB_SOCKET_NSL

#
# check for getopt.h and getopt_long
#
AC_CHECK_HEADERS(getopt.h)
AC_CHECK_FUNCS([getopt_long])

# check for wiringPi headers and library linkage for
# ^$ZRPI ssv on Raspberry Pi SBCs
AC_CHECK_HEADERS(wiringPi.h)

AC_DEFUN([AX_LIB_WIRINGPI],
[
        AC_SEARCH_LIBS([wiringPiSetup], [wiringPi], [], [
                AC_CHECK_LIB([wiringPi], [wiringPiSetup], [LIBS="-lwiringPi $LIBS"],
                [], [-lwiringPi])])
])

AX_LIB_WIRINGPI

AC_ENABLE_SHARED
AC_DISABLE_STATIC
LT_INIT
AM_INIT_AUTOMAKE
AC_CONFIG_FILES(Makefile src/Makefile doc/Makefile)
AC_PROG_CC
AC_OUTPUT
