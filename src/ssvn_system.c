/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_system.c
 *    ^$SYSTEM ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#if defined(HAVE_STDINT_H)
# include <stdint.h>
#endif
#include "mref.h"
#include "mpsdef.h"
#include "shmmgr.h"
#include "iniconf.h"

void ssvn_system_update(void)
{
    FILE *fp;
    char line[255];
    char line_ns[255];

    char k_buf[255];
    char d_buf[255];
    
    fp = fopen (SYSCONFDIR"/freem.conf", "r");

    while (fgets (line, 254, fp) != NULL) {

        if (line[0] == '[') {
            strcpy (line_ns, line + 1);
            line_ns[strlen (line_ns) - 2] = '\0';

            snprintf (k_buf, 254, "^$SYSTEM\202NAMESPACE\202%s\201", line_ns);
            symtab (set_sym, k_buf, "\201");
        }
        
    }

    fclose (fp);

    snprintf (k_buf, 254, "^$SYSTEM\202CHANNELS\202TERMINAL\201");
    snprintf (d_buf, 254, "0,0\201");
    symtab (set_sym, k_buf, d_buf);

    snprintf (k_buf, 254, "^$SYSTEM\202CHANNELS\202FILE\201");
    snprintf (d_buf, 254, "1,%d\201", FIRSTSCK - 1);
    symtab (set_sym, k_buf, d_buf);

    snprintf (k_buf, 254, "^$SYSTEM\202CHANNELS\202SOCKET\201");
    snprintf (d_buf, 254, "%d,%d\201", FIRSTSCK, MAXDEV - 1);
    symtab (set_sym, k_buf, d_buf);

    ssvn_job_update ();
    
}

void ssvn_system(short action, char *key, char *data)
{

    freem_ref_t *r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_system");

    mref_init (r, MREF_RT_SSV, "^$SYSTEM");
    internal_to_mref (r, key);

    if ((r->subscript_count > 2) && (strcmp (r->subscripts[0], "EVENT") != 0) && (strcmp (r->subscripts[0], "MAPPINGS") != 0)) {
        ierr = INVREF;
        goto done;
    }

    switch (action) {
        
        case get_sym:
           
            if (strcmp (r->subscripts[0], "NAMESPACE") == 0) {

                symtab (action, key, data);
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "MAPPINGS") == 0) {

                if ((strcmp (r->subscripts[1], "GLOBAL") == 0) || (strcmp (r->subscripts[1], "ROUTINE") == 0)) {

                    ierr = OK;
                    symtab (action, key, data);

                    break;

                }
                else {

                    ierr = M38;
                    break;

                }

            }
            else if (strcmp (r->subscripts[0], "MAINTENANCE_MODE") == 0) {

                snprintf (data, 511, "%d\201", shm_config->hdr->maintenance_mode);
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "DAEMON_PID") == 0) {

                snprintf (data, 511, "%d\201", shm_config->hdr->first_process);
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "MWAPI") == 0) {
                
#if defined(MWAPI_GTK)
                snprintf (data, 511, "1\201");
#else
                snprintf (data, 511, "0\201");
#endif
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "EVENT") == 0) {

                global (action, key, data);
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "ERRMSG") == 0) {

                int errnum;

                errnum = merr_code_to_num (r->subscripts[1]);

                if (errnum == -1) {
                    ierr = M38;
                    break;
                }
                
                sprintf (data, errmes[errnum]);
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "NAME_LENGTH") == 0) {

                sprintf (data, "255\201");
                ierr = OK;
                break;
                
            }
            else if (strcmp (r->subscripts[0], "DEFPSIZE") == 0) {
                sprintf (data, "%ld\201", DEFPSIZE);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "DEFUDFSVSIZ") == 0) {
                sprintf (data, "%ld\201", DEFUDFSVSIZ);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "DEFNSIZE") == 0) {
                sprintf (data, "%ld\201", DEFNSIZE);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "MAXNO_OF_RBUF") == 0) {
                sprintf (data, "%ld\201", MAXNO_OF_RBUF);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "DEFNO_OF_RBUF") == 0) {
                sprintf (data, "%ld\201", DEFNO_OF_RBUF);
                ierr = OK;
                break;                
            }
            else if (strcmp (r->subscripts[0], "DEFPSIZE0") == 0) {
                sprintf (data, "%ld\201", DEFPSIZE0);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "NO_GLOBLS") == 0) {
                sprintf (data, "%ld\201", NO_GLOBLS);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "NO_OF_GBUF") == 0) {
                sprintf (data, "%ld\201", NO_OF_GBUF);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "NESTLEVLS") == 0) {
                sprintf (data, "%ld\201", NESTLEVLS);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "PARDEPTH") == 0) {
                sprintf (data, "%ld\201", PARDEPTH);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "PATDEPTH") == 0) {
                sprintf (data, "%ld\201", PATDEPTH);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "TRLIM") == 0) {
                sprintf (data, "%ld\201", TRLIM);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "ARGS_IN_ESC") == 0) {
                sprintf (data, "%ld\201", ARGS_IN_ESC);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "ZTLEN") == 0) {
                sprintf (data, "%ld\201", ZTLEN);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "FUNLEN") == 0) {
                sprintf (data, "%ld\201", FUNLEN);
                ierr = OK;
                break;
            }
            else if (strcmp (r->subscripts[0], "STRING_MAX") == 0) {

                sprintf (data, "%ld\201", STRLEN);
                ierr = OK;
                break;

            }
            else if (strcmp (r->subscripts[0], "$NEXTOK") == 0) {

                sprintf (data, "1\201");
                ierr = OK;
                break;

            }
            else if (strcmp (r->subscripts[0], "EOK") == 0) {

                sprintf (data, "1\201");
                ierr = OK;
                break;

            }
            else if (strcmp (r->subscripts[0], "OFFOK") == 0) {

                sprintf (data, "1\201");
                ierr = OK;
                break;

            }
            else if (strcmp (r->subscripts[0], "BIG_ENDIAN") == 0) {

#if defined(HAVE_STDINT_H)                
                volatile uint32_t i = 0x01234567;

                if ((*((uint8_t*)(&i)) == 0x67) == 0) {
                    sprintf (data, "1\201");
                }
                else {
                    sprintf (data, "0\201");
                }
                
                ierr = OK;
                break;
#else
                ierr = M29;
                goto done;
#endif
            }
            else if (strcmp (r->subscripts[0], "ZDATE_FORMAT") == 0) {
                //sprintf (data, "%%x\201");

                get_conf ("SYSTEM", "zdate_format", data);
                stcnv_c2m (data);
                
                break;
            }
            else if (strcmp (r->subscripts[0], "ZTIME_FORMAT") == 0) {
                //sprintf (data, "%%X\201");
                get_conf ("SYSTEM", "ztime_format", data);
                stcnv_c2m (data);
                
                break;
            }
          
            ierr = M29;
            goto done;

        case fra_order:
            
            if (strcmp (r->subscripts[0], "NAMESPACE") != 0) {
                ierr = M29;
                goto done;
            }

            symtab (action, key, data);
            ierr = OK;
            goto done;

        case kill_sym:
        case set_sym:

            if ((strcmp (r->subscripts[0], "MAINTENANCE_MODE") == 0) && (action == set_sym)) {
                shm_config->hdr->maintenance_mode = tvexpr (data);
                ierr = OK;
                goto done;
            }            
            
            if ((strcmp (r->subscripts[0], "EVENT") != 0) && (strcmp (r->subscripts[0], "MAPPINGS") != 0)) {
                ierr = M29;
                goto done;
            }

            if ((strcmp (r->subscripts[0], "MAPPINGS") == 0) && ((strcmp (r->subscripts[1], "GLOBAL") != 0) && (strcmp (r->subscripts[1], "ROUTINE") != 0))) {
                ierr = M38;
                goto done;
            }
            
            global (action, key, data);
            ierr = OK;
            goto done;

        case dat:

            global (action, key, data);
            ierr = OK;
            goto done;
            
        default:

            ierr = INVREF;
            break;

    }

done:

    free (r);
    
    return;
}
