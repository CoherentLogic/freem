/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_character.c
 *    ^$CHARACTER ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "mref.h"
#include "mpsdef.h"

void ssvn_character(short action, char *key, char *data)
{
    freem_ref_t *r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_character");

    mref_init (r, MREF_RT_SSV, "^$CHARACTER");
    internal_to_mref (r, key);

    switch (action) {

        case get_sym:

            if (strcmp (mref_get_subscript (r, 0), "M") != 0) {
                ierr = M38;
                goto done;
            }

            if (strcmp (mref_get_subscript (r, 1), "PATCODE") == 0) {
                ierr = M38;
                goto done;
            }
            
            if (strcmp (mref_get_subscript (r, 1), "IDENT") == 0) {
                ierr = OK;
                *data = '\201';
                goto done;
            }
            else if (strcmp (mref_get_subscript (r, 1), "COLLATE") == 0) {
                ierr = OK;
                *data = '\201';
                goto done;
            }
            else if (strcmp (mref_get_subscript (r, 1), "INPUT") == 0) {

                if (strcmp (mref_get_subscript (r, 2), "M") != 0) {
                    ierr = M38;
                    goto done;
                }

                *data = '\201';
                ierr = OK;
                goto done;
                
            }
            else if (strcmp (mref_get_subscript (r, 1), "OUTPUT") == 0) {

                if (strcmp (mref_get_subscript (r, 2), "M") != 0) {
                    ierr = M38;
                    goto done;
                }

                *data = '\201';
                ierr = OK;
                goto done;
                
            }
            
            ierr = OK;
            goto done;
            
        default:
            ierr = M29;
            goto done;
    }


done:
    free (r);
    
    return;
}
