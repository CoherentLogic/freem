%NET.HTTP ;VCL/JPW-FREEM HTTP SYSTEM LIBRARY; 10/23/2020 6:55 PM
    ;0.0;FreeM;****FREEM**;John P Willis @2020
    ;
    ;                            *
    ;                           * *
    ;                          *   *
    ;                     ***************
    ;                      * *       * *
    ;                       *  MUMPS  *
    ;                      * *       * *
    ;                     ***************
    ;                          *   *
    ;                           * *
    ;                            *
    ;
    ;   %HTTP.m
    ;    FreeM HTTP system library
    ;
    ;  
    ;   Author: John P. Willis <jpw@coherent-logic.com>
    ;    Copyright (C) 1998 MUG Deutschland
    ;    Copyright (C) 2020 Coherent Logic Development LLC
    ;
    ;
    ;   This file is part of FreeM.
    ;
    ;   FreeM is free software: you can redistribute it and/or modify
    ;   it under the terms of the GNU Affero Public License as published by
    ;   the Free Software Foundation, either version 3 of the License, or
    ;   (at your option) any later version.
    ;
    ;   FreeM is distributed in the hope that it will be useful,
    ;   but WITHOUT ANY WARRANTY; without even the implied warranty of
    ;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    ;   GNU Affero Public License for more details.
    ;
    ;   You should have received a copy of the GNU Affero Public License
    ;   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
    ;
    ;
	QUIT
 	;
 	;
GET(URL) ; HTTP GET
	;
 	; SET UP ERROR TRAP
 	;
 	N OET S OET=$ZTRAP S $ZTRAP="GETERR^%HTTP"
 	; INIT TERMINATION SEQUENCE
 	N CH,%,LN S CH=255,^$DEVICE(CH,"TERMINATOR")=$C(13,10),LN=0
 	; PARSE URL
 	N PROTO S PROTO=$P(URL,":",1)
 	N SRV,HOST,PRT S SRV=$P(URL,"/",3) I $L(SRV,":") S PRT=$P(SRV,":",2),HOST=$P(SRV,":",1) E  S HOST=SRV,PRT=80
 	N PTH I $L(URL,"/")>3 S PTH=$P(URL,"/",4) E  S PTH="/"
 	I PROTO'="http" K % W "Protocol "_PROTO_" not supported.",! G GETERR
 	; OPEN CONNECTION TO SERVER
 	O CH:HOST_":"_PRT_":IPV4:TCP"
 	U CH:/CONNECT
 	; SEND GET COMMAND
 	W "GET "_PTH_" HTTP/1.1",!,"Host: "_HOST,!,!
 	;
 	;
GETERR U $P C CH K % S $ZTRAP=OET Q 0
