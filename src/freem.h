/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   freem.h
 *    libfreem data structures and prototypes
 * 
 *    NOTE: Caller must pre-allocate memory for *all* APIs!
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#ifndef _LIBFREEM_H
# define _LIBFREEM_H

#include "merr.h"
#include <sys/types.h>

#if !defined(STRLEN)
# define STRLEN 65535
#endif

#define MREF_RT_LOCAL 0
#define MREF_RT_GLOBAL 1
#define MREF_RT_SSVN 2
#define MREF_RT_SSV MREF_RT_SSVN

#define MREF_ST_NEW 0
#define MREF_ST_INIT 1
#define MREF_ST_ERR 10


typedef struct freem_ref_t {

    /*
     * The 'reftype' field can be one of:
     * 
     *  MREF_RT_LOCAL
     *  MREF_RT_GLOBAL
     *  MREF_RT_SSVN
     */
    short reftype;

    /*
     * The 'name' field is the name of the local variable,
     * global variable, or SSV.
     */
    char name[256];    

    /*
     * Returned data goes in a string, so it is important to make sure
     * that your application accounts for canonical numbers as needed.
     */
    char value[STRLEN];   

    short status;

    unsigned int subscript_count;
    char subscripts[255][256];

} freem_ref_t;

typedef struct freem_ent_t {

    /* name of function or procedure entry point */
    char name[256];

    /* return value */
    char value[STRLEN];

    /* value of ierr on return */
    short status;

    /* argument count and array */
    unsigned int argument_count;
    char arguments[255][256];

} freem_ent_t;

#ifndef TRUE
# define TRUE 1
#endif

#ifndef FALSE
# define FALSE 0
#endif

#ifdef _cplusplus
 extern "C" {
#endif

extern pid_t freem_init(char *namespace_name);
extern short freem_version(char *result);
extern short freem_set(freem_ref_t *ref);
extern short freem_get(freem_ref_t *ref);
extern short freem_kill(freem_ref_t *ref);
extern short freem_data(freem_ref_t *ref);
extern short freem_order(freem_ref_t *ref);
extern short freem_query(freem_ref_t *ref);
extern short freem_lock(freem_ref_t *ref, long timeout);
extern short freem_unlock(freem_ref_t *ref);
extern short freem_tstart(char *tp_id, short serial, short restartable, char **sym_save);
extern short freem_trestart(void);
extern short freem_trollback(int tp_levels);
extern short freem_tcommit(void);
extern int freem_tlevel(void);
extern short freem_function(freem_ent_t *ent);
extern short freem_procedure(freem_ent_t *ent);

#ifdef _cplusplus
 }
#endif

#endif
