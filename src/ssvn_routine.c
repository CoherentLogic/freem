/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_routine.c
 *    ^$ROUTINE ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>

#include "mpsdef.h"
#include "mref.h"


void ssvn_routine_add_dir(char *);

void ssvn_routine_add_dir(char *rtndir)
{
    DIR *dir;
    struct dirent *ent;
    char filename[STRLEN];
    char rtnname[256];
    char *rtnext;
    
    char k_buf[512];
    char d_buf[512];
    char t_buf[512];
    
    dir = opendir (rtndir);
    while ((ent = readdir (dir)) != NULL) {

        strncpy (filename, ent->d_name, STRLEN - 1);

        rtnext = ent->d_name + (strlen (ent->d_name) - 2);
        
        if (strcmp (rtnext, ".m") == 0) {

            strcpy (rtnname, ent->d_name);
            rtnname[strlen (ent->d_name) - 2] = '\0';
            
            snprintf (k_buf, 512 - 1, "^$ROUTINE\202%s\202CHARACTER\201", rtnname);
            snprintf (d_buf, 512 - 1, "M\201");
            symtab (set_sym, k_buf, d_buf);

            snprintf (k_buf, 512 - 1, "^$ROUTINE\202%s\202NAMESPACE\201", rtnname);
            if (rtnname[0] == '%') {
                snprintf (d_buf, 512 - 1, "SYSTEM\201");
            }
            else {
                snprintf (d_buf, 512 - 1, "%s\201", nsname);
            }

            symtab (set_sym, k_buf, d_buf);

            snprintf (k_buf, 512 - 1, "^$ROUTINE\202%s\202PATH\201", rtnname);

            if (rtnname[0] == '%') {
                stcpy (t_buf, rou0plib);
            }
            else {
                stcpy (t_buf, rou0path);
            }

            stcnv_m2c (t_buf);
            snprintf (d_buf, 512 - 1, "%s/%s\201", t_buf, filename);

            symtab (set_sym, k_buf, d_buf);
            
        }

    }

    closedir (dir);

}

void ssvn_routine_update(void)
{
    
    char t_buf[STRLEN];
    char k_buf[STRLEN];

    snprintf (k_buf, STRLEN - 1, "^$ROUTINE\201");
    
    stcpy (t_buf, rou0path);
    stcnv_m2c (t_buf);
    
    ssvn_routine_add_dir (t_buf);

    stcpy (t_buf, rou0plib);
    stcnv_m2c (t_buf);

    ssvn_routine_add_dir (t_buf);
    
    return;
    
}

void ssvn_routine(short action, char *key, char *data)
{

    freem_ref_t *r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_routine");

    mref_init (r, MREF_RT_SSV, "^$ROUTINE");
    internal_to_mref (r, key);
    
    ssvn_routine_update ();

    switch (action) {

        case get_sym:           
            
            symtab (action, key, data);
            ierr = OK;
            goto done;


        case fra_order:

            if (r->subscript_count > 2 || r->subscript_count < 1) {
                ierr = M29;
                goto done;
            }

            if (r->subscript_count == 1) {

                /* likely ordering over routine names only */
                symtab (action, key, data);

                ierr = OK;
                goto done;

            }

            if (r->subscript_count == 2) {

                ierr = M38;
                goto done;
                
            }
        
    }

    
done:

    free (r);
    
    return;
}
