#!/bin/sh

echo "cleanup_shm.sh:  cleaning up shared memory..."
ipcrm -M 0x00bbdd20

echo "cleanup_shm.sh:  cleaning up semaphores..."
ipcrm -S 0x00bbdd21
ipcrm -S 0x00bbdd22
ipcrm -S 0x00bbdd23
ipcrm -S 0x00bbdd24

echo "Done."
