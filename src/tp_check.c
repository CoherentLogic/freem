/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   tp_check.c
 *    TP database checkpointing code
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2022 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include <string.h>
#include "tp_check.h"
#include "mpsdef.h"
#include "transact.h"
#include "journal.h"

short frm_global_exists(char *, char *, char *);

cptab *cptab_head[TP_MAX_NEST];

cptab *cptab_insert(int tlevel, char *global)
{
    cptab *t;
    char mode;

    short g_exists;
    
    char *gc_ns;
    char *gc_pth;
    
    gc_ns = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(gc_ns,"cptab_insert");

    gc_pth = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(gc_pth,"cptab_insert");

    for (t = cptab_head[tlevel]; t != NULL; t = t->next) {

        if ((strcmp (t->global, global) == 0) && (t->mode > CP_UNUSED)) {
            /* found match */
            return t;
        }

    }

    /* insert */
    t = (cptab *) malloc (sizeof (cptab));
    NULLPTRCHK(t,"cptab_insert");

    t->global = (char *) malloc (sizeof (char) * (strlen (global) + 1));
    NULLPTRCHK(t->global,"cptab_insert");

    strcpy (t->global, global);

    g_exists = frm_global_exists (gc_ns, gc_pth, global);
    
    t->file = (char *) malloc (sizeof (char) * (strlen (gc_pth)));
    NULLPTRCHK(t->file,"cptab_insert");

    t->cp_file = (char *) malloc (sizeof (char) * STRLEN);
    NULLPTRCHK(t->cp_file,"cptab_insert");
    
    strcpy (t->file, gc_pth);
    stcnv_m2c (t->file);
    
    snprintf (t->cp_file, STRLEN - 1, "%s.%d.%d.chk", t->file, pid, tp_level);

    free (gc_ns);
    free (gc_pth);
    
    if (!g_exists) {
        t->mode = CP_REMOVE;
    }
    else {
        t->mode = CP_RESTORE;
    }

    t->next = cptab_head[tlevel];
    cptab_head[tlevel] = t;

    return t;
}

short cptab_precommit(int tlevel)
{
    cptab *t;
    char *cmd;
    char *pctmp;
    int rc;
    
    cmd = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(cmd,"cptab_precommit");

    pctmp = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(pctmp,"cptab_precommit");
    
    for (t = cptab_head[tlevel]; t != NULL; t = t->next) {
        
        if (t->mode == CP_RESTORE) {
            
            snprintf (cmd, STRLEN - 1, "/bin/cp %s %s", t->file, t->cp_file);
            rc = system (cmd);

            if (rc != 0) {

                strcpy (pctmp, t->file);
                stcnv_c2m (pctmp);
                
                jnl_ent_write (JNLA_CHECKPOINT_FAIL, " \201", pctmp);
                
                free (cmd);
                free (pctmp);

                return FALSE;
                
            }
            else {

                strcpy (pctmp, t->file);
                stcnv_c2m (pctmp);
                
                jnl_ent_write (JNLA_CHECKPOINT_OK, " \201", pctmp);

            }

        }
        
    }
    
    free (cmd);
    free (pctmp);
    
    return TRUE;    
}

void cptab_postcommit(int tlevel)
{
    cptab *t;
    char *cmd;
    int rc;

    cmd = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(cmd,"cptab_postcommit");


    for (t = cptab_head[tlevel]; t != NULL; t = t->next) {

        if (t->mode == CP_RESTORE) {
            snprintf (cmd, STRLEN - 1, "/bin/rm -f '%s'", t->cp_file);
            rc = system (cmd);          
        }
        
    }

    cptab_head[tlevel] = NULL;
}

short cptab_rollback(int tlevel)
{
    cptab *t;
    char *cmd;
    int rc;

    cmd = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(cmd,"cptab_rollback");

    for (t = cptab_head[tlevel]; t != NULL; t = t->next) {
        
        switch (t->mode) {

            case CP_REMOVE:
                snprintf (cmd, STRLEN - 1, "/bin/rm -f '%s'", t->file);
                rc = system (cmd);
                break;

            case CP_RESTORE:
                snprintf (cmd, STRLEN - 1, "/bin/cp '%s' '%s'", t->cp_file, t->file);
                rc = system (cmd);

                if (rc != 0) {
                    cptab_head[tlevel] = NULL;
                    free (cmd);
                    return FALSE;
                }
                
                snprintf (cmd, STRLEN - 1, "/bin/rm -f %s", t->cp_file);

                rc = system (cmd);

                if (rc != 0) {
                    cptab_head[tlevel] = NULL;
                    free (cmd);
                    return FALSE;
                }

                break;

                
        }

    }

    cptab_head[tlevel] = NULL;
    
    return TRUE;

}

void cptab_dump(int tlevel)
{
    cptab *gt;
    char cp_mode[15];
    
    printf ("\n  Global database checkpoints:\n");

    printf ("\n   %-30s%-20s%s\n", "GLOBAL", "MODE", "FILES");
    printf ("   %-30s%-20s%s\n", "------", "----", "-----");

    for (gt = cptab_head[tlevel]; gt != NULL; gt = gt->next) {

        switch (gt->mode) {

            case CP_UNUSED:
                strcpy (cp_mode, "CP_UNUSED");
                break;

            case CP_REMOVE:
                strcpy (cp_mode, "CP_REMOVE");
                break;

            case CP_RESTORE:
                strcpy (cp_mode, "CP_RESTORE");
                break;

        }

        if (gt->mode > CP_UNUSED) {
            printf ("   %-30s%-20sIN:   %s\n", gt->global, cp_mode, gt->file); 
        }
        else {
            printf ("   N/A\n");
        }
        
        if (gt->mode == CP_RESTORE) {
            printf ("   %-30s%-20sOUT:  %s\n", "", "", gt->cp_file);
        }
        
    }
}

