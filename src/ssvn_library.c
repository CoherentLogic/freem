/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_library.c
 *    ^$LIBRARY ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020, 2023 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <ctype.h>
#include <errno.h>

#include "mpsdef.h"
#include "mref.h"

void ssvn_library_add_dir(char *);

void ssvn_library_add_dir(char *libdir)
{

    DIR *dir;
    struct dirent *ent;
    char filename[STRLEN];
    char rtnname[256];
    char *rtnext;
    
    char k_buf[512];
    char d_buf[512];
    char t_buf[512];
    register int i;
    
    dir = opendir (libdir);
    while ((ent = readdir (dir)) != NULL) {

        strncpy (filename, ent->d_name, STRLEN - 1);

        rtnext = ent->d_name + (strlen (ent->d_name) - 2);
        
        if ((strcmp (rtnext, ".m") == 0) && (strncmp (filename, "%ul", 3) == 0)) {

            strcpy (rtnname, ent->d_name + 3);
            rtnname[strlen (ent->d_name) - 5] = '\0';

            for (i = 0; i < strlen (rtnname); i++) {
                if (rtnname[i] >= 'a' && rtnname[i] <= 'z') {
                    rtnname[i] -= 32;
                }
            }
            
            snprintf (k_buf, 512 - 1, "^$LIBRARY\202%s\201", rtnname);
            snprintf (d_buf, 512 - 1, " \201");
            symtab (set_sym, k_buf, d_buf);
            
        }

    }

    closedir (dir);

}

void ssvn_library_update(void)
{
    char t_buf[STRLEN];
    
    stcpy (t_buf, rou0plib);
    stcnv_m2c (t_buf);

    ssvn_library_add_dir (t_buf);
    
    return;

}

void ssvn_library(short action, char *key, char *data)
{
    freem_ref_t *r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"ssvn_routine");

    mref_init (r, MREF_RT_SSV, "^$ROUTINE");
    internal_to_mref (r, key);
    
    ssvn_routine_update ();

    switch (action) {

        case get_sym:
        case fra_order:
        case fra_query:
        case bigquery:
        case dat:

            symtab (action, key, data);
            goto done;

        default:

            ierr = M38;
            goto done;

    }

done:
    free (r);

    return;
}

