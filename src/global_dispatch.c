/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   global_dispatch.c
 *    global handler dispatch module
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

#include "mpsdef.h"
#include "mref.h"
#include "events.h"
#include "config.h"
#include "namespace.h"
#include "transact.h"
#include "mdebug.h"
#include "journal.h"
#include "iftab.h"
#include "shmmgr.h"

void (*gbl_u)(short, char *, char *);
void (*gbl_s)(short, char *, char *);

void global_set_engine(char ns, char *engine)
{
            
    
    if ((strcmp (engine, "BUILTIN") != 0) && (strcmp (engine, "BERKELEYDB") != 0)) {

        if (strcmp (engine, "NICKELBACK") == 0) {
            char *j;
            printf ("\r\nERROR:  please go burn your CD collection in fire. Here, have a segfault...\r\n");

            j[2] = '\201';
        }
        
        printf ("\r\nERROR:  '%s' is not a valid global storage engine\r\n", engine);
        ierr = M38;
        
        return;
        
    }

    switch (ns) {

        case 'u':
            strcpy (gbl_u_engine, engine);

            if (strcmp (engine, "BUILTIN") == 0) {
                gbl_u = &global_bltin;
            }
#if defined(GVH_BDB)    
            else if (strcmp (engine, "BERKELEYDB") == 0) {
                gbl_u = &global_bdb;
            }
#endif            
            else {
                fprintf (stderr, "global_set_engine:  invalid global handler '%s' or FreeM built without '%s' global handler support.\r\n", engine, engine);
                fprintf (stderr, "global set_engine:  defaulting to 'BUILTIN' global handler for namespace '%s'.\r\n", nsname);
                gbl_u = &global_bltin;
            }
           
            break;

        case 's':
            strcpy (gbl_s_engine, engine);

            if (strcmp (engine, "BUILTIN") == 0) {
                gbl_s = &global_bltin;
            }
#if defined(GVH_BDB)    
            else if (strcmp (engine, "BERKELEYDB") == 0) {
                gbl_s = &global_bdb;
            }
#endif
            else {
                fprintf (stderr, "global_set_engine:  invalid global handler '%s' or FreeM built without '%s' global handler support.\r\n", engine, engine);
                fprintf (stderr, "global set_engine:  defaulting to 'BUILTIN' global handler for namespace 'SYSTEM'.\r\n");
                gbl_s = &global_bltin;
            }
            
            break;

    }

    ssvn_job_update ();

    ierr = OK;
    return;
    
}

void global (short action, char *key, char *data)
{

    int ierr_sav = OK;

    char *ptr;
    char name[256];
    char old_gloplib[PATHLEN];
    register int i = 0;
    register int j = 0;
    register char ch;

    char *old_ns;
    char *old_value;
    char *new_value;
    char *tk_buf;
    char *mapk_buf;
    char *mapd_buf;
    freem_ref_t *r;
    short extref_flg;
    short mapped_flg;

    /* variables for transaction-in-flight symbol table */
    iftab *ift; 
    char tmpd[256];          

    old_ns = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(old_ns,"global");
    
    old_value = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(old_value,"global");

    new_value = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(new_value,"global");

    tk_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(tk_buf,"global");

    mapk_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(mapk_buf,"global");

    mapd_buf = (char *) malloc (STRLEN * sizeof (char));
    NULLPTRCHK(mapd_buf,"global");
    
    r = (freem_ref_t *) malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"global");

    extref_flg = FALSE;
    mapped_flg = FALSE;

    ptr = key;
    if (key[1] != '$') {
        while ((shm_config->hdr->tp_owner != pid) && (shm_config->hdr->tp_owner != 0)) {
            sleep (1);
        }
    }
    
    stcpy (tk_buf, key);
    
    if (key[1] != '$') frm_process_alias (key);

    if (key[1] != '$') {
        /* process mappings */
        ierr_sav = OK;
        
        while ((ch = *(ptr++)) != DELIM && ch != EOL && i < 256) {
            name[i++] = ch;
        }
        
        name[i] = '\0';
        
        snprintf (mapk_buf, STRLEN - 1, "^$SYSTEM\202MAPPINGS\202GLOBAL\202%s\201", name);
        global (get_sym, mapk_buf, mapd_buf);

        stcnv_m2c (mapd_buf);
        
        if (ierr == OK) {
            mapped_flg = TRUE;
            strncpy (old_ns, nsname, STRLEN - 1);
            set_namespace (mapd_buf, 0);
            strncpy (old_gloplib, gloplib, PATHLEN - 1);
            strncpy (gloplib, glopath, PATHLEN - 1);
        }
        else {
            ierr = OK;
        }
    }
        
    if (dbg_enable_watch && (action == set_sym)) dbg_fire_watch (key);
    
    if ((jnl_enabled == TRUE) && (key[1] != '$') && (tp_committing == FALSE)) {
        switch (action) {
            
            case set_sym:
                jnl_ent_write (JNLA_SET, key, data);
                break;

            case kill_sym:
            case killone:
                jnl_ent_write (JNLA_KILL, key, data);
                break;
                
        }
    }

    /* If we are writing to a global in a transaction but not committing it,
       queue up the operation at the current transaction level.*/
    if ((tp_level > 0) && (action == set_sym ||
                           action == kill_sym ||
                           action == killone ||
                           action == merge_sym) && (tp_committing == FALSE)) {        
        
        if (key[1] != '$') {
            tp_add_op (FALSE, action, key, data);

            ierr = OK;
            return;
        }
        
    }

    /* in a transaction and retrieving */
    if ((tp_level > 0) && (action == get_sym)) {

        /* check transactions-in-flight symbol table */
        ift = iftab_retrieve (key, tmpd);

        if (ift != NULL) {

            if (ift->killed == TRUE) {
                ierr = M7;
            }
            else {
                stcpy (data, ift->data);
                ierr = OK;
            }

            return;
        }

        /* if we got here, the node referenced by "key" has not been
           modified in the current transaction, so we can proceed to
           the normal disk block retrieval. */
    }

    
    mref_init (r, MREF_RT_GLOBAL, "^DUMMY");
    internal_to_mref (r, key);

    if (rtn_dialect () == D_M77) {

        for (i = 0; i < r->subscript_count; i++) {
            for (j = 0; j < strlen (r->subscripts[i]); j++) {
                
                ch = r->subscripts[i][j];
                
                if (!isdigit (ch)) {
                    ierr = NOSTAND;
                    return;
                }
                
            }
        }
        
    }

    
    if ((r->name[1] == '|')) {

        /* this is an mdc-style extended reference */
        
        char old_code[512];
        char *old_codptr;
        char newns[256];       
        register int i = 0;
        char *nsstart;
        char *nssave;
        char ch;
        char *subprt;
        extref_flg = TRUE;

        /* save off current interpreter state (code and codptr)
           so we can call expr() without losing the plot */        
        stcpy (old_code, code);
        old_codptr = codptr;

        /* save off the current namespace */
        strncpy (old_ns, nsname, STRLEN - 1);
                
        nsstart = r->name + 2;
        nssave = nsstart;

        /* grab everything between the vertical bars */
        while ((ch = *(nsstart++)) != '|') {
            newns[i++] = ch;
        }

        newns[i] = '\0';

        /* load up the namespace expression into the code buffer */
        strcpy (code, newns);
        strcat (code, " ");        
        stcnv_c2m (code);

        /* point the code pointer at the beginning of code */
        codptr = code;

        /* parse a string expression */
        expr (STRING);

        /* expr (STRING) stores its result in argptr */
        stcpy (newns, argptr);
        stcnv_m2c (newns);

        /* restore interpreter state */
        stcpy (code, old_code);
        codptr = old_codptr;

        /* grab the rest of the gvn */
        subprt = nssave + i + 1;
        snprintf (r->name, 255, "^%s", subprt);

        /* get a fully-formed key */
        key = mref_to_internal (r);

        /* switch to the namespace specified in the extended reference */
        set_namespace (newns, 0);
        if (ierr > OK) {
            
            if (ierr == M26) {
                /* if namespace doesn't exist, go back to the
                   original one and raise M26 */
                set_namespace (old_ns, 0);
                ierr = M26;
            }
            
            return;
        }
    }

    switch (r->name[1]) {

        case '%':
        case '$':
            
            if ((action % 2) == 0) {
                (*gbl_s)(get_sym, tk_buf, old_value);
                stcpy (new_value, data);
                ierr = OK;
            }
                      
            (*gbl_s)(action, key, data);
            ierr_sav = ierr;
            break;

        default:

            if ((action % 2) == 0) {
                (*gbl_u)(get_sym, tk_buf, old_value);
                stcpy (new_value, data);
                ierr = OK;
            }
            
            (*gbl_u)(action, key, data);
            ierr_sav = ierr;
            break;

    }

    if ((extref_flg == TRUE) || (mapped_flg == TRUE)) {

        if (mapped_flg == TRUE) {
            strncpy (gloplib, old_gloplib, PATHLEN - 1);
        }

        set_namespace (old_ns, 0);

    }
    
    if (evt_async_enabled && r->reftype == MREF_RT_GLOBAL) {
        char an[20];
        char ev_id[512];

        char *k_buf = (char *) malloc (STRLEN * sizeof (char));
        char *d_buf = (char *) malloc (STRLEN * sizeof (char));
        NULLPTRCHK(k_buf,"global");
        NULLPTRCHK(d_buf,"global");

        mref_to_external (r, d_buf);
        stcnv_c2m (d_buf);
        
        switch (action) {
            case set_sym:
                sprintf (an, "SET");
                break;
            case kill_sym:
            case kill_all:
            case killexcl:
            case killone:
                sprintf (an, "KILL");
                break;
            case get_sym:
                sprintf (an, "GET");
                break;
            case dat:
                sprintf (an, "DATA");
                break;
            case fra_order:
                sprintf (an, "ORDER");
                break;
            case fra_query:
            case bigquery:
                sprintf (an, "QUERY");
                break;
            case getinc:
                sprintf (an, "INCREMENT");
                break;
            case getnext:
                sprintf (an, "NEXT");
                break;
            case zdata:
                sprintf (an, "ZDATA");
                break;
        }

        /* NEW ^$EVENT */
        symtab (new_sym, "^$EVENT\201", "\201");
        
        /* populate ^$EVENT("GLVN") with the key */
        snprintf (k_buf, STRLEN - 1, "^$EVENT\202GLOBAL\201");
        symtab (set_sym, k_buf, d_buf);

        if ((action % 2) == 0) {

            snprintf (k_buf, STRLEN - 1, "^$EVENT\202OLD_VALUE\201");
            symtab (set_sym, k_buf, old_value);

            snprintf (k_buf, STRLEN - 1, "^$EVENT\202NEW_VALUE\201");
            symtab (set_sym, k_buf, new_value);

        }
        
        sprintf (ev_id, "%s:%s", an, r->name);
        evt_enqueue (ev_id, EVT_CLS_TRIGGER, 0);

        free (k_buf);
        free (d_buf);
    }

    free (old_value);
    free (new_value);
    free (r);
    free (tk_buf);
    free (old_ns);
    free (mapk_buf);
    free (mapd_buf);
    
    ierr = ierr_sav;
    
}
