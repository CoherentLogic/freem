/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   events.c
 *    event framework
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include <string.h>

#include "mpsdef.h"
#include "events.h"
#include "freem.h"
#include "mref.h"


short evt_async_enabled = FALSE;
short evt_async_restore = FALSE;
short evt_async_initial = FALSE;

short evt_queue_rear = -1;
short evt_queue_front = -1;

evt_entry *evt_queue[EVT_QLENGTH];
short evt_status[EVT_MAX];
int evt_blocks[EVT_MAX];
int evt_depth;

void evt_init (void)
{
    register int i;

    for(i = 0; i < EVT_MAX; i++) {
        evt_status[i] = EVT_S_DISABLED;
        evt_blocks[i] = 0;
    }
   
    evt_async_enabled = FALSE;
    evt_depth = 0;    
}

int evt_ablock (short evt_class) 
{
    register int i;
    int unblocked_evt_count = 0;

    evt_blocks[evt_class]++;
    
    for (i = 0; i < EVT_MAX; i++) {
        if (evt_blocks[i] == 0) unblocked_evt_count++;
    }

    if (unblocked_evt_count == 0) evt_async_enabled = FALSE;

    return evt_blocks[evt_class];
}

int evt_aunblock (short evt_class)
{
    register int i;
    int unblocked_evt_count = 0;

    if (evt_blocks[evt_class]) evt_blocks[evt_class]--;

    for (i = 0; i < EVT_MAX; i++) {
        if (evt_blocks[i] == 0) unblocked_evt_count++;
    }

    if (unblocked_evt_count) evt_async_enabled = TRUE;

    return evt_blocks[i - 1];
}

int evt_enqueue (char *event_id, short evt_class, short interrupt)
{
    evt_entry *e = malloc (sizeof (evt_entry));
    NULLPTRCHK(e,"evt_enqueue");
    
    if (!evt_registered (event_id, evt_class)) {
        free (e);
        return -1;
    }

    strncpy (e->event_id, event_id, 256);
    e->evt_class = evt_class;

    if (evt_queue_rear == EVT_QLENGTH - 1) {
        /* queue overflow */
        free (e);
        return -1;
    }
        
    if (evt_queue_front == -1) evt_queue_front = 0;
        
    evt_queue[++evt_queue_rear] = e;

    if (interrupt && (evt_blocks[evt_class] == 0)) {
        ierr = ASYNC;
    }
    
    return evt_queue_rear;
}

evt_entry *evt_dequeue (void)
{
    evt_entry *e;
    
    if (evt_queue_front == -1 || evt_queue_front > evt_queue_rear) {
        /* queue underflow */
        return NULL;
    }
    
    e = evt_queue[evt_queue_front++];
    
    return e;
}


char *evt_class_name (evt_entry *e) {

    char *evt_name = malloc (15 * sizeof (char));
    NULLPTRCHK(evt_name,"evt_class_name");
    
    switch (e->evt_class) {

        case EVT_CLS_COMM:
            strcpy (evt_name, "COMM");
            break;

        case EVT_CLS_HALT:
            strcpy (evt_name, "HALT");
            break;

        case EVT_CLS_IPC:
            strcpy (evt_name, "IPC");
            break;

        case EVT_CLS_INTERRUPT:
            strcpy (evt_name, "INTERRUPT");
            break;

        case EVT_CLS_POWER:
            strcpy (evt_name, "POWER");
            break;

        case EVT_CLS_TIMER:
            strcpy (evt_name, "TIMER");
            break;

        case EVT_CLS_USER:
            strcpy (evt_name, "USER");
            break;

        case EVT_CLS_WAPI:
            strcpy (evt_name, "WAPI");
            break;

        case EVT_CLS_TRIGGER:
            strcpy (evt_name, "TRIGGER");
            break;

    }

    return evt_name;

}

char *evt_class_name_c (int c) {

    char *evt_name = malloc (15 * sizeof (char));
    NULLPTRCHK(evt_name,"evt_class_name_c");
    
    switch (c) {

        case EVT_CLS_COMM:
            strcpy (evt_name, "COMM");
            break;

        case EVT_CLS_HALT:
            strcpy (evt_name, "HALT");
            break;

        case EVT_CLS_IPC:
            strcpy (evt_name, "IPC");
            break;

        case EVT_CLS_INTERRUPT:
            strcpy (evt_name, "INTERRUPT");
            break;

        case EVT_CLS_POWER:
            strcpy (evt_name, "POWER");
            break;

        case EVT_CLS_TIMER:
            strcpy (evt_name, "TIMER");
            break;

        case EVT_CLS_USER:
            strcpy (evt_name, "USER");
            break;

        case EVT_CLS_WAPI:
            strcpy (evt_name, "WAPI");
            break;

        case EVT_CLS_TRIGGER:
            strcpy (evt_name, "TRIGGER");
            break;

    }

    return evt_name;

}

int evt_get_handlers (char *buf) {

    char *t_key;    
    char *class_name;
    char *t_buf;
    freem_ref_t *r;
    freem_ref_t *rs;
    char pid_s[10];
    int ct;       
    evt_entry *e;

    class_name = malloc (256 * sizeof (char));
    NULLPTRCHK(class_name,"evt_get_handlers");
    
    t_buf = malloc (STRLEN * sizeof (char));
    NULLPTRCHK(t_buf,"evt_get_handlers");

    r = malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"evt_get_handlers");

    rs = malloc (sizeof (freem_ref_t));
    NULLPTRCHK(rs,"evt_get_handlers");

    ct = 0;
    t_buf[0] = '\201';
    
    mref_init (r, MREF_RT_SSVN, "^$JOB");
    mref_init (rs, MREF_RT_SSVN, "^$SYSTEM");
    
    snprintf (pid_s, 9, "%d", pid);
    

    while ((e = evt_dequeue ()) != NULL) {       

        if (evt_status[e->evt_class] > EVT_S_DISABLED) {
            class_name = evt_class_name (e);

            mref_set_subscript (r, 0, pid_s);
            mref_set_subscript (r, 1, "EVENT");
            mref_set_subscript (r, 2, class_name);
            mref_set_subscript (r, 3, e->event_id);

            t_key = mref_to_internal (r);
            ssvn (get_sym, t_key, t_buf);
            free (t_key);
            stcnv_m2c (t_buf);

            if (strlen (t_buf) > 0) {

                ct++;
                
                strcat (buf, t_buf);
                strcat (buf, ",");
            }

            mref_set_subscript (rs, 0, "EVENT");
            mref_set_subscript (rs, 1, class_name);
            mref_set_subscript (rs, 2, e->event_id);

            t_key = mref_to_internal (rs);
            ssvn (get_sym, t_key, t_buf);
            free (t_key);            
            stcnv_m2c (t_buf);
            
            if (strlen (t_buf) > 0) {

                ct++;

                strcat (buf, t_buf);
                strcat (buf, ",");
            }
            
        }

        free (e);

    }

    if (ct) {
        buf[strlen (buf) - 1] = NUL;
    }
    else {
        buf[0] = NUL;
    }

    free (r);
    free (rs);
    free (class_name);
    free (t_buf);

    return ct;
}

short evt_registered (char *event_id, short evt_class)
{
    char *t_key;
    char *class_name;
    char *t_buf;
    freem_ref_t *r;
    freem_ref_t *rs;
    char pid_s[10];
    int ct;
    
    t_key = malloc (256 * sizeof (char));
    NULLPTRCHK(t_key,"evt_registered");
    
    class_name = malloc (256 * sizeof (char));
    NULLPTRCHK(class_name,"evt_registered");
    
    t_buf = malloc (STRLEN * sizeof (char));
    NULLPTRCHK(t_buf,"evt_registered");


    r = malloc (sizeof (freem_ref_t));
    NULLPTRCHK(r,"evt_registered");

    rs = malloc (sizeof (freem_ref_t));
    NULLPTRCHK(rs,"evt_registered");

    ct = 0;
    t_buf[0] = '\201';
    
    mref_init (r, MREF_RT_SSVN, "^$JOB");

    snprintf (pid_s, 9, "%d", pid);
    
    class_name = evt_class_name_c (evt_class);

    mref_set_subscript (r, 0, pid_s);
    mref_set_subscript (r, 1, "EVENT");
    mref_set_subscript (r, 2, class_name);
    mref_set_subscript (r, 3, event_id);

    t_key = mref_to_internal (r);

    ssvn (get_sym, t_key, t_buf);
    stcnv_m2c (t_buf);

    if (strlen (t_buf) > 0) ct++;

    mref_init (rs, MREF_RT_SSVN, "^$SYSTEM");

    mref_set_subscript (rs, 0, "EVENT");
    mref_set_subscript (rs, 1, class_name);
    mref_set_subscript (rs, 2, event_id);

    t_key = mref_to_internal (rs);

    ssvn (get_sym, t_key, t_buf);
    stcnv_m2c (t_buf);

    if (strlen (t_buf) > 0) ct++;
        
    free (r);
    free (rs);
    free (t_key);
    free (class_name);
    free (t_buf);

    return (ct > 0) ? TRUE : FALSE;
}
