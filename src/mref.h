/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mref.h
 *    supporting functions for handling freem_ref_t structures
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_MREF_H)
#define _MREF_H

#include "freem.h"

extern freem_ref_t *mref_init (freem_ref_t *ref, short ref_type, char *name);
extern char *mref_get_subscript (freem_ref_t *ref, int index);
extern freem_ref_t *mref_set_subscript (freem_ref_t *ref, int index, char *value);
extern char *mref_to_internal (freem_ref_t *ref);
extern freem_ref_t *internal_to_mref (freem_ref_t *ref, char *key);
extern void mref_to_external (freem_ref_t *ref, char *buf);

#endif