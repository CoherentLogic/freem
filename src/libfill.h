/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   libfill.h
 *    Provides correct definitions of standard library
 *    functions to get around compiler warnings until 
 *    we can move to using the standard headers.
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#ifndef _LIBFILL_H
# define _LIBFILL_H

void exit(int status);
char *strcat(char *dest, const char *src);

//FILE *popen(const char *command, const char *type);
char *strcpy(char *dest, const char *src);
char *strchr(const char *s, int c);
char *strncpy(char *dest, const char *src, size_t n);
unsigned int sleep(unsigned int seconds);
void free(void *ptr);
size_t strlen(const char *s);
int strncmp(const char *s1, const char *s2, size_t n);
char *strtok(char *str, const char *delim);
void *calloc(size_t nmemb, size_t size);
void *malloc(size_t size);
char *strncat(char *dest, const char *src, size_t n);

#endif