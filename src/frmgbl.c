/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   frmgbl.c
 *    freem global C variables
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

/* needed if byte data are to be interpreted as unsigned integer */
#include <stdlib.h>
#include <setjmp.h>
#include <signal.h>
#include <stdio.h>
#include "mpsdef0.h"
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "transact.h"
#include "locktab.h"
#include <errno.h>

#ifdef LIBFREEM
# include "errmsg.h"
#endif

#define UNSIGN(A) ((A)&0377)

#define g_EOL 30
#define POINT 28
#define MINUS 26

#define ROOT 0L
    /* length of blocks. status bytes defined as offset to blocklength */
#define DATALIM (BLOCKLEN-11)
#define LLPTR   (BLOCKLEN-10)
#define NRBLK    LLPTR
#define RLPTR   (BLOCKLEN- 6)
#define FREE     RLPTR
#define BTYP    (BLOCKLEN- 3)
#define OFFS    (BLOCKLEN- 2)

#define EMPTY    0
#define FBLK     1
#define POINTER  2
#define BOTTOM   6
#define DATA     8

#define PROTECT 30


#ifndef SYSFIVE
    #define FreeM_timezone -3600
#else

    #ifdef __CYGWIN__
        #define FreeM_timezone _timezone
    #else
        long FreeM_timezone;
    #endif /* __CYGWIN__ */

#endif /* SYSFIVE */

#if defined(__CYGWIN__)
# define FreeM_timezone _timezone
#endif

/* mumps commands */
#define BREAK       'b'
#define CLOSE       'c'
#define DO          'd'
#define DO_BLOCK     2
#define ELSE        'e'
#define FOR         'f'
#define GOTO        'g'
#define HA          'h'
#define HALT        '0'
#define HANG        '1'
#define IF          'i'
#define JOB         'j'
#define KILL        'k'
#define LOCK        'l'
#define NEW         'n'
#define OPEN        'o'
#define QUIT        'q'
#define READ        'r'
#define SET         's'
#define USE         'u'
#define VIEW        'v'
#define WRITE       'w'
#define XECUTE      'x'

#define ZALLOCATE   'A'
#define ZBREAK      'B'
#define ZDEALLOCATE 'D'
#define ZGO         'G'
#define ZHALT       'H'
#define ZINSERT     'I'
#define ZJOB        'J'
#define ZLOAD       'L'
#define ZNEW        'N'
#define ZPRINT      'P'
#define ZQUIT       'Q'
#define ZREMOVE     'R'
#define ZSAVE       'S'
#define ZTRAP       'T'
#define ZWRITE      'W'
#define PRIVATE     SP


short run_daemon = FALSE;
short nofork = FALSE;
char *pid_file_path;
int pid_fd;

/* USING and WITH */
char i_using[STRLEN] = {'\201'};
char i_with[STRLEN] = {'\201'};

/* common definitions for all mumps modules           */
/* same as external definition in include_file mpsdef */

extern int errno;           /* external error code for systemcalls */

int     m_argc;             /* arguments count     */
char  **m_argv;             /* arguments string    */
char  **m_envp;             /* environment pointer */

short frm_throw_all_errors = 1;

/* glvn size parameters       */
union four_fl {
long unsigned all;
char    one[4];
} glvnflag;             /* [0] unique name chars          0=no limit */

/* [1] case sensitivity flag      0=sensitive */
/* [2] max. name+subscripts       0=no limit */
/* [3] max. length of a subscript 0=no limit */
int     lonelyflag = FALSE;     /* single user flag */
int     lowerflag = TRUE;       /* lowercase everywhere flag */
int     killerflag = TRUE;      /* SIGTERM handling flag */
int     huperflag = TRUE;       /* SIGHUP handling flag */
int     s_fun_flag = TRUE;      /* VIEW 70: ZSORT/ZSYNTAX flag */
int     n_fun_flag = TRUE;      /* VIEW 71: ZNEXT/ZNAME flag */
int     p_fun_flag = TRUE;      /* VIEW 72: ZPREVIOUS/ZPIECE flag */
int     d_fun_flag = TRUE;      /* VIEW 73: ZDATA/ZDATE flag */
int     zjobflag = TRUE;        /* VIEW 79: old ZJOB vs. new ZJOB flag */
int     eightbit = TRUE;        /* VIEW 80: 7 vs. 8 bit flag */
int     PF1flag = FALSE;        /* VIEW 81: PF1 flag */
int     ordercounter = 0;       /* VIEW 82: order counter */
int     etxtflag = FALSE;       /* VIEW 83: text in $ZE flag */
char    lvndefault[256] = "\201";   /* VIEW 89: UNDEF lvn default */
char    gvndefault[256] = "\201";   /* VIEW 90: UNDEF gvn default */
char    exfdefault[256] = "\201";   /* VIEW 91: missing QUIT expr default */
int     typemmflag = FALSE;     /* VIEW 92: EUR2DEM: type mismatch error */
int     namespace = 0;          /* VIEW 200: namespace index */
int     config = 0;         /* VIEW 201: configuration index */
char    WHR[12][4] = {                  /* names of currencies */
    "\201",
    "EUR\201",
    "ATS\201",
    "BFR\201",
    "DEM\201",
    "ESP\201",
    "FMK\201",
    "FRF\201",
    "IEP\201",
    "ITL\201",
    "NLG\201",
    "PTE\201"
};

char    EUR2WHR[12][9] = {                  /* conversion factors EUR to ... */
    "\201",             /* dont care */
    "1\201",                /* to EUR */
    "13.7603\201",          /* to ATS */
    "40.3399\201",          /* to BFR */
    "1.95583\201",          /* to DEM (DM) */
    "166.386\201",          /* to ESP */
    "5.94573\201",          /* to FMK */
    "6.55957\201",          /* to FRF (FF) */
    ".787564\201",          /* to IEP */
    "1936.27\201",          /* to ITL */
    "2.20371\201",          /* to NLG */
    "200.482\201"           /* to PTE */
};

long    v93 = 1;            /* VIEW 93: ASCII rule default */
char    v93a[NO_V93][2560] = {
    /*     ASCII    */
    " :, :!,A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z\201",
    /* B  - BELGIAN */
    " :, :!,@:a,\\:c,{:e,}:e,\
    A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z\201,\200:e",
    /* D  - GERMAN  */
    " \001\002 \001\002!\001\002\042\001\002#\001\002$\001\002%\001\002&\001\002\
    '\001\002(\001\002)\001\002*\001\002+\001\002,\001\002-\001\002.\001\002\
    /\001\002:\001\002;\001\002<\001\002=\001\002>\001\002?\001\002@\001\002\
    ^\001\002_\001\002`\001\002A\001a\002B\001b\002C\001c\002D\001d\002E\001e\002\
    F\001f\002G\001g\002H\001h\002I\001i\002J\001j\002K\001k\002L\001l\002\
    M\001m\002N\001n\002O\001o\002P\001p\002Q\001q\002R\001r\002S\001s\002\
    T\001t\002U\001u\002V\001v\002W\001w\002X\001x\002Y\001y\002Z\001z\002\
    {\001ae\002[\001ae\002|\001oe\002\134\001oe\002}\001ue\002]\001ue\002\
    ~\001ss\002\200\001e\002\201",
    /* DK - DANISH  */
    " :, :!,{:ae,|:oe,}:au,~:ue,\
    A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z,\
    [:ae,\\:oe,]:ao,^:ue,\200:e\201",
    /* E  - SPANISH */
    " :, :!,|:n,}:c,ll:l,\
    A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z,\200:e,\
    \\:n,LL:l\201",
    /* F  - FRENCH  */
    " :, :!,\\:c,{:e,|:u,}:e,\
    A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z,\200:e,\201",
    /* I  - ITALIAN */
    " :, :!,\\:c,]:e,`:u,{:a,|:o,}:e,~:i,\
    A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z,\200:e,\201",
    /* S  - SWEDISH */
    " :, :!,`:e,{:ae,|:oe,}:ao,~:ue,\
    A:a,B:b,C:c,D:d,E:e,F:f,G:g,H:h,I:i,J:j,K:k,L:l,M:m,N:n,O:o,P:p,Q:q,R:r,S:s,T:t,U:u,V:v,W:w,X:x,Y:y,Z:z,\
    @:e,[:ae,\\:oe,]:ao,~:ue,\200:e,\201"
};


char    glo_prefix[MONTH_LEN] = "^\201";    /* VIEW 96: global prefix */
char    glo_ext[MONTH_LEN] = "\201";    /* VIEW 97: global postfix */
char    rou_ext[MONTH_LEN] = ".m\201";  /* VIEW 98: routine extention */
long    tzoffset = 0L;          /* VIEW 99:  timer offset     */
int     v100 = 0;           /* VIEW 100: return value of kill */
char    l_o_val[256] = "\201";      /* VIEW 110: local $o/$q data value */
char    g_o_val[256] = "\201";      /* VIEW 111: global $o/$q data value */
int     zsavestrategy = TRUE;       /* VIEW 133: remember ZLOAD directory on ZSAVE */

/* vars for screen save/restore     */
struct vtstyp *screen = NULL;       /* active screen */
short   jour_flag = 0;          /* journal flag 0/1/-1              */

/* trace vars for global module     */
unsigned long traceblk[TRLIM];      /* trace stack - block numbers      */
short   traceadr[TRLIM];        /*             - status             */
short   trx;                /*             - stack pointer      */
char    compactkey[256];        /* internal form of key in global.c */

short   mcmnd;              /* mumps command letter */
short   arg;                /* stack pointer for expr.c         */
char   *argstck[PARDEPTH + 1];      /* stack of pointers to             */

/*       intermediate results       */

long    ordercnt = 0L;          /* repeater for $order/$query       */
short   setpiece = FALSE;       /* TRUE: set$piece executing        */
short   setop = 0;          /* SET op flag                      */
char    rou_name[256] =
{EOL};                  /* $T(+0)/$ZN routine name          */
char   *namstck;            /* routine name stack               */
char   *namptr;             /* routine name stack pointer       */
char   *framstck;           /* DO_frame stack                   */
char   *dofrmptr;           /* DO_frame stack pointer           */
char    zb[40] = "\201";        /* $ZB last ESC_sequence            */
char    zerror[300] = "\201";       /* $ZE last error                   */
short   DSM2err = FALSE;        /* enable normal error processing   */
short   nesterr = 0;            /* nesterr and callerr contain info */
char    callerr[NESTLEVLS + 1][40]; /* about call situation at error    */

char    stack0[256] = "\201";

char    zmc[128] = "\
\000\001\002\003\004\005\006\007\010\011\012\013\014\015\016\017\
\020\021\022\023\024\025\026\027\030\031\032\033\034\035\036\037\177\201";

/* $ZMC loadable match 'controls'  */
char    zmn[128] = "0123456789\201";    /* $ZMN loadable match 'numerics'  */

/* $ZMP loadable match 'punctuation' */
char    zmp[128] = " !\042#$%&'()*+,-./:;<=>?@^_`\201";

/* $ZML loadable match 'lowercase' */
char    zml[128] = "abcdefghijklmnopqrstuvwxyz{|}~\201";

/* $ZMU loadable match 'uppercase' */
char    zmu[128] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]\201";

short   zerr = OK;          /* $ZE numeric error code          */
char    zloc[256] = "\201";     /* $ZL last local reference        */
char    zref[256] = "\201";     /* $ZR last global reference       */
short   nakoffs = 0;            /* offset to naked reference       */
char    zfunkey[44][FUNLEN];        /* $ZF function key */

typedef struct frm_devstat {
    short mdc_err;
    short frm_err;
    char err_txt[80];
} frm_devstat;

frm_devstat devstat[MAXDEV + 1]; /* channel statuses for $DEVICE */

short   xpos[MAXDEV + 1];       /* $X-vector                       */
short   ypos[MAXDEV + 1];       /* $Y-vector                       */
short   crlf[MAXDEV + 1];       /* CR/LF flag vector               */
short   fm_nodelay[MAXDEV + 1];     /* nodelay flag vector             */

int     ESCflag[MAXDEV + 1] =
{0, 0, 0, 0, 0};            /* ESC flag                     */

short   RightMargin = 0;        /* Output Margin. Default no       */

/* automatic CR/LF                 */
short   InFieldLen = 255;       /* Input Field length Def: 255 char */
long    DSW = BIT2 + BIT21;     /* Device Status word (Terminal)   */
char    LineTerm[32] = "\012\015\201";  /* Input Line Terminator chars     */
char    BrkKey = 3;         /* <INTERRUPT> key Def: CTRL/C     */
char    ug_buf[MAXDEV + 1][256];    /* ungetc-buffers                  */
char    devopen[MAXDEV + 1] =
{0, 0, 0, 0, 0};            /*  0         not open             */

/* 'r'        input                */
/* 'w' or 'a' output               */

/* names of IO devices */
char    dev[MAXDEV + 1][40] = {
    " ",                    /* HOME  */
    "/usr/tmp/mout.1/a\201",        /* dev 1 */
    "/usr/tmp/mout.2/a\201",        /* dev 2 */
    "/usr/tmp/mout.3/a\201",        /* dev 3 */
    "/usr/tmp/mout.4/a\201"     /* dev 4 */
};

char    G0I[MAXDEV + 1][257];       /* G0 input translation table */
char    G0O[MAXDEV + 1][257];       /* G0 output translation table */
char    G1I[MAXDEV + 1][257];       /* G1 input translation table */
char    G1O[MAXDEV + 1][257];       /* G1 output translation table */

FILE   *opnfile[MAXDEV + 1];
char    act_oucpath[MAXDEV + 1][40] = {"\201", "\201", "\201", "\201", "\201"};
char    sq_modes[MAXDEV + 1];

short   olddes[NO_GLOBLS];      /* filedescr of open global files */
char    oldfil[NO_GLOBLS][40];      /* names of open global files */
long    g_ages[NO_GLOBLS];      /* last access of global files */
short   usage[NO_GLOBLS];       /* usage count of global files */
short   inuse = 0;          /* file in use */

int     lio_mode = -1;
short   io = HOME;          /* $IO */
short   test = FALSE;           /* $TEST */
short   pattrnflag = FALSE;     /* incomplete match flag */
char    pattrnchar = EOL;       /* incomplete match flag supplement */
int     zsystem = 0;            /* $ZSYSTEM return status of UNIX call */
short   zcc = FALSE;            /* $ZC (ControlC-Flag)            */

char   *rouptr;             /* pointer to begin of routine    */
char   *roucur;             /* cursor into routine            */
char   *rouend;             /* pointer to end of pgm          */
char   *rouins;             /* pointer for direct mode insert */
short   breakon = ENABLE;       /* BREAK enable/disable-flag      */
short   zbreakon = DISABLE;     /* ZBREAK enable/disable-flag     */
short   zbflag = FALSE;         /* 'ZBREAK from terminal'-flag    */
short   zprecise = 100;          /* $ZPRECISION of arithmetic      */
char    fp_conversion[10];      /* sprintf conversion constant for ieee754 support */
long    nrandom;                /* random number seed             */
long    ran_a = 24298L;         /* random number parameter a      */
long    ran_b = 99991L;         /* random number parameter b      */
long    ran_c = 199017L;        /* random number parameter c      */

short   usermode = 1;           /* 0=user mode 1=programmer mode  */
int     restricted_mode = FALSE; /* TRUE=restricted FALSE=unrestricted */
short   demomode = FALSE;       /* 0=no demo   1=demo mode        */
int     d0char = DEL;           /* demomode ouput character       */
int     d1char = CAN;           /* demomode '!'   character       */
short   cset = FALSE;           /* 0=mumps set 1='C' set flag     */

/* startup flags */
int     hardcopy = DISABLE;     /* hardcopy flag                   */
int     filter = FALSE;         /* filter flag                     */
int     noclear = FALSE;        /* noclear flag                    */
int     standard = D_FREEM;     /* default dialect */
int     quiet_mode = FALSE;      /* quiet mode */
char    config_file[4096];      /* path to configuration file      */


short fp_mode = 0;             /* set to 0 for fixed-point math, 1 for IEEE754 floating point */
short en_revstrf = 1;          /* enable reverse forms of string intrinsics */

short   ierr;               /* immediate error status          */
short   deferred_ierr;      /* deferred error status (after returning from a private routine) */

long    PSIZE = DEFPSIZE;       /* size of 'partition'             */
char   *partition;          /* partition                       */
long    symlen = DEFPSIZE;      /* 'lower' bound of symbol table   */
unsigned long alphptr[128] =        /* pointers into symbol table      */
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
char   *apartition;         /* alternate partition             */
long    asymlen = DEFPSIZE;     /* 'lower' bound of symbol table   */
unsigned long aalphptr[128] =       /* pointers into symbol table      */
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

short   autopsize = TRUE;       /* automatic increase of PSIZE     */
long    svnlen = DEFUDFSVSIZ;       /* 'lower' bound of udf_svn_tab    */
long    UDFSVSIZ = DEFUDFSVSIZ;     /* size of userdef special var tab. */
char   *svntable;           /* udf special variable table      */
unsigned long svnaptr[128] =        /* pointers into udf_svn_tab       */
{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

short   autousize = TRUE;       /* automatic increase of UDFSVSIZ  */
long    NO_OF_RBUF = DEFNO_OF_RBUF; /* number of routine buffers       */
long    PSIZE0 = DEFPSIZE0;     /* size of routine buffers         */
short   autorsize = TRUE;       /* automatic increase of PSIZE0    */
short   aliases = 0;            /* aliases pointer                 */
char    ali[2000];          /* aliases table                   */
long    v22ptr = 0L;            /* view 22 aliases pointer         */
char   *v22ali;             /* view 22 aliases field           */
long    v22size = 0L;           /* current size of aliases field   */


long    NSIZE = DEFNSIZE;       /* size of newstack                */
char   *newstack;           /* stack for NEWed variables       */
char   *newptr;             /* pointer to NEW stack            */
char   *newlimit;           /* pointer to NEW stack end        */

short   nstx = 0;           /* nest stack:       */
short   nestc[NESTLEVLS + 1];       /* - command (DO...) */
char   *nestp[NESTLEVLS + 1];       /* - cmdptr          */
char   *nestn[NESTLEVLS + 1];       /* - namptr          */
long    nestr[NESTLEVLS + 1];       /* - roucur          */
char   *nestnew[NESTLEVLS + 1];     /* - newptr          */
short   neste[NESTLEVLS + 1];       /* - was this frame entered as the result of an error? */
short   nestlt[NESTLEVLS + 1];      /* stack $T / stack levelcount */
short   brkstk[NESTLEVLS + 1];      /* stack for BREAK information */

char    ztrap[NESTLEVLS + 2][ZTLEN];    /* $ZTRAP to be xecuted on error    */


char   *s;              /* pointer to symlen_offset        */
char   *argptr;             /* pointer to beg of tmp-storage   */

char    code[512] =
{EOL, EOL};             /* currently interpreted code      */
char   *codptr = code;          /* pointer within code[]           */

char    dosave[20];         /* in a FOR range save DO label    */
char   *xdosave;

int     errfunlvl = 0;          /* avoid wrong error message in $$FUN */
short   repQUIT = 0;            /* QUIT repeater     */

char    varnam[256];            /* variable/array/function name  */
char    varerr[256] =
{EOL};                  /* reference in error message    */
char   *buff;               /* routine buffer pool           */
char    pgms[MAXNO_OF_RBUF][40];    /* names of alt.pgms             */
long    ages[MAXNO_OF_RBUF];        /* last call to this pgm         */
char   *ends[MAXNO_OF_RBUF];        /* saved rouend-pointer          */
char    path[MAXNO_OF_RBUF][256];   /* directory where routine was loaded */
rtn_flags rbuf_flags[MAXNO_OF_RBUF]; /* per-routine flags */

char    glopath[PATHLEN];           /* path to access globals        */
char    rou0path[PATHLEN];          /* routine access with DO,GOTO,JOB */
char    rou1path[PATHLEN];          /* routine access with ZL,ZS     */
char    gloplib[PATHLEN];           /* path to access %globals   */

char    gbl_u_engine[255];          /* user global engine */
char    gbl_s_engine[255];          /* system global engine */
char    loc_engine[255];            /* local engine */

unsigned long int bdb_flush_threshold;

char    rou0plib[PATHLEN];          /* %routine path (DO..)     */
char    rou1plib[PATHLEN];          /* %routine path (ZL..)     */
char    oucpath[PATHLEN] = "\201";  /* OPEN/USE/CLOSE path */
char    zargdefname[PATHLEN]= "%\201";  /* default varname for Z-commands */
char    locktab[PATHLEN] = "/usr/tmp/locktab";  /* file with LOCKFs */
char    zallotab[PATHLEN] = "/usr/tmp/locktab";     /* file with ZALLOCATE */
char    hcpyfile[PATHLEN] = "/usr/tmp/hardcopy";    /* hardcopy file */
char    jourfile[PATHLEN] = "/usr/tmp/ioprotocol";  /* journal file */
FILE   *jouraccess;         /* dto. filedes */
char    curdir[256] = ".";      /* current directory */

char    startuprou[PATHLEN] = "\201";   /* start up routine from cmdline*/

char    zcommds[256] =
" za zas zb zd zg zh zi zj zl zn zp zq zr zs zt zwr zallocate zbreak zdeallocate \
zgo zhalt zinsert zjob zload znamespace zprint zquit zremove zsave ztrap zwrite \201";  /* intrinsic z-commands */
char    zfunctions[256] =       /* intrinsic z-functions */
" zb zc zd ze zh zht zk zl zm zn zo zp zr zs zt zboolean zcall zcr zcrc zdata zdate zedit zhorolog \
zkey zlength zlsd zname znext zorder zpiece zprevious zreplace zsyntax zsort ztime \201";
char    zsvn[256] =         /* intrinsic z-special variables */
" za zb zc zd ze zf zh zi zj zl zmc zmn zmp zma zml zmu zme zn zo zp zs zt zu zv \
zcontrolc zdate zerror zname zhorolog zinrpt zjob zlocal zorder zprecision zsystem ztime ztr ztrap zuuid zut zversion \201";
char    brkaction[256] = "\201";    /* action in case of BREAK     */
pid_t   father = 0;         /* JOB-ID of father process         */
char jour_hostid[256];
/* date types parameters */
char    month[NO_DATETYPE][12][MONTH_LEN] = {
    {"01\201", "02\201", "03\201", "04\201", "05\201", "06\201", "07\201", "08\201", "09\201", "10\201", "11\201", "12\201"},
    {"01\201", "02\201", "03\201", "04\201", "05\201", "06\201", "07\201", "08\201", "09\201", "10\201", "11\201", "12\201"},
    {"JAN\201", "FEB\201", "MAR\201", "APR\201", "MAY\201", "JUN\201", "JUL\201", "AUG\201", "SEP\201", "OCT\201", "NOV\201", "DEC\201"},
    {"01\201", "02\201", "03\201", "04\201", "05\201", "06\201", "07\201", "08\201", "09\201", "10\201", "11\201", "12\201"},
    {"1\201", "2\201", "3\201", "4\201", "5\201", "6\201", "7\201", "8\201", "9\201", "10\201", "11\201", "12\201"},
    {"1\201", "2\201", "3\201", "4\201", "5\201", "6\201", "7\201", "8\201", "9\201", "10\201", "11\201", "12\201"},
    {"1\201", "2\201", "3\201", "4\201", "5\201", "6\201", "7\201", "8\201", "9\201", "10\201", "11\201", "12\201"},
    {"01\201", "02\201", "03\201", "04\201", "05\201", "06\201", "07\201", "08\201", "09\201", "10\201", "11\201", "12\201"}
};
char    dat1char[NO_DATETYPE][MONTH_LEN] =  /* date 1st delimiter */
{"/\201", "/\201", " \201", "/\201", ".\201", ".\201", ".\201", ".\201"};
char    dat2char[NO_DATETYPE][MONTH_LEN] =  /* date 2nd delimmiter */
{"/\201", "/\201", " \201", "/\201", ".\201", ".\201", ".\201", ".\201"};
char    dat3char[NO_DATETYPE] =
{'0', '0', '0', '0', '\201', '\201', '\201', '0'};  /* date day justify char */
char    dat4flag[NO_DATETYPE] =
{2, 1, 0, 0, 0, 0, 0, 0};       /* 0=DMY, 1=MDY, 2=YMD */
char    dat5flag[NO_DATETYPE] =
{0, 1, 1, 1, 1, 1, 0, 1};       /* suppress century digits */
long int datGRbeg[NO_DATETYPE] =
{578101L, 578101L, 578101L, 578101L, 578101L, 578101L, 578101L, 578101L};

/* first day of gregorian calendar 15-OCT-1582 ($H+672411) */
int     datetype = 0;           /* type for $zd special variable */

char    tim1char[NO_TIMETYPE] =
{':', ':'};             /* time 1st delimiter */
char    tim2char[NO_TIMETYPE] =
{':', ':'};             /* time 2nd delimiter */
char    tim3char[NO_TIMETYPE] =
{SP, SP};               /* time hour justify char */
char    tim4flag[NO_TIMETYPE] =
{0, 1};                 /* 0=24 Hrs 1=12 Hrs */
char    tim5flag[NO_TIMETYPE] =
{0, 0};                 /* suppress seconds */
int     timetype = 0;           /* type for $zt special variable */

jmp_buf sjbuf;
char   *roucu0;
char   *dofram0;

short   forx = 0;           /* FOR stack pointer */
char    forvar[NESTLEVLS + 1][40],  /* FOR variable */
    forinc[NESTLEVLS + 1][40],  /* FOR increment */
    forpost[NESTLEVLS + 1][128], /* FOR postconditional */
    forlim[NESTLEVLS + 1][40];  /* FOR limit value */
short   fortyp[NESTLEVLS + 1];      /* 0 = forever    1 = single,     */

/* 2 = unlim.iter,3 = limit iter. */
/* 4 =  "" +inc=1 5 =  "" + inc=1 */
short   fori[NESTLEVLS + 1];        /* if fortyp=5 length of forlimit */

char   *fvar;               /* current forvar */
char   *finc;               /* current forinc */
char   *fpost;              /* current forpost */
char   *flim;               /* current forlim */
short   ftyp;               /* current fortyp */
short   fi;             /* current fori   */
short   forsw = FALSE;          /* FOR switch */
short   loadsw = TRUE;          /* flag to avoid redundant loads */
short   argless_forsw_quit = FALSE; /* QUIT from argumentless FOR */
short   sigint_in_for = FALSE;
short   direct_mode = TRUE;     /* are we in direct mode? */

/* after XECUTEs */
short   promflag = TRUE;        /* prompt execute flag */
short   privflag = FALSE;       /* extrinsic z-command flag */


char   *cmdstack;
char   *cmdptr;

short   offset;
long    timeout;
short   timeoutms;
char    tmp4[80] = "\201";
short   param = 0;          /* parameter count */
short   paramx = 0;         /* current parameter */
char   *ttyname ();
short   level = 0;          /* level count */
pid_t    pid;                /* $J = process ID */

char    nsname[256] = "USER\0"; /* namespace name */
char    nsroot[4096];        /* root path of namespace */
char    *freem_path;    /* path to the running instance of FreeM */
char    history_file[256];  /* path to the history file */
int n_lines;
int n_columns;

short ipc_pending = 0;   /* 1 if an incoming IPC is pending, 0 otherwise */

int strict_mode = 0;

short first_process = FALSE;
size_t shm_init_size = 4194304;

short inrpt_after_async = FALSE;

//#ifdef  USE_SIGACTION
//   struct sigaction act = {0};     /* signals stuff */
//#endif/*USE_SIGACTION*/

void unnew (void)
{
    char   *xptr;
    int     i;
    long    j;
    char    tmp[256];
    
    #ifdef DEBUG_NEWPTR
        int loop;
        printf("Un-Newing: ");
        printf("[nstx] nstx is %d\r\n",nstx);
        printf("[nestnew] nestnew[nstx] is %d\r\n",nestnew[nstx]);
    #endif
    

    xptr = nestnew[nstx];       /* get position of newpointer */
    
    while (xptr < newptr) {
        i = *--newptr;

        if (i != kill_all) {
            j = UNSIGN (*--newptr);
            newptr -= (j + 1);
            stcpy0 (varnam, newptr, j + 1);

            if (i == set_sym) {
                j = UNSIGN (*--newptr);
                newptr -= (j + 1);
                stcpy (tmp, newptr);
            } 
            else {
                tmp[0] = EOL;
            }
        } 
        else {
            varnam[0] = EOL;
            tmp[0] = EOL;
        }

        if (varnam[0] == '$') {
            if (varnam[1] == 't') test = tmp[0]; /* pop $TEST */
            else if (varnam[1] == 'j') pid = UNSIGN (tmp[0]) * 256 + UNSIGN (tmp[1]); /* pop $job */
            else if (varnam[1] == 'z' && varnam[2] == 'i') breakon = tmp[0]; /* pop $zinrpt */
            else if (varnam[1] == 'e' && varnam[2] == 't') { /* pop $etrap */
                stcpy (etrap, tmp);
            }
            else if (varnam[1] == 'e' && varnam[2] == 's') { /* pop $estack */
                char esbuf[256];

                stcpy (esbuf, tmp);
                stcnv_m2c (esbuf);

                estack = atoi (esbuf);
            }
            else { /* pop $reference/$zreference */
                stcpy (zref, tmp); 
                nakoffs = UNSIGN (varnam[4]);
            }
            
            continue;
        }
        symtab (i, varnam, tmp);
    }

    newptr = nestnew[nstx];
    nestnew[nstx] = 0;          /* reset pointers */

    return;

}                   /* end unnew() */

void m_fatal(char *s)
{
    int errno_sav;
    errno_sav = errno;
    
    set_io (UNIX);


    if (tp_level) {
	fprintf (stderr, "freem [FATAL]:  memory allocation failure in %s; rolling back %d transactions [errno %d %s]\n", s, tp_level, errno_sav, strerror (errno_sav));
	tp_trollback (tp_level);
    }
    else {
	fprintf (stderr, "freem [FATAL]:  memory allocation failure in %s [errno %d %s]\n", s, errno_sav, strerror (errno_sav));
    }
    
    exit (3);
}
