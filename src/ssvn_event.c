/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_event.c
 *    ^$EVENT ssv
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "freem.h"
#include "mref.h"
#include "mpsdef.h"

void ssvn_event(short action, char *key, char *data)
{

    freem_ref_t *ref;
    char *kbuf;
    
    ref = (freem_ref_t *) malloc (sizeof(freem_ref_t));
    NULLPTRCHK(ref,"ssvn_event");
	
    kbuf = (char *) malloc (STRLEN * sizeof(char));
    NULLPTRCHK(kbuf,"ssvn_event");
	
    stcpy (kbuf, key);

    mref_init (ref, MREF_RT_SSVN, "");
    internal_to_mref (ref, key);

    switch (action) {
    
    	case new_sym:
        case get_sym:         
        case set_sym:   
        case killone:
        case kill_sym:     
        case dat:
        case fra_order:
        case fra_query:
        case bigquery:
        case getnext:
        case m_alias:
        case zdata:            
            
            symtab (action, key, data);
                
            free (kbuf);
            free (ref);

            ierr = OK;
            return;
            
        default:
            ierr = INVREF;
            return;

    }

    free (kbuf);
    free (ref);

    *data = EOL;
    return;
}
