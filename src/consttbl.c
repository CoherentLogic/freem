/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   consttbl.c
 *    support for local constants
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include "mpsdef.h"
#include "consttbl.h"

ll_constant *const_head;

short const_define(char *key, char *data)
{
    
    ll_constant *t;    
    
    for (t = const_head; t != NULL; t = t->next) {

	if (stcmp (t->key, key) == 0) {
	    return 0;
	}

    }

    t = (ll_constant *) malloc (sizeof (ll_constant));
    NULLPTRCHK(t,"const_define");

    t->key = (char *) malloc (stlen (key) + 1);
    NULLPTRCHK(t->key,"const_define");

    t->data = (char *) malloc (stlen (data) + 1);
    NULLPTRCHK(t->data,"const_define");
    
    stcpy (t->key, key);
    stcpy (t->data, data);
    t->next = const_head;
    const_head = t;

    return stlen(t->key);

}

short const_is_defined(char *key)
{
    ll_constant *t;

    for (t = const_head; t != NULL; t = t->next) if (stcmp (t->key, key) == 0) return 1;	

    return 0;
    	
}

void const_restore(void)
{
    ll_constant *t;

    restoring_consts = TRUE;
    
    for (t = const_head; t != NULL; t = t->next) {
        symtab (set_sym, t->key, t->data);        
    }

    restoring_consts = FALSE;
    
    return;
}
