MAXLEN ;
    S $ZTRAP="ERRH"
    W "Creating an 8-character local variable name:  "
    S IDENT="ABCDEFGH",@IDENT="FOO"
    I ABCDEFGH="FOO" W "OK",!  E  W "MISMATCH",!
    KILL
    W "Creating a 256-character local variable name:  "
    S $P(IDENT,"A",256)="A",@IDENT="FOO"
    I @IDENT="FOO" W "OK",!  E  W "MISMATCH",!
    KILL
    W "Creating a 257-character local variable name:  "
    S $P(IDENT,"A",257)="A",@IDENT="FOO"
    I @IDENT="FOO" W "OK",!  E  W "MISMATCH",!
    Q
ERRH ;
    W "ERROR!",!
    W " $ECODE  = ",$ECODE,!
    W " $ZERROR = ",$ZERROR,!
    S ERCODE=$P($P($ZERROR,"<",2),">",1)
    W " MESSAGE = '",$VIEW(16,ERCODE),"'",!
    HALT

