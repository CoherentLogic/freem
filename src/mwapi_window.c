/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mwapi_window.c
 *    mwapi window support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2014, 2020, 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include "config.h"

#if defined(MWAPI_GTK)

#include "mpsdef0.h"
#include "mwapi_window.h"
#include "mwapi_event.h"

mwapi_window *mwapi_win_head;

void mwapi_foreach(void (*callback)(mwapi_window *))
{
    mwapi_window *t;

    for (t = mwapi_win_head; t != NULL; t = t->next) {
        (*callback)(t);
    }   
}

int mwapi_win_count(void)
{
    mwapi_window *t;
    int c = 0;

    for (t = mwapi_win_head; t != NULL; t = t->next) c++;

    return c;
}


void mwapi_win_show_all(void)
{
    mwapi_foreach (&mwapi_win_show);
}

void mwapi_win_show(mwapi_window *win)
{
    
    if (!win->win_visible) {

        mwapi_event *destroy_ev = mwapi_event_new ("CLOSE", win->window_name, win->window);       
        mwapi_wire_event (destroy_ev, "CLOSE^%MWAPI.EVENT.DEFAULT");
        
        gtk_widget_show_all (win->window);
        
        win->win_visible = TRUE;
        
    }

    return;
}

mwapi_window *mwapi_win_get(char *window_name)
{
    
    mwapi_window *t;

    for (t = mwapi_win_head; t != NULL; t = t->next) {

        if (stcmp (t->window_name, window_name) == 0) {
            return t;
        }   
        
    }

    /* this window does not yet exist. create it. */
    t = (mwapi_window *) malloc (sizeof (mwapi_window));
    NULLPTRCHK(t,"win_get");

    stcpy (t->window_name, window_name);

    t->window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
    t->win_visible = FALSE;
    
    t->next = mwapi_win_head;
    mwapi_win_head = t;

    return t;
    
}
#endif
