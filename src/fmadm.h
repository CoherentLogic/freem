/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   fmadm.h
 *    FreeM Administrative Tool
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(_FMADM_H)
#define _FMADM_H

#include "mpsdef0.h"

#include "fma_globals.h"
#include "fma_journals.h"
#include "fma_locks.h"
#include "fma_routines.h"
#include "fma_jobs.h"

#define FMA_MAXARGS 30

#define FMAC_SELECT 0
#define FMAC_LIST 1
#define FMAC_EXAMINE 2
#define FMAC_VERIFY 3
#define FMAC_COMPACT 4
#define FMAC_REPAIR 5
#define FMAC_CREATE 6
#define FMAC_REMOVE 7
#define FMAC_IMPORT 8
#define FMAC_EXPORT 9
#define FMAC_BACKUP 10
#define FMAC_RESTORE 11
#define FMAC_MIGRATE 12
#define FMAC_EDIT 13
#define FMAC_SET 14
#define FMAC_SHOW 15
#define FMAC_EXIT 100
#define FMAC_INVALID 255

#define ACT_LIST 0
#define ACT_EXAMINE 1
#define ACT_VERIFY 2
#define ACT_COMPACT 3
#define ACT_REPAIR 4
#define ACT_CREATE 5
#define ACT_REMOVE 6
#define ACT_IMPORT 7
#define ACT_EXPORT 8
#define ACT_BACKUP 9
#define ACT_RESTORE 10
#define ACT_MIGRATE 11
#define ACT_EDIT 12

#define OBJ_LOCK 0
#define OBJ_ZALLOC 1
#define OBJ_JOURNAL 2
#define OBJ_NAMESPACE 3
#define OBJ_GLOBAL 4
#define OBJ_ROUTINE 5
#define OBJ_HARDCOPY 6
#define OBJ_RGAPROTOCOL 7
#define OBJ_JOB 8

#define UNSIGN(A) ((A)&0377)

#if !defined(PATH_MAX)
# define PATH_MAX 1024
#endif

int fmadm_usage (void);
void fmadm_exit (int retval);
int fm_list (short object, int optc, char **options);
int fm_examine (short object, int optc, char **options);
int fm_verify (short object, int optc, char **options);
int fm_compact (short object, int optc, char **options);
int fm_repair (short object, int optc, char **options);
int fm_create (short object, int optc, char **options);
int fm_remove (short object, int optc, char **options);
int fm_import (short object, int optc, char **options);
int fm_export (short object, int optc, char **options);
int fm_backup (short object, int optc, char **options);
int fm_restore (short object, int optc, char **options);
int fm_migrate (short object, int optc, char **options);
int fm_edit (short object, int optc, char **options);

void fm_sig_attach (int sig, void *handler);
void fm_sig_init (void);
void fm_on_sigint (void);
void fm_on_sigterm (void);

extern char fma_namespace[STRLEN];
extern char fma_locktab[STRLEN];
extern char fma_routine_path[STRLEN];
extern char fma_global_path[STRLEN];
extern char fma_journal_path[STRLEN];
extern char fma_pct_global_path[STRLEN];
extern char fma_pct_routine_path[STRLEN];
extern char fma_journal_cut_threshold[STRLEN];

extern char gloplib[PATHLEN];
extern char glopath[PATHLEN];

extern pid_t pid;

extern short fma_base_opt;
extern short fma_min_args;

#endif
