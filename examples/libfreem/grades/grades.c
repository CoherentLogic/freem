#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <freem.h>

int main(int argc, char **argv)
{
    char buf[256];
    double student_grade;
    int student_count;

    freem_ref_t *ref;

    /* initialize libfreem; use the USER namespace */
    freem_init ("USER");

    if ((ref = (freem_ref_t *) malloc (sizeof (freem_ref_t))) == NULL) {
        fprintf (stderr, "Memory allocation error.\n");

        exit (1);
    }

    ref->reftype = MREF_RT_GLOBAL;
    strcpy (ref->name, "grades");

    while (ref->subscript_count < 256) {

        ref->subscript_count = 0;

        fprintf (stdout, "Enter student name, or \"exit\" to quit:  ");
        fgets (buf, 255, stdin);

        buf[strcspn(buf, "\n")] = 0;

        if (strcmp (buf, "exit") == 0) break;

        student_count++;

        strncpy (ref->subscripts[ref->subscript_count++], buf, 254);

        fprintf (stdout, "\nEnter student grade:  ");
        fgets (buf, 255, stdin);

        buf[strcspn(buf, "\n")] = 0;

        printf("buf = '%s'\n", buf);

        strncpy (ref->value, buf, 254);

        printf("ref->value = '%s'\n", ref->value);

        freem_set (ref);

    }

    fprintf (stdout, "\nYou have entered %d student grades:\n\n", student_count);

}