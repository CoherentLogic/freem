/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   fma_locks.c
 *    fmadm - locks
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdlib.h>
#include "config.h"

#if defined(HAVE_GETOPT_H)
# include <getopt.h>
#endif

#include "fmadm.h"

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "locktab.h"

int unlock (char *locktab, long pid);
long int tell (int fd);

int fma_locks_list (int optc, char **opts)
{

    printf ("\nFreeM Lock Table\n");
    printf ("----------------\n\n");

    locktab_dump ();
    
    return 0;
} /* fma_locks_list() */

int fma_locks_remove (int optc, char **opts)
{

    int ct;
    int c;
    int option_index = 0;
    pid_t t_pid = 0;

#if defined(HAVE_GETOPT_LONG)

    struct option long_options[] = {        
        {"pid", required_argument, 0, 'p'},
        {0, 0, 0, 0}
    };

#endif

#if defined(HAVE_GETOPT_LONG)

    while(1) {
        
        c = getopt_long (optc, opts, "p:", long_options, &option_index);

        if (c == -1) break;

        switch (c) {

            case 'p':
                t_pid = atol (optarg);
                break;

        }
    }




#else

#endif

    if (t_pid == 0) {
        fprintf (stderr, "fmadm:  must supply -p or --pid argument\n");
        return 1;
    }

    printf ("\nRemoving all LOCKs for PID %ld:\t", t_pid);

    ct = unlock (fma_locktab, t_pid);

    if (ct >= 0) {
        printf ("[%d MATCHES]\n\n", ct);
    }
    else {
        printf ("[ERROR]\n\n");
    }

    return ct >= 0 ? 0 : 1;

} /* fma_locks_remove() */

/* unLOCK all entries of pid */
int unlock (char *locktab, long n_pid)           
{
    pid_t old_pid = pid;
    pid = n_pid;
    

    if (locktab_get_sem () == TRUE) {

        locktab_unlock_all ();

        pid = old_pid;
        locktab_release_sem ();

        return 1;
    }
    else {
        fprintf (stderr, "fmadm:  could not acquire LOCK table semaphore\n");
        pid = old_pid;
        return 0;
    }

    pid = old_pid;
    return 0;

} /* lock() */
