%DB ; FREEM DBMS
    Q
G(S,I,F) Q $G(@S@(I,F),"")
S(S,I,F,V) S @S@(I,F)=V Q
D(S,I) K @S@(I) Q
GA(S,I,F,E) Q $G(@S@(I,F,E),"")
SA(S,I,F,E,V) S @S@(I,F,E)=V Q
DA(S,I,F,E) K @S@(I,F,E) Q
MA(S,I,F,C) N E,R S (E,R)="" F  S E=$O(@S@(I,F,E)) Q:E=""  S R="D "_C_"("_I_","_E_","_@S@(I,F,E)_")" X R


