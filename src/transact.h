/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   transact.h
 *    Function prototypes, structs, and macros for FreeM
 *    transaction support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#define TP_MAX_NEST 256
#define TP_MAX_OPS 256

typedef struct tp_gblop {

	/* does this operation represent a LOCK? */
	short is_lock;

	/* flags presented to global(); action and 
	   data do not apply if is_lock == TRUE */
	short action;
	char key[256];
	char data[256];

} tp_gblop;

typedef struct tp_transaction {

	/* transaction ID */
	char tp_id[256];

	/* transaction flags */
	short serial;
	short restartable;

	/* array of symbols to be restored on TRESTART */
	char sym_save[256][256];

	int opcount;

	tp_gblop ops[TP_MAX_OPS];

} tp_transaction;

extern int tp_committing;
extern int tp_level;
extern tp_transaction transactions[TP_MAX_NEST];
extern void tp_init(void);
extern short tp_get_sem(void);
extern void tp_release_sem(void);
extern int tp_tstart(char *tp_id, short serial, short restartable, char **sym_save);
extern int tp_add_op(short islock, short action, char *key, char *data);
extern int tp_tcommit(void);
extern int tp_cleanup(int levels);
extern int tp_trollback(int levels);
extern int tp_trestart(void);
extern void tp_tdump(void);
extern void tp_get_op_name(char *buf, const short action);

