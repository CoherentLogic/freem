/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   symtab_dispatch.c
 *    symbol table dispatch module
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <string.h>
#include <ctype.h>
#include "mpsdef.h"
#include "mref.h"

void symtab (short action, char *key, char *data)
{
    register int i;
    register int j;
    char ch;

    freem_ref_t rv;
    freem_ref_t *r = &rv;

    mref_init (r, MREF_RT_LOCAL, "");
    internal_to_mref (r, key);

    if (action != fra_order && action != fra_query) {
        for (i = 0; i < r->subscript_count; i++) {
            
            if (strlen (r->subscripts[i]) < 1) {
                ierr = SBSCR;
                return;
            }
            
        }
    }

    if (rtn_dialect () == D_M77) {

        for (i = 0; i < r->subscript_count; i++) {
            for (j = 0; j < strlen (r->subscripts[i]); j++) {

                ch = r->subscripts[i][j];

                if (!isdigit (ch)) {
                    ierr = NOSTAND;
                    return;
                }
                
            }
        }

    }

    symtab_bltin (action, key, data);
    
}
