/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   mpsdef0.h
 *    common constants definitions for all mumps modules
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020, 2023 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#ifdef HAVE_WIRINGPI_H
#define register
#endif

#include <signal.h>
#include <stddef.h>

#ifndef	MDC_VENDOR_ID
#define MDC_VENDOR_ID 49
#endif/*MDC_VENDOR_ID*/

#define byte char
#define toggle(A) (A^=01)

/* if you do not want to run under SCO-UNIX, put the following in comment */

#define SCO

/* if you want mumps to run under XENIX, put the following in comment */
#define SYSFIVE

/* if you want mumps to run under LINUX, un-comment the following */
#define LINUX

/* rlf 01/15/99 If you want to compile under libc-6 (GLIBC), as on */
/*              RedHat Linux >= 5.0, define LINUX_GLIBC.           */
#define LINUX_GLIBC

/* spz 4/19/99 If you want to compile under FreeBSD 2.2.8+, define the */
/* following line... */
/*#define FREEBSD */

/* spz 5/24/99 use the new variable stack */
/* #define NEWSTACK   */
/* #define DEBUG_NEWSTACK */

/* rlf 01/16/99 If you want German error messages, define EM_GERMAN   */
/*              If you want English error messages, define EM_ENGLISH */
#define EM_ENGLISH

/* lmv 1999-05-09 define this if you want to use gmtoff timezone data in */
/*                struct tm instead of tzadj                             */
#define USE_GMTOFF

/* lmv 1999-05-09 define this if you want to include sys/time.h */
#define USE_SYS_TIME_H

/* lmv 1999-05-09 define this if you want to use the gettimeofday function */
/*                instead of the ftime function                            */
#define USE_GETTIMEOFDAY


/* if you do not want MUMPS to support color terminals, put this in comment */
#ifdef SCO
#define COLOR
#endif /* SCO */

/* if you do not want MUMPS to support automatic DEM <--> EUR conversion, */
/* put this in comment */
#define EUR2DEM "1.95583\201"

/* end of line symbol  0201/-127 */
#define EOL ((char)'\201')

/* end of line symbol  0202/-126/130 */
#define DELIM ((char)'\202')

/* default size of a 'partition' i.e. intermediate result stack+local vars */
#define DEFPSIZE 8388608L

/* default size of 'userdefined special variable table' */
#define DEFUDFSVSIZ 1000

/* default size of the NEW stack */
#define DEFNSIZE 4096

/* default number & size of alternate routine buffers */
/* maximum number of routine buffers */
#define MAXNO_OF_RBUF 128
#define DEFNO_OF_RBUF 128
#define DEFPSIZE0 10001

/* number of global files concurrently open */
#define NO_GLOBLS 6

/* length of global blocks */
#define BLOCKLEN 1024

/* number of global buffers */
#define NO_OF_GBUF 6

/* number of DO_FOR_XECUTE levels; i.e. depth of gosub-stack */
#define NESTLEVLS 80

/* depth of parser stack; each pending operation/argument requires one entry */
#define PARDEPTH 128

/* pattern match stack; maximum number of pattern atoms */
#define PATDEPTH 14

/* trace limit in globals module, i.e. trees wo'nt grow to the sky */
#define TRLIM 32

/* arguments in an ESC_sequence */
#define ARGS_IN_ESC 5

/* maximum length of a string, 0 <= $L() <= 255 */
#define STRLEN 65535

/* length of $ZTRAP variable */
#define ZTLEN 20

/* length of $ZF (function key) variable */
#define FUNLEN 128

/* length of $V(3)...$V(8) i.e. path names */
#define PATHLEN 4096

/* length of error message */
#define ERRLEN 180

/* number of DATE types */
#define NO_DATETYPE 8
#define NO_TIMETYPE 2
#define MONTH_LEN 10

/* number of zkey() production rules */
#define NO_V93 8

/* depth of CS/CRST (cursor save/restore) stack */
#define CSLEN 1

/* HOME device: number of lines and columns */
#define N_LINES 2000
#define N_COLUMNS 2000

#if !defined(FALSE)
# define FALSE   0
#endif

#if !defined(TRUE)
# define TRUE    1
#endif

#define DISABLE 0
#define ENABLE  1

/* parameters for set_io() */
#define UNIX    0
#define MUMPS   1

#include "merr.h"

/* HOME = default device */
#define HOME 0
/* number of devices/units */
#define MAXDEV 256
#define MAXSEQ 99       /* last sequential I/O channel */
#define FIRSTSCK 100    /* first socket I/O channel */
#define SCKCNT 155
#define MAXSCK SCKCNT-1

/* if tab_clear TBC (CSI 3g) and tab_set HTS (ESC H) are not processed */
/* by the 'HOME' device, define the symbol PROC_TAB to emulate them    */
/* #define PROC_TAB          #otherwise make it comment !!!            */
#define PROC_TAB

/* ASCII control character mnemonics */
#define NUL 0
#define SOH 1
#define STX 2
#define ETX 3
#define EOT 4
#define ENQ 5
#define ACK 6
#define BEL 7
#define BS 8
#if defined(TAB)
# undef TAB
#endif
#define TAB 9
#define LF 10
#define VT 11
#define FF 12
#define CR 13
#define SO 14
#define SI 15
#define DLE 16
#define DC1 17
#define DC2 18
#define DC3 19
#define DC4 20
#define NAK 21
#define SYN 22
#define ETB 23
#define CAN 24
#define EM 25
#define SUB 26
#if defined(ESC)
# undef ESC
#endif
#define ESC 27
#define FS 28
#define GS 29
#define RS 30
#define US 31
#define SP 32
#define DEL 127

/* function select in expr evaluator */
#define STRING 0
#define NAME 1
#define LABEL 2
#define OFFSET 3
#define ARGIND 4

/* function select in global/local/ssv variables management */
/* even numbers require 'read/write' access, odd numbers 'read' access */
#define set_sym  0
#define kill_sym 2
#define kill_all 4
#define killexcl 6
#define new_sym  8
#define new_all 10
#define newexcl 12
#define killone 14
#define merge_sym 16
#define lock_inc 24
#define lock_dec 26
#define lock_old 28

#define get_sym  1
#define dat      3
#define fra_order    5
#define fra_query    7
#define bigquery 9
#define getinc  11
#define getnext 13
#define m_alias 15
#define zdata   17

/* sets 8th bit in A */
#define SETBIT(A) ((A)|0200)
/* needed if byte data are to be interpreted as unsigned integer */

#define UNSIGN(A) ((A)&0377)

/* Fail if pointer null */
extern void m_fatal(char *s);
#define NULLPTRCHK(p,s) if (p == NULL) m_fatal(s)

/* device control for terminal I/O */

#define ECHOON      (~DSW&BIT0)
#define DELMODE     (DSW&BIT2)
#define ESCSEQPROC  (DSW&BIT6)
#define CONVUPPER   (DSW&BIT14)
#define DELEMPTY    (DSW&BIT19)
#define NOCTRLS     (DSW&BIT20)
#define CTRLOPROC   (DSW&BIT21)
#define NOTYPEAHEAD (DSW&BIT25)

#define BIT0  1
#define BIT1  2
#define BIT2  4
#define BIT3  8
#define BIT4  16
#define BIT5  32
#define BIT6  64
#define BIT7  128
#define BIT8  256
#define BIT9  512
#define BIT10 1024
#define BIT11 2048
#define BIT12 4096
#define BIT13 8192
#define BIT14 16384
#define BIT15 32768
#define BIT16 65536
#define BIT17 131072
#define BIT18 262144
#define BIT19 524288
#define BIT20 1048576
#define BIT21 2097152
#define BIT22 4194304
#define BIT23 8388608
#define BIT24 16777216
#define BIT25 33554432
#define BIT26 67108864
#define BIT27 134217728
#define BIT28 268435456
#define BIT29 536870912
#define BIT30 1073741824
#define BIT31 2147483648

/* functions from mumps.c */
void    unnew (void);

#include "sighnd.h"

/* functions from expr.c */
void    expr (short extyp);
void    zsyntax ();
void    zdate ();
void    ztime ();
void    zkey ();
short   is_horolog(char *s);

/* functions from ssvn.c */
void    ssvn (short action, char *key, char *data);

/* functions from ssvn_character.c */
void    ssvn_character(short action, char *key, char *data);

/* functions from ssvn_device.c */
void    ssvn_device(short action, char *key, char *data);

/* functions from ssvn_display.c */
void    ssvn_display_update(void);
void    ssvn_display(short action, char *key, char *data);

/* functions from ssvn_event.c */
void    ssvn_event(short action, char *key, char *data);

/* functions from ssvn_global.c */
void    ssvn_global(short action, char *key, char *data);

/* functions from ssvn_job.c */
void ssvn_job_add_device(int channel, char *device);
void ssvn_job_remove_device(int channel);
void    ssvn_job_update(void);
void    ssvn_job(short action, char *key, char *data);
void    frm_process_alias (char *key);

/* functions from ssvn_library.c */
void    ssvn_library(short action, char *key, char *data);
void    ssvn_library_update(void);

/* functions from ssvn_lock.c */
void    ssvn_lock_add(char *key, pid_t owner_job, int count);
void    ssvn_lock_remove(char *key);
void    ssvn_lock(short action, char *key, char *data);

/* functions from ssvn_routine.c */
void    ssvn_routine(short action, char *key, char *data);
void    ssvn_routine_update(void);

/* functions from ssvn_system.c */
void    ssvn_system(short action, char *key, char *data);
void    ssvn_system_update(void);
/* functions from ssvn_window.c */
void    ssvn_window(short action, char *key, char *data);

/* functions from ssvn_z.c */
void    ssvn_z(short action, char *key, char *data);

/* functions from ssvn_zos.c */
void    ssvn_zos(short action, char *key, char *data);

/* functions from ssvn_zfreem.c */
void    ssvn_zfreem(short action, char *key, char *data);

/* functions from ssvn_zprocess.c */
void    ssvn_zprocess(short action, char *key, char *data);

/* functions from ssvn_routine.c */
void    ssvn_routine_date(void);

#include "ssvn_zrpi.h"

/* functions from symtab_dispatch.c */
void symtab (short action, char *key, char *data);
short int collate ();
short int numeric ();
short int comp ();
void    intstr ();
void    lintstr ();
void    udfsvn ();
long    getpmore ();
long    getumore ();
long    getrmore ();
short int getnewmore ();

/* functions from symtab_bltin.c */
void symtab_bltin (short action, char *key, char *data);

/* functions from global_dispatch.c */
void global_set_engine(char ns, char *engine);
void global (short action, char *key, char *data);
void close_all_globals ();

/* functions from global_bltin.c */
void global_bltin (short action, char *key, char *data);

/* functions from global_bdb.c */
void global_bdb (short action, char *key, char *data);
void frm_bdb_gbl_session_dump(void);
void frm_bdb_flush_all(void);
void frm_bdb_close_all(void);

/* functions from operator.c */
short int pattern ();
void    pminmax ();
void    add ();
void    mul ();
void    mdiv ();
void    power ();
void    g_sqrt ();
int     numlit ();
long    intexpr ();
short int tvexpr ();
void    m_op ();

/* functions from service.c */
long int find ();
short int kill_ok ();
void    lineref ();
void    zi ();
void    write_f ();
void    writeHOME ();
void    m_output ();
void    write_m ();
void    write_t (short int col);
void    ontimo ();
void    read_m (char *stuff, long timeout, short timeoutms, short length);
void    hardcpf ();
int     rdchk0 ();
int     locking ();
void    set_io (short action);
void    set_break (short break_char);
void    set_zbreak (short quit_char);
void    zload ();
void    zsave ();
void    lock (char *lockarg, long time_out, char type);
void    getraddress (char *a, short lvl);

/* functions from compr.c */
void    CompressString ();
void    ExpandString ();

/* functions from strings.c */
long int stlen ();
long int stcpy ();
void    stcpy0 ();
void    stcpy1 ();
short int stcat ();
short int stcmp ();
char *trim (char *s);
void stcnv_m2c (char *mstr);
void stcnv_c2m (char *cstr);
size_t key_to_name (char *buf, const char *key, size_t count);
size_t name_to_key (char *buf, const char *name, size_t count);
void create_var_key (char *buf, int subct, char *nam, ...);
void trim_decimal (char *s);
void uuid_v4 (char *buf);

short is_standard(void);
int rtn_dialect(void);

/* CRT screen */
struct vtstyp {
    unsigned char screenx[N_LINES + 1][N_COLUMNS];	/* characters */
    unsigned char screena[N_LINES + 1][N_COLUMNS];	/* attributes */
#ifdef COLOR
    unsigned char screenc[N_LINES + 1][N_COLUMNS];	/* colors     */
#endif					/* COLOR */
    char    sclines[N_LINES + 1];	/* lines translation table   */
    char    rollflag;			/* Roll or Page mode */
    char    lin24;			/* 24 lines or 25 lines mode */
    char    savarg;
    char    tabs[N_COLUMNS];
    unsigned char Xpos;
    unsigned char Ypos;
    unsigned char sc_up;
    unsigned int sc_lo; /* jpw */
    unsigned char csx[CSLEN];
    unsigned char csy[CSLEN];
    short   cssgr[CSLEN];		/* save SGR flag */
    short   cscol[CSLEN];		/* save SGR flag */
    short   cs;
    unsigned char att;
#ifdef COLOR
    unsigned char col;			/* color byte */
#endif					/* COLOR */
#ifdef SCO
    unsigned char bw;			/* black_on_white flag */
#endif					/* SCO */
};

/* functions from views.c */
void    view_com ();
void    view_fun ();
short int newpsize ();
short int newusize ();
short int newrsize ();
void    zreplace ();
short int tstglvn ();
void    zname ();
short int znamenumeric ();
void    procv22 ();
void    v25 ();
void    m_tolower ();
void    part_ref (struct vtstyp *scr, short from, short to);

#define D_FREEM 0
#define D_M77 1
#define D_M84 2
#define D_M90 3
#define D_M95 4
#define D_MDS 5
#define D_M5  6

/* per-routine flags */
typedef struct rtn_flags {
    short standard;
    short dialect;
} rtn_flags;


