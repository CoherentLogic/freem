%TUI.MENU ; FREEM MENU HANDLER
    Q
GO(S,I) ;
    N %,C,DS,OPT S (%,OPT)=""
    D DISP(S,I)
NXT S DS=1 W "Select ",$$G^%DB(S,I,"NAME")," Option//",$$G^%DB(S,I,"DEFAULT")," "
    R %
    I (%="q")!(%="Q") W " Quit",! Q
    I %="?" D DISP(S,I) G NXT
    S:%="" %=$$G^%DB(S,I,"DEFAULT"),DS=0
    S %=$TR(%,"abcdefghijklmnopqrstuvwxyz","ABCDEFGHIJKLMNOPQRSTUVWXYZ")
    S:'$D(@S@(I,"OPTS",%)) %=""
    I %="" W !,"Invalid selection. Press ? to view all options.",! G NXT
    W:DS " " W @S@(I,"OPTS",%)
    I ('$D(@S@(I,"ACT",%)))&('$D(@S@(I,"TAG",%))) W !,"No action or tag defined for ",%,".",! G NXT
    K %TAG I $D(@S@(I,"TAG",%)) S %TAG=@S@(I,"TAG",%)
    I $D(@S@(I,"ACT",%)) S C="D "_@S@(I,"ACT",%) X C K C G NXT
    Q
DISP(S,I) ;
    N % S %=""
    W !,$$G^%DB(S,I,"DESC")," - ",$$G^%DB(S,I,"NAME"),!,!
    F  S %=$O(@S@(I,"OPTS",%)) Q:%=""  D
    . W "  ",%,") ",@S@(I,"OPTS",%),!
    W !,"  Q) Quit",!,!
    Q
    
