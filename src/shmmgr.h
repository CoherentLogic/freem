/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   shmmgr.h
 *    shared memory manager data structures
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2021 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#if !defined(__shmmgr_h)
# define __shmmgr_h

#include <stdlib.h>
#if defined(HAVE_STDINT_H)
# include <stdint.h>
#endif
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/types.h>

#include "locktab.h"
#include "jobtab.h"

/* allocation states */
#define PG_FREE     0
#define PG_ALLOC    1

/* size of a page */
#define PG_SIZE 1024

/* shm_init return values */
#define SHMS_SUCCESS 1
#define SHMS_GET_ERR 0
#define SHMS_ATTACH_ERR -1

#define SEMK_SHM 0xBBDD21
#define SEMK_LOCKTAB 0xBBDD22
#define SEMK_JOBTAB 0xBBDD23
#define SEMK_TP 0xBBDD24

#define SHMALIGN(a) (void *) (((unsigned long) a + sysconf (_SC_PAGESIZE)) & ~(sysconf (_SC_PAGESIZE) - 1))

/* data structures */

/* alloc map entry */
typedef struct shm_page_t {

    /* is this the first entry for this allocation? */
    short is_first;

    /* can be PG_FREE or PG_ALLOC */
    short pg_state;

    /* to which pid does this page belong? */
    pid_t pid;

    /* is this the last entry for this allocation? */
    short is_last;
    
} shm_page_t;


/* segment header */
typedef struct shm_hdr_t {

    /* will be set to shm_key to identify whether or not the segment is initialized */
    key_t magic;

    /* pid of the first process that attached to the segment */
    pid_t first_process;

    /* shm address */
    void *shmad;

    /* HEAD of lock table linked list */
    locktab_ent_t *locktab_head;    

    /* HEAD of job table linked list */
    job_slot_t *jobtab_head;

    /* locks out non-fmadm processes when set to 1 */
    short maintenance_mode;

    unsigned long max_locks;
    unsigned long max_jobs;
    unsigned long max_ipcs;
    
    /* pid of process currently in transaction */
    /* zero if nobody owns these exclusive rights */
    pid_t tp_owner;

    /* counter for the tp semaphore */
    unsigned long long tp_semctr;
    
    /* monotonically-incrementing serial number for DB operations:
     * all ops inside of transactions have the same tp_serial_number.
     *
     * used in journaling.
     */
    unsigned long long tp_serial_number;

    /* allocation map */
    shm_page_t alloc_map[1];
    
} shm_hdr_t;

typedef struct shm_config_t {

    /* segment size */
    size_t segsiz;

    /* page size */
    size_t pgsiz;

    /* page count */
    int pgct;

    /* shared memory key */
    key_t key;

    /* segment ID */
    int seg_id;

    /* segment data area */
    void *dta;

    /* pointer to the header */
    shm_hdr_t *hdr;
    
    /* actual buffer */
    void *buf;
    
} shm_config_t;

/* global variables */
extern shm_config_t *shm_config;

/* function prototypes */
extern short shm_init(const size_t seg_size);
extern short shm_exit(void);
extern short shm_get_sem(void);
extern short shm_release_sem(void);
extern shm_page_t *shm_get_alloc_map_entry(const int page_number);
extern void *shm_page_num_to_address(const int page_num);
extern int shm_address_to_page_num(const void *address);
extern void *shm_alloc_pages(const int page_count);
extern void *shm_alloc(const size_t bytes);
extern void shm_free_page(const int page_number);
extern void shm_free(const void *addr);
extern void shm_dump(void);
extern void shm_dump_pages(void);
#endif

   
