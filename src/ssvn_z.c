/*
 *                            *
 *                           * *
 *                          *   *
 *                     ***************
 *                      * *       * *
 *                       *  MUMPS  *
 *                      * *       * *
 *                     ***************
 *                          *   *
 *                           * *
 *                            *
 *
 *   ssvn_z.c
 *    implementation-defined ssv support
 *
 *  
 *   Author: John P. Willis <jpw@coherent-logic.com>
 *    Copyright (C) 1998 MUG Deutschland
 *    Copyright (C) 2020 Coherent Logic Development LLC
 *
 *
 *   This file is part of FreeM.
 *
 *   FreeM is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   FreeM is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero Public License for more details.
 *
 *   You should have received a copy of the GNU Affero Public License
 *   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
 *
 **/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "mpsdef.h"
#include "mref.h"

void ssvn_z(short action, char *key, char *data)
{

    freem_ref_t *ref = (freem_ref_t *) malloc (sizeof(freem_ref_t));
    NULLPTRCHK(ref,"ssvn_z");

    mref_init (ref, MREF_RT_SSVN, "");
    internal_to_mref (ref, key);


    if (strcmp (ref->name, "^$ZPROCESS") == 0) {
        ssvn_zprocess (action, key, data);
        return;
    }
    else if (strcmp (ref->name, "^$ZFREEM") == 0) {
        ssvn_zfreem (action, key, data);
        return;
    }
    else if (strcmp (ref->name, "^$ZRPI") == 0) {
        ssvn_zrpi (action, key, data);
        return;
    }

    *data = EOL;
    return;
}
