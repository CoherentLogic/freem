# FreeM Project Policies


## Copyright and License

This document is Copyright (C) 2020 John P. Willis

Permission is granted to copy, distribute and/or modify this document under the 
terms of the GNU Free Documentation License, Version 1.3 or any later version 
published by the Free Software Foundation; with no Invariant Sections, with no 
Front-Cover texts, and with no Back-Cover Texts.

## Mission Statement

To provide a highly portable, feature-rich, free software implementation of the 
M programming language, compliant with relevant MDC standards, and focused on
extensibility and easy application development on client, desktop, single-board,
and older server devices.

## Identity

FreeM is an implementation of the M programming language and database. This is
woven into the foundational motivations of our core developers, and the
observance of this identity is to be considered a foundational rule of the
project.

We are first and foremost lovers of the M programming language, and are proud
to represent it with our quirky--yet feature-rich--little implementation. No
effort will be made or allowed to hide our M identity or cultural and community
heritage. 

## Shalom ha-Ashkenaz

We will continue to honor the enigmatic Shalom ha-Ashkenaz for his original 
contribution of FreeM to the community. The Star of David is the symbol he
chose to represent this implementation, and it will remain forever enshrined
in our code headers.

## External Forks

At least one external fork of FreeM is known to exist, pre-dating the current
maintainer of the GUMP-era codebase taking over. The FreeM project and its 
members may not publicly disparage or plagiarize from any such fork. If the
maintainers of these forks wish to join this project, they will be welcomed
warmly into our community.

## Other Implementations

FreeM project contributors are free to criticize other M implementations 
outside of the project. However, disparaging other implementations in the
FreeM mailing lists, Discord server, or other public or private social media
venues will not be tolerated, nor may FreeM contributors indicate or 
insinuate that their criticisms in other venues represent the official opinion
of the FreeM project itself.

Those who have signed nondisclosure agreements involving proprietary, closed-source
M implementations may not contribute source code to FreeM, as our project and its
host organization, Coherent Logic Development, cannot afford to defend itself 
against copyright infringement claims.

## Mailing Lists

The *freem-dev* mailing list (freem-dev@mailman.chivanet.org) is a venue for
discussions among those contributing to FreeM itself. Commit messages are 
automatically sent there.

The *freem-users* mailing list (freem-users@mailman.chivanet.org) is a venue for
those using FreeM to develop M programs and applications.

## Project Governance

Internal project governance is handled in the *project-policy* channel of the
*FreeM Community* Discord server.

John P. Willis (jpw@coherent-logic.com) is the project lead. In John's absence,
any member of the "Core Team" from the *FreeM Community* Discord server is 
authorized to speak with authority on matters of FreeM project governance, or
redirect such matters to John per their own judgment.

## Relationship to Coherent Logic Development LLC

Coherent Logic Development LLC lends its Internet and server infrastructure and
corporate weight to the FreeM project. It does not exercise authority over the
project itself or its contributors. We do not believe in corporate personhood.

## Copyright Policy

### Ethical Positioning

FreeM strongly identifies with the *free software* movement, and does not associate
itself with the ethically-empty *open source* movement or terminology. We believe
that the ethical implications of proprietary software are far more important than
the mere practical implications. We believe there is a natural right to software
freedom, and the laws written to protect proprietary software devils are both 
unjust and ill-conceived. Let the hoarders hoard their piles of money: we of the
FreeM project believe in all of the essential rights of the free software movement,
and this position rests well above and beyond any commercial gain we stand to make
from FreeM, if any.

### Code Licenses

FreeM will use strong copyleft licenses (namely, the GNU Affero General Public 
License, version 3 or later) for all contributions of code. 

### Documentation

Documentation will be released under the GNU Free Documentation License.

### Contributor Protections

FreeM contributors will retain their individual copyright for their contributions.

FreeM contributors will also receive full credit for their contributions in the
FreeM manual and on the FreeM website.

## Versioning and the Software Contract

### Semantic Versioning

FreeM will adhere as closely as possible to Semantic Versioning 2.0.0:

* Breaking changes will be limited to changes in major version
* New functionality will increment the minor version
* Fixes without functionality enhancements will increment the patch version

Releases prior to 1.0.0 come with no promises of stable APIs or functionality.

However, once a 1.0.0 release is reached, we will make every effort to avoid 
breaking API changes wherever possible, above and beyond the requirements 
enforced by semantic versioning.

## Issue Reporting

Issue reporting is handled exclusively through the FreeM GitLab repository
at https://gitlab.coherent-logic.com/jpw/freem

When reporting issues, please provide as much detail as possible. Leave the
assignee field blank, unless you intend to assign yourself, and a FreeM
core team member will assign the issue to the appropriate developer.

## Repository Access

If you have write access to the FreeM git repository, please do your development
in your own branch, and create a pull request for your changes. Unless you are
a core developer, you may not commit directly to the main branch.

## Code of Conduct

For the moment, the FreeM project will observe the *GNU Kind Communication 
Guidelines* (https://www.gnu.org/philosophy/kind-communication.html) as a 
Code of Conduct, pending the development of a more exhaustive and general
set of community software guidelines.

Our main focus is neither punitive nor retributive:  most people who push the
limits of acceptable behavior should be gently and privately reminded of the 
broader goals of the project, and guided with the same gentle hand towards
healthier means of communication.

FreeM developers should strive to display radical hospitality--free of judgment--
to all community members, participants, and outsiders, no matter what their
background or belief system. 

The one category of offense for which tolerance will be strictly limited is
hypocrisy. Hypocrisy, in our estimation, is most prominently on display in acts
based on prejudice. Whether against a race, culture, gender identity, sexual
orientation, religion, language, a mental, emotional, or physical challenge, all
prejudices are rooted in hypocrisy.

Place the needs of the community above your own, and you will get along well here.

## Standards Compliance

* The FreeM project will endeavor to comply with M Development Committee standards
  to the best of its ability, and to the extent to which FreeM's architecture
  allows.

* The FreeM project will maintain a presence in MDC proceedings. Its representative(s)
  will relay all MDC decisions to the FreeM developer community.

* The FreeM project will respect MDC guidelines with regards to vendor-specific
  extensions (using 'Z' prefixes, etc.).

* The FreeM project *may* implement MDC proposals prior to such proposals being
  elevated to type A extension status. Such features will be clearly marked as
  being subject to change, to avoid problems for FreeM users, should such proposals
  fail to become extensions, or should their syntax or semantics change.

* The FreeM project *may* retain non-standard features where architectural issues
  preclude the implementation of a standards-compliant alternative, until such
  time as a standards-compliant solution may be found. Such exceptions
  will be noted in the Conformance Clause in Appendix F of *The FreeM Manual*.

* Once the 1.0.0 release milestone is reached, each major or minor release will
  update the Conformance Clause in Appendix F of *The FreeM Manual*. This
  conformance clause will include a full, un-redacted report of a thorough and
  complete run of the *M Validation and Test Suite* (or *MVTS*), to include both
  success and error conditions. To hide areas where FreeM fails to conform to
  MDC standards does a great disservice to this project, undermines the labors
  of the MDC, and undermines trust in our community of users and developers.

## Coding Standards

Contributions must follow the Coding Standards in Appendix E of The FreeM Manual. 


