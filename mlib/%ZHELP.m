%ZHELP ;VCL/JPW-FREEM ONLINE HELP; 10/23/2020 6:55 PM
    ;0.0;FreeM;****FREEM**;John P Willis @2020
    ;
    ;                            *
    ;                           * *
    ;                          *   *
    ;                     ***************
    ;                      * *       * *
    ;                       *  MUMPS  *
    ;                      * *       * *
    ;                     ***************
    ;                          *   *
    ;                           * *
    ;                            *
    ;
    ;   %ZHELP.m
    ;    FreeM Online Help
    ;
    ;  
    ;   Author: John P. Willis <jpw@coherent-logic.com>
    ;    Copyright (C) 1998 MUG Deutschland
    ;    Copyright (C) 2020 Coherent Logic Development LLC
    ;
    ;
    ;   This file is part of FreeM.
    ;
    ;   FreeM is free software: you can redistribute it and/or modify
    ;   it under the terms of the GNU Affero Public License as published by
    ;   the Free Software Foundation, either version 3 of the License, or
    ;   (at your option) any later version.
    ;
    ;   FreeM is distributed in the hope that it will be useful,
    ;   but WITHOUT ANY WARRANTY; without even the implied warranty of
    ;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    ;   GNU Affero Public License for more details.
    ;
    ;   You should have received a copy of the GNU Affero Public License
    ;   along with FreeM.  If not, see <https://www.gnu.org/licenses/>.
    ;
    ;
    N D S D=$DIALECT,$DIALECT="FREEM"
    S:$D(%SYS.HLP) EXEC="!info freem --index-search='"_%SYS.HLP_"'"
    S:'$D(%SYS.HLP) EXEC="!info freem"
    KV %SYS.HLP
    @EXEC
    S $DIALECT=D K D
    Q